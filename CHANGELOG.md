## [14.2.1](https://gitlab.com/simonoscr/nixfiles/compare/14.2.0...14.2.1) (2025-03-02)

### 📦 Other

* update flake ([009f47a](https://gitlab.com/simonoscr/nixfiles/commit/009f47a185fc5ab4286700c734f0e1cd18e83f86))

### 🛠 Fixes

* cleanup ([f983626](https://gitlab.com/simonoscr/nixfiles/commit/f98362620159e03f125b9d08bb66fdfba1548bbe))
* update system pkgs, nix and config ([efd6c4b](https://gitlab.com/simonoscr/nixfiles/commit/efd6c4b2c48ac1637dc26a7de60ed597da1d1023))
* **hm:** enable brave as chromium fallback browser ([4cf157e](https://gitlab.com/simonoscr/nixfiles/commit/4cf157e0f3414e7f2e148252bbce3c7ea27661d6))
* **hm/gaming:** updatre sc gamescope args ([4de2798](https://gitlab.com/simonoscr/nixfiles/commit/4de2798dbf6bf6073ca36be99294e413e491932c))
* **hm/hyprland:** update shortcuts, settings etc ([7c2eeaa](https://gitlab.com/simonoscr/nixfiles/commit/7c2eeaafb911497a0fd4153d1777d63de0c9ac58))
* **hm/zed:** add log extension ([d544a3e](https://gitlab.com/simonoscr/nixfiles/commit/d544a3ee07df9b39baafe8dcf1d8f62568e31a58))
* **llm:** update configs ([04e3e0c](https://gitlab.com/simonoscr/nixfiles/commit/04e3e0c187b5c729274186e5100dc8c0e9916c10))

## [14.2.0](https://gitlab.com/simonoscr/nixfiles/compare/14.1.2...14.2.0) (2025-03-01)

### 📦 Other

* update flake ([4b8f92a](https://gitlab.com/simonoscr/nixfiles/commit/4b8f92a5144767c32f59717d1cf1e4d87c9ce9e1))
* update flake ([aa68b8b](https://gitlab.com/simonoscr/nixfiles/commit/aa68b8bbff045f36d0c94c358ab1023637bd8341))

### 🚀 Features

* update hardware and performance related settings and also update core parts ([4f99ec9](https://gitlab.com/simonoscr/nixfiles/commit/4f99ec96d19c7789c82ad72e3463b4790e656503))

### 🛠 Fixes

* disable kde-connect and vscodium ([b1f4785](https://gitlab.com/simonoscr/nixfiles/commit/b1f4785719b9048e139e9d5c688a7090540f0526))
* disable unused inputs ([2878832](https://gitlab.com/simonoscr/nixfiles/commit/28788322073341f284d02014a96097115978e8e3))
* **gaming:** update sc, and config little helper ([e5142f0](https://gitlab.com/simonoscr/nixfiles/commit/e5142f065ccd047dcd2896e2215fa5dabb8d5935))
* **hyprland:** update settings and remove unused plugins ([5b95338](https://gitlab.com/simonoscr/nixfiles/commit/5b95338e45f87bd79a2ed196b87422fceb83b32e))
* **kernel:** update sysctl config ([cb20fe0](https://gitlab.com/simonoscr/nixfiles/commit/cb20fe06093478a0ed79f6a430c50e5834aefba8))
* **network:** update rules and configs ([9930861](https://gitlab.com/simonoscr/nixfiles/commit/9930861df2171ee6aa71733e2e69f200b3cde99a))
* **programs:** remove unused or update other program configs ([5f67991](https://gitlab.com/simonoscr/nixfiles/commit/5f67991f08d8ce342ef5698f4b4e4d80372c891a))
* **services:** update services ([3731050](https://gitlab.com/simonoscr/nixfiles/commit/3731050f962effdc7c5901c11c433719e806c775))

## [14.1.2](https://gitlab.com/simonoscr/nixfiles/compare/14.1.1...14.1.2) (2025-02-25)

### 🛠 Fixes

* update flake ([8c49fec](https://gitlab.com/simonoscr/nixfiles/commit/8c49fec41f3d0b8599ede70b8d32cd4688a56de5))

## [14.1.1](https://gitlab.com/simonoscr/nixfiles/compare/14.1.0...14.1.1) (2025-02-21)

### 🛠 Fixes

* zed config ([579adca](https://gitlab.com/simonoscr/nixfiles/commit/579adca2844db88e6b64c0e9a67f305271109fc9))

## [14.1.0](https://gitlab.com/simonoscr/nixfiles/compare/14.0.0...14.1.0) (2025-02-21)

### 📦 Other

* update flake ([52e0061](https://gitlab.com/simonoscr/nixfiles/commit/52e0061f799218738767f32e2622bb30f4da8b1b))

### 🚀 Features

* update hyprland and hyprlock configuration ([55a1856](https://gitlab.com/simonoscr/nixfiles/commit/55a1856f3ecc86c74904618618420a5817dcecaf))

### 🛠 Fixes

* XDG portals fixing not doing xdg-open ([acef413](https://gitlab.com/simonoscr/nixfiles/commit/acef413544d5256392d33de34a21e042ea371cb0))
* **zed-editor:** update settings ([8d3a711](https://gitlab.com/simonoscr/nixfiles/commit/8d3a711dfed2ad1c4df2dfb34df0875c0e6f6fad))

## [14.0.0](https://gitlab.com/simonoscr/nixfiles/compare/13.11.1...14.0.0) (2025-02-21)

### 🧨 Breaking changes!

* moved infra setup to own repository and cleanup repo ([94aac10](https://gitlab.com/simonoscr/nixfiles/commit/94aac1065b896ed646291f7e28290dce3a794480))

## [13.11.1](https://gitlab.com/simonoscr/nixfiles/compare/13.11.0...13.11.1) (2025-02-18)

### :scissors: Refactor

* rename server to proxmox-server to refelct the server in name ([0c9c57c](https://gitlab.com/simonoscr/nixfiles/commit/0c9c57ccc74cb0260c982ec09ef8ca1b9e4b4f41))

## [13.11.0](https://gitlab.com/simonoscr/nixfiles/compare/13.10.0...13.11.0) (2025-02-18)

### 📦 Other

* update flake ([bf0c4ce](https://gitlab.com/simonoscr/nixfiles/commit/bf0c4ce86df1a9bf351df1d5b2569fae91e4e67a))
* update flock ([89a8a73](https://gitlab.com/simonoscr/nixfiles/commit/89a8a7334f7651843ae660d120ac191d3e337ffe))

### 🚀 Features

* added proxmox-server deployment ([089c37f](https://gitlab.com/simonoscr/nixfiles/commit/089c37fc30fa58e821c3df587e5cb40cd2cbb4bb))

### 🛠 Fixes

* try to avoid nixpkgs overlay in hm ([5c2ee27](https://gitlab.com/simonoscr/nixfiles/commit/5c2ee27fcde8c2e40c62ef0e14a130ce58ccd443))
* update k8s deployment and dont use it anymore ([76f8e02](https://gitlab.com/simonoscr/nixfiles/commit/76f8e0226857fa01fd6e955b5e2e4dec0b668e8c))
* update ssh settings ([b11bf2f](https://gitlab.com/simonoscr/nixfiles/commit/b11bf2f7c314fca06570a552f7d94d4f5653ced9))
* **desktop:** update substitutors and nix settings ([f651d1f](https://gitlab.com/simonoscr/nixfiles/commit/f651d1f723911f2dd19a51266856158b707f8cc4))

## [13.10.0](https://gitlab.com/simonoscr/nixfiles/compare/13.9.0...13.10.0) (2025-02-14)

### 🚀 Features

* update shell integration to manage globally§ ([a5baaa6](https://gitlab.com/simonoscr/nixfiles/commit/a5baaa6bebea6ae74edb1eff9d8799e35bde1fcd))

## [13.9.0](https://gitlab.com/simonoscr/nixfiles/compare/13.8.0...13.9.0) (2025-02-14)

### 🚀 Features

* added global aliases for all shells in hm ([6449058](https://gitlab.com/simonoscr/nixfiles/commit/64490589ebf6e70ad2c9159e9d740c73558742bd))

## [13.8.0](https://gitlab.com/simonoscr/nixfiles/compare/13.7.2...13.8.0) (2025-02-14)

### 🚀 Features

* update mostly server k8s things ([59275a0](https://gitlab.com/simonoscr/nixfiles/commit/59275a0b828e14d384ba49adc85df545cf6e81ed))

## [13.7.2](https://gitlab.com/simonoscr/nixfiles/compare/13.7.1...13.7.2) (2025-02-09)

### 📦 Other

* flake update ([e13653d](https://gitlab.com/simonoscr/nixfiles/commit/e13653df9e177cb1c4c61e896e5981b4553f252f))

### 🛠 Fixes

* **k8s:** make open-iscsi possible with longhorn ([827c26e](https://gitlab.com/simonoscr/nixfiles/commit/827c26ed4304bc3a6f900b922b46f302995ffe72))

## [13.7.1](https://gitlab.com/simonoscr/nixfiles/compare/13.7.0...13.7.1) (2025-02-09)

### 🛠 Fixes

* update desktop hyprland bar etc ([4a04346](https://gitlab.com/simonoscr/nixfiles/commit/4a0434633f29a6c37d00f580336301264e1501e7))

## [13.7.0](https://gitlab.com/simonoscr/nixfiles/compare/13.6.2...13.7.0) (2025-02-01)

### 📔 Docs

* update README ([0e96940](https://gitlab.com/simonoscr/nixfiles/commit/0e96940f8765b839e54c81fe862aa6d35aed9f14))

### 📦 Other

* flake update ([e1bc51c](https://gitlab.com/simonoscr/nixfiles/commit/e1bc51c9e060c68081ff640ecea288d8b59f9a25))

### 🚀 Features

* added second monitor to hyprland setup ([27995b2](https://gitlab.com/simonoscr/nixfiles/commit/27995b2fc65a03cad04104b7178fbf9f392bac1c))

### 🛠 Fixes

* **anyrun:** set terminal to ghostty ([1e779af](https://gitlab.com/simonoscr/nixfiles/commit/1e779af662e89d911b59c4fbca88c5056096300b))
* **hm:** disable polkit and use hyprpolkitagent ([093d8f8](https://gitlab.com/simonoscr/nixfiles/commit/093d8f843dc28880344516aed571df1ed60b30aa))
* **start-citizen:** readd gamescope args ([deea4f2](https://gitlab.com/simonoscr/nixfiles/commit/deea4f273a7bb7faae4acfc41686853a350bf75d))
* **tailray:** module already contains systemd.user ([b31748f](https://gitlab.com/simonoscr/nixfiles/commit/b31748f9f9f71f3526452ba502de8fe0e2d9cc98))

## [13.6.2](https://gitlab.com/simonoscr/nixfiles/compare/13.6.1...13.6.2) (2025-01-31)

### 🛠 Fixes

* **zeditor:** update zed settings and config ([33aacc3](https://gitlab.com/simonoscr/nixfiles/commit/33aacc30733ebf2d1f68f6a2aa4907dad57e52ca))

## [13.6.1](https://gitlab.com/simonoscr/nixfiles/compare/13.6.0...13.6.1) (2025-01-31)

### 📦 Other

* flake update ([780ea2d](https://gitlab.com/simonoscr/nixfiles/commit/780ea2d7e60e09509a73796f2dfca4a2b5047ddb))
* **hm:** disable unused modules ([085b837](https://gitlab.com/simonoscr/nixfiles/commit/085b83749e4f843e99eaf31882137cd4fcc71dd5))

### 🛠 Fixes

* network settings ([6965c14](https://gitlab.com/simonoscr/nixfiles/commit/6965c14082b1e209019c7014723d46c8999a7a4f))
* **audio:** make wireplumber low-latency confif work ([0f81ee2](https://gitlab.com/simonoscr/nixfiles/commit/0f81ee278a8c862cac8a3e7f2abaa662491710da))
* **core:** update kernel ([8ddaa2a](https://gitlab.com/simonoscr/nixfiles/commit/8ddaa2a6f740336a001ea659803148f69c26d0bc))
* **hyprland:** disable HDR again ([86f4ead](https://gitlab.com/simonoscr/nixfiles/commit/86f4ead4683680ff489b7507ceda50973f24f22b))
* **hyprland:** nixos module pkgs ([9aad088](https://gitlab.com/simonoscr/nixfiles/commit/9aad0885fbab9abc9f4028d755342a2d09cf8da2))
* **network:** update online network check ([19374d2](https://gitlab.com/simonoscr/nixfiles/commit/19374d2a09bee12777d81fed0040a1ee4e2a2d13))

## [13.6.0](https://gitlab.com/simonoscr/nixfiles/compare/13.5.0...13.6.0) (2025-01-30)

### 📦 Other

* flake update ([3ef5e1f](https://gitlab.com/simonoscr/nixfiles/commit/3ef5e1f5f8708a2283ad458a5ce72127c07932fe))

### 🚀 Features

* add jujutsu vcs ([61b38de](https://gitlab.com/simonoscr/nixfiles/commit/61b38def1d7a6ff854ecb5984ade3c9b588f2d02))

### 🛠 Fixes

* hyprland hdr set 10bit for monitor ([b6dc251](https://gitlab.com/simonoscr/nixfiles/commit/b6dc251492e809ba1bfabecdfb231528dbceb6d2))

## [13.5.0](https://gitlab.com/simonoscr/nixfiles/compare/13.4.0...13.5.0) (2025-01-29)

### 📦 Other

* flake update ([2f6f16b](https://gitlab.com/simonoscr/nixfiles/commit/2f6f16b59db6ba10f276ad891c799ff0a9647415))
* flake update ([453d3ec](https://gitlab.com/simonoscr/nixfiles/commit/453d3eca9b69645f07f3831e89d6602fd74013cf))

### 🚀 Features

* add appimage packages ([8e86d8d](https://gitlab.com/simonoscr/nixfiles/commit/8e86d8d8a55ef72f95ca6df006fd29a29704ba67))
* add ghostty ([eb0db5c](https://gitlab.com/simonoscr/nixfiles/commit/eb0db5c21ae97ef95ce3fc11f6e850277465a61d))

### 🛠 Fixes

* desktop services like audio and cosmic setup ([fe14989](https://gitlab.com/simonoscr/nixfiles/commit/fe14989236b3911db65353ce7a6c5f9f2cbce4c3))
* hyprland nixos module ([f977d3d](https://gitlab.com/simonoscr/nixfiles/commit/f977d3dfd626312bbdb1c214a73ca85881d15a85))
* update configs of hyprland, cosmic, gaming, terminal and pkgs ([cdebd8a](https://gitlab.com/simonoscr/nixfiles/commit/cdebd8a059a2b1320aae4cd68613eb9508653f0d))
* update core system components like nm, cpu+gpu ([d087e1a](https://gitlab.com/simonoscr/nixfiles/commit/d087e1a24c2d4094beab9e153892bb2afce14d4e))
* update gaming ([a28310d](https://gitlab.com/simonoscr/nixfiles/commit/a28310d8f3386b20fb89a60aea134a71b51ec632))
* update hyprland settings ([14695c4](https://gitlab.com/simonoscr/nixfiles/commit/14695c47f5abe9d294ac816d57133b7d253977c4))
* update hyprpanel settings ([e2b398f](https://gitlab.com/simonoscr/nixfiles/commit/e2b398fd94df86bff79403857e65a234427c47f7))
* update overlays ([83949dd](https://gitlab.com/simonoscr/nixfiles/commit/83949dd2ab9b50a6bb2b2f926ed2c721231c4d40))
* update zen ([e252dc6](https://gitlab.com/simonoscr/nixfiles/commit/e252dc6433fc6aedbde61a9d6886b00413c55df4))

## [13.4.0](https://gitlab.com/simonoscr/nixfiles/compare/13.3.0...13.4.0) (2024-12-23)

### 📦 Other

* flake update ([c257d39](https://gitlab.com/simonoscr/nixfiles/commit/c257d391fb99a0ae0ace33aec9dc886e235c9a23))

### 🚀 Features

* WIP add cosmic DE ([9537f27](https://gitlab.com/simonoscr/nixfiles/commit/9537f27dc88438d73b7095a00943fc01136f9a47))

### 🛠 Fixes

* add nixos-cosmic subs ([f106521](https://gitlab.com/simonoscr/nixfiles/commit/f1065214c0692035ac53182144510b84fa48d52e))
* cleanup and add pkgs and configuration ([ec6141a](https://gitlab.com/simonoscr/nixfiles/commit/ec6141a11f02b7a7111ea86defbad7dd89584482))
* update hyprland config and binds ([99b1901](https://gitlab.com/simonoscr/nixfiles/commit/99b190107bb62eada6eded9770e3192da431985e))
* update k8s deployment ([8aafe79](https://gitlab.com/simonoscr/nixfiles/commit/8aafe794e1254caa5cd8ebde3cd9fdac884159bc))
* update pam and polkit settings ([9a1bce0](https://gitlab.com/simonoscr/nixfiles/commit/9a1bce0df91674780cef620be024cf7936ded256))
* update zed settings ([3c2d368](https://gitlab.com/simonoscr/nixfiles/commit/3c2d36806380b568418d2a9031cf8494c0c82bfd))

## [13.3.0](https://gitlab.com/simonoscr/nixfiles/compare/13.2.4...13.3.0) (2024-12-05)

### 🚀 Features

* performance tweaks and generell system settings for gaming performance with cachyos kernel ([3b6d5f7](https://gitlab.com/simonoscr/nixfiles/commit/3b6d5f7212132a1bb777c5eae26feb6809e16cb2))

## [13.2.4](https://gitlab.com/simonoscr/nixfiles/compare/13.2.3...13.2.4) (2024-12-04)

### 📦 Other

* flake update ([8bb0337](https://gitlab.com/simonoscr/nixfiles/commit/8bb0337939cd1741f8225202dc624f3a132bf88a))

### 🛠 Fixes

* **gaming:** update gamemode config and hyrmode script ([5ddd1cc](https://gitlab.com/simonoscr/nixfiles/commit/5ddd1cc72d49e3142c0daf50cbb87b3d9db94e7b))
* **security:** switch to sudo-rs ([45fc451](https://gitlab.com/simonoscr/nixfiles/commit/45fc4515dbff5d33f4867259f2d29a3a560260e2))

## [13.2.3](https://gitlab.com/simonoscr/nixfiles/compare/13.2.2...13.2.3) (2024-12-04)

### 📦 Other

* update flake and remove unused inputs ([43f74d8](https://gitlab.com/simonoscr/nixfiles/commit/43f74d878a88d8392e91fe0ed653f60cf9761997))

### 🛠 Fixes

* **secrets:** make nix access-token for github readable by user who executes nix command ([70e2d8b](https://gitlab.com/simonoscr/nixfiles/commit/70e2d8b3ef6157e716b42deacd5f340be4df4924))

## [13.2.2](https://gitlab.com/simonoscr/nixfiles/compare/13.2.1...13.2.2) (2024-12-04)

### 🛠 Fixes

* add k9s module ([0964624](https://gitlab.com/simonoscr/nixfiles/commit/0964624786d3e212f9255a45fa16eebad719625c))
* add k9s module ([cdef805](https://gitlab.com/simonoscr/nixfiles/commit/cdef805706edcb4429ff3dfbe7d4133af5ce8287))
* update config ([928933c](https://gitlab.com/simonoscr/nixfiles/commit/928933c968466872b8617cf732f57b0decbac185))

## [13.2.1](https://gitlab.com/simonoscr/nixfiles/compare/13.2.0...13.2.1) (2024-12-02)

### 📦 Other

* update flake ([eaa1e10](https://gitlab.com/simonoscr/nixfiles/commit/eaa1e1085051c0b7300016ce51be165280f619a7))
* update flake ([5642e89](https://gitlab.com/simonoscr/nixfiles/commit/5642e8957742fc9653425b9f1ba60c23a4291c1c))

### 🛠 Fixes

* **hyprland:** enable plugins and update rules ([011a5f2](https://gitlab.com/simonoscr/nixfiles/commit/011a5f29bb1160683e84740c6a15d589924b99f2))

## [13.2.0](https://gitlab.com/simonoscr/nixfiles/compare/13.1.0...13.2.0) (2024-12-01)

### 📦 Other

* update flake ([f0dd100](https://gitlab.com/simonoscr/nixfiles/commit/f0dd1006626735ef99caaa2b89ccc53801687654))
* update flake ([a81ebd2](https://gitlab.com/simonoscr/nixfiles/commit/a81ebd2a8f77f5279c574a9ac483ea6d373de1ea))
* update flake ([810cf7b](https://gitlab.com/simonoscr/nixfiles/commit/810cf7be4d601f5660c959c60d92efb400c6dda3))
* update flake input ([b923aa4](https://gitlab.com/simonoscr/nixfiles/commit/b923aa470e8b309b97d043ab30db2ea1109f043a))
* **server:** cleanup and reinstall and update ([47564e0](https://gitlab.com/simonoscr/nixfiles/commit/47564e07e526400a718a3b50fcd233ae18dd7e56))
* **server:** cleanup and reinstall and update ([e715062](https://gitlab.com/simonoscr/nixfiles/commit/e715062a45086dd4d7b44e4b36d6526277cef9e4))

### 🚀 Features

* switch to nixos scheduler module instead of chaotic nyx ([74332a0](https://gitlab.com/simonoscr/nixfiles/commit/74332a0f8581c1929cf76f918615d9830aa644ba))
* **hyprland:** update to uwsm, adjust binds and settings ([4b671d3](https://gitlab.com/simonoscr/nixfiles/commit/4b671d3fa07f8bcdc25de6407cbe04f6368f38f4))

### 🛠 Fixes

* add extraGroups for user simon ([1badb1e](https://gitlab.com/simonoscr/nixfiles/commit/1badb1e825ef225a0ff28c271efd3043da6f0d36))
* add ingress to initial argocd ([3ad9b02](https://gitlab.com/simonoscr/nixfiles/commit/3ad9b029253f61cec7636ccdaa2b8389f0ab1c21))
* add ingress to initial argocd ([8553aac](https://gitlab.com/simonoscr/nixfiles/commit/8553aac3ec077982fb1fa82b86118235a2ab6f2d))
* add ingress to initial argocd ([a30fd81](https://gitlab.com/simonoscr/nixfiles/commit/a30fd81e58eef91c5077dc1e6609bceca720ccdb))
* add lact ([8c27a29](https://gitlab.com/simonoscr/nixfiles/commit/8c27a29b99269ce72ccfa88c438a488742060731))
* add nh option hm ([c656972](https://gitlab.com/simonoscr/nixfiles/commit/c6569724d07d1cfb1cc94da6cb6d163d528ef61e))
* add systemd service for kdeconnect ([0d8eebd](https://gitlab.com/simonoscr/nixfiles/commit/0d8eebdc08952def51b006c683db100029e4371d))
* add systemd services for diff services ([6de8212](https://gitlab.com/simonoscr/nixfiles/commit/6de8212d09656c56fee7f6a8c02584b543ea37e5))
* enable all firmware ([41e5120](https://gitlab.com/simonoscr/nixfiles/commit/41e51207c1f7a0e52b8ea818ce96c8ecd7ce5ea6))
* remove further docs to reduce size ([fbee36d](https://gitlab.com/simonoscr/nixfiles/commit/fbee36d4b42f1c58adc9709d36dc103b19782d2d))
* switch nh to hm module ([3f16187](https://gitlab.com/simonoscr/nixfiles/commit/3f161873dbe3ed788ebf2e611e3091af0b4ad12d))
* update audio lowlatency settings ([df5b2e8](https://gitlab.com/simonoscr/nixfiles/commit/df5b2e8cbcbcf418ed5d5de8a0fea60db53c773e))
* update fwupd ([9d81b40](https://gitlab.com/simonoscr/nixfiles/commit/9d81b40470dae7f0ea0652b9510137d9b854d06a))
* update gtk and qt themes and cursor ([3d5152b](https://gitlab.com/simonoscr/nixfiles/commit/3d5152b8a3d4225978e78db04126d866e8ac5fde))
* update locale ([f7edcab](https://gitlab.com/simonoscr/nixfiles/commit/f7edcab2b779454ed6fedb18e53ac9ead7dfb55f))
* update lsp and config ([a19c68a](https://gitlab.com/simonoscr/nixfiles/commit/a19c68a1d34c1b234dca7c488d36a44715012daa))
* **amd:** update REDV_PERFTEST vars ([27015a6](https://gitlab.com/simonoscr/nixfiles/commit/27015a688eeeea4636328a68b98befe7d8ddd5d3))
* **audio:** update lowlatency configs ([16675db](https://gitlab.com/simonoscr/nixfiles/commit/16675db501b4de48a449875ac2d8ee0393447af3))
* **boot:** adjust supported fs ([1e1e50d](https://gitlab.com/simonoscr/nixfiles/commit/1e1e50dc472bd9914165c2d19143e12baa7cc3ea))
* **corectrl:** disable ([109c366](https://gitlab.com/simonoscr/nixfiles/commit/109c366bd5d6610f67327ede91ff6ea2740be42b))
* **fonts:** update nerdfonts ns ([82326ab](https://gitlab.com/simonoscr/nixfiles/commit/82326ab7d5b6f738839b94d6767ef67f0e59b719))
* **gmaing:** remove extraPkgs, setup gamescope and add tweaks ([9ca4736](https://gitlab.com/simonoscr/nixfiles/commit/9ca4736eb6150a2d342e3ad2677ba38639725485))
* **gnome-services:** remove settings daemon ([00759e4](https://gitlab.com/simonoscr/nixfiles/commit/00759e44e10664b7213d416a997fb7a3aaa628a6))
* **greetd:** enable autologin and start hyprland-uwsm setup ([eebfa16](https://gitlab.com/simonoscr/nixfiles/commit/eebfa16850afd2445427ff44b9afb0457b3f221c))
* **hayprland:** enable withUWSM key ([29a3ef7](https://gitlab.com/simonoscr/nixfiles/commit/29a3ef7cc3335f99806f05116ce26799b6a3771a))
* **hyprpanel:** add systemd services with uwsm ([2a5ea42](https://gitlab.com/simonoscr/nixfiles/commit/2a5ea42f011a5742059612c8b376762bb143d32c))
* **k8s:** update ([de2ef7a](https://gitlab.com/simonoscr/nixfiles/commit/de2ef7a8e25756c8a6f90440d6dc14e4ff64ccc3))
* **network:** random mac address ([4001b77](https://gitlab.com/simonoscr/nixfiles/commit/4001b7785fbc518a64b79d5c39d68dbff10caf0f))
* **pkgs:** cleasnup unsued pkgs ([a7d2a8c](https://gitlab.com/simonoscr/nixfiles/commit/a7d2a8c0c43db1413350f62c865f38c97c04f5ae))
* **power:** set cpuFreq to performance ([35d5385](https://gitlab.com/simonoscr/nixfiles/commit/35d53857ede3e84ebb8295da76e68bceb93967e9))
* **xdg:** set common default portal to * ([624baac](https://gitlab.com/simonoscr/nixfiles/commit/624baacfa9e0bb65a72f00574bcdb57361de4cdc))

## [13.1.0](https://gitlab.com/simonoscr/nixfiles/compare/13.0.0...13.1.0) (2024-11-26)

### 📔 Docs

* update README ([9f2ff7a](https://gitlab.com/simonoscr/nixfiles/commit/9f2ff7ac1c6fba319bb98909788d0ecc55c3c92c))

### 📦 Other

* update flake ([45d6b70](https://gitlab.com/simonoscr/nixfiles/commit/45d6b70a58f7e0a8b9ac76201d440a7bdae381be))
* update flake ([b29d306](https://gitlab.com/simonoscr/nixfiles/commit/b29d30601b80f721382a4f126cbd7be3a20c34fb))
* update flkae inputs ([a3845b4](https://gitlab.com/simonoscr/nixfiles/commit/a3845b4413d8929822bbed93500417a31a83e2b6))

### 🚀 Features

* replace hyprland systemd with uwsm ([1125949](https://gitlab.com/simonoscr/nixfiles/commit/1125949191fa03fc8c200a36d83a57dd16cb541f))

### 🛠 Fixes

* enable bluetooth ([c9fa869](https://gitlab.com/simonoscr/nixfiles/commit/c9fa869ffd973b4b332055408bfe57d5ef40c609))
* enable yazi again ([8b38a42](https://gitlab.com/simonoscr/nixfiles/commit/8b38a425808befe343240e913868456d8c129560))
* remove ags aylur dots ([a65c459](https://gitlab.com/simonoscr/nixfiles/commit/a65c459ac755a179697a6170050cb6eaf5a9d558))
* update amdgpu envs ([85d2b88](https://gitlab.com/simonoscr/nixfiles/commit/85d2b88c5e7c22f411ffeafd9a49ed9e211f60a8))
* update network configuration ([88c5577](https://gitlab.com/simonoscr/nixfiles/commit/88c55775b7a95923d2f68ec07b40828f2b822ce9))
* update teamspeak 3 pkgs name ([eb52d59](https://gitlab.com/simonoscr/nixfiles/commit/eb52d5913d466f1d7e042f1d1681ef0d0520264a))
* update to LTO cachyos kernel and use rustland sched-ext scheduler ([de9c0fc](https://gitlab.com/simonoscr/nixfiles/commit/de9c0fc6b193537c3c575eacd2938d709cc18baf))

## [13.0.0](https://gitlab.com/simonoscr/nixfiles/compare/12.1.0...13.0.0) (2024-11-08)

### 🧨 Breaking changes!

* **k8s:** add k3s deployment, gitops, initial helm-charts etc ([2c525ef](https://gitlab.com/simonoscr/nixfiles/commit/2c525efb12a11093849af20c4aa3a54b4b4a77b9))

### 🚀 Features

* add colmena deployment ([d553c80](https://gitlab.com/simonoscr/nixfiles/commit/d553c808fc7a1ae3d485abb3506884e98267c35f))

### 🛠 Fixes

* update hyprland config ([3218301](https://gitlab.com/simonoscr/nixfiles/commit/32183015fcfb4f8c1eae283e2ad7379c771c9f6c))
* update scheduler ([84baf5b](https://gitlab.com/simonoscr/nixfiles/commit/84baf5bed7b51281c84cdb9587f10f80a461d982))
* **server:** argocd for server ([5094a3b](https://gitlab.com/simonoscr/nixfiles/commit/5094a3b22865f4f2a013386aef55b59e6e732a9f))

## [12.1.0](https://gitlab.com/simonoscr/nixfiles/compare/12.0.0...12.1.0) (2024-11-04)

### 📦 Other

* update flake ([f7a72fa](https://gitlab.com/simonoscr/nixfiles/commit/f7a72fa0fcdf168d30b279c3590d739697ffeef6))

### 🚀 Features

* add k8s cluster ([1c7a42f](https://gitlab.com/simonoscr/nixfiles/commit/1c7a42f00c3e463efcd71aa8c79b9ab2e2f76f60))
* add nixosConfigurations for k8s cluster and use default-systems because of arm arch ([7be45a5](https://gitlab.com/simonoscr/nixfiles/commit/7be45a59741a7f7e431264f0fc0faa3e4dbb075f))
* enable crosscompile with binfmt for aarch64 ([2a03334](https://gitlab.com/simonoscr/nixfiles/commit/2a033342b333acd6c7d389eb45ad3d99577d80dc))
* update server deployment ([6ce7737](https://gitlab.com/simonoscr/nixfiles/commit/6ce7737cc608661a486d566ab5567248e18b969a))

### 🛠 Fixes

* disable broken packages for now ([d068e9e](https://gitlab.com/simonoscr/nixfiles/commit/d068e9e785a5dac63c859346a36114ee8eaa1fa3))
* update template ([a3fab87](https://gitlab.com/simonoscr/nixfiles/commit/a3fab87e81fa32c1a0b81ee7e4b68f2d78914be1))

## [12.0.0](https://gitlab.com/simonoscr/nixfiles/compare/11.3.2...12.0.0) (2024-11-01)

### 🧨 Breaking changes!

* **deps:** update dependency @types/node to v22 ([cbd0173](https://gitlab.com/simonoscr/nixfiles/commit/cbd017375524c3e9df6984b11aa9a64c021d755e))
* **deps:** update dependency @typescript-eslint/eslint-plugin to v8 ([d1190a3](https://gitlab.com/simonoscr/nixfiles/commit/d1190a37c53220db652544f8cf0441d114672a8c))

## [11.3.2](https://gitlab.com/simonoscr/nixfiles/compare/11.3.1...11.3.2) (2024-11-01)

### 📦 Other

* update flake ([2a6176f](https://gitlab.com/simonoscr/nixfiles/commit/2a6176f10d66a850cbbebb103d83cc4aaf1937c0))

### 🦊 CI/CD

* update pipeline template file path ([19feea1](https://gitlab.com/simonoscr/nixfiles/commit/19feea1f64aedce696cfb00038f0d669a764efbf))

### 🛠 Fixes

* dont verbose pre-commit-hooks ([c06c153](https://gitlab.com/simonoscr/nixfiles/commit/c06c153ea1542acb148e5cfb94a951e118e015c0))

## [11.3.1](https://gitlab.com/simonoscr/nixfiles/compare/11.3.0...11.3.1) (2024-11-01)

### 📦 Other

* update flake ([5332f81](https://gitlab.com/simonoscr/nixfiles/commit/5332f8155e3cc0dcdf29f551041c3068666a5242))

### 🛠 Fixes

* allowed ip ranges for kde-connect ([d05401e](https://gitlab.com/simonoscr/nixfiles/commit/d05401e8aebdd36b2f01375cea86df9072292e64))
* use hyprlock login instead of greetd ([6c85a63](https://gitlab.com/simonoscr/nixfiles/commit/6c85a6330236a714df87215a612f3f2bb532098b))

## [11.3.0](https://gitlab.com/simonoscr/nixfiles/compare/11.2.0...11.3.0) (2024-10-31)

### 📦 Other

* update flake ([57ada21](https://gitlab.com/simonoscr/nixfiles/commit/57ada211a5b401d4f1305e350ec6d1fc9d1a0aca))
* update flake ([2863c5b](https://gitlab.com/simonoscr/nixfiles/commit/2863c5bc256a37f49089fe73ef8df51656c81d45))

### 🚀 Features

* add and enable hyprpanel ([924bc34](https://gitlab.com/simonoscr/nixfiles/commit/924bc3407f8211d5d8207bbaa616a8ca0cc5c99b))
* add anyrun ([31152cd](https://gitlab.com/simonoscr/nixfiles/commit/31152cd38807fce60fa44733ae21e093ca00c5cc))

### 🛠 Fixes

* make it possible to chose bars witrh module definition ([7c68105](https://gitlab.com/simonoscr/nixfiles/commit/7c68105a532d5b6678a907f5b0fc3050b72a561d))

## [11.2.0](https://gitlab.com/simonoscr/nixfiles/compare/11.1.3...11.2.0) (2024-10-29)

### 📦 Other

* **flake:** update flake.lock ([1051662](https://gitlab.com/simonoscr/nixfiles/commit/105166254228563ea4f35556aac11d61d7949c95))

### 🚀 Features

* add initial ags bar ([eb4c959](https://gitlab.com/simonoscr/nixfiles/commit/eb4c959a7fd559324f0e17b8b5d8b03e69ba9944))

### 🛠 Fixes

* add templates ([3dde146](https://gitlab.com/simonoscr/nixfiles/commit/3dde1467455a84b20e5d9c2a01292a8582df2ae2))
* removed asusctl ([a85d246](https://gitlab.com/simonoscr/nixfiles/commit/a85d24611d9b3e339e2ed09d939dfba1e355e402))
* update ags and hyprland shoprtcuts for ags ([b4546a0](https://gitlab.com/simonoscr/nixfiles/commit/b4546a0be2b68f8e660a5653f4184b1fe78f9215))

## [11.1.3](https://gitlab.com/simonoscr/nixfiles/compare/11.1.2...11.1.3) (2024-10-27)

### 🛠 Fixes

* last working commit ([eb627e8](https://gitlab.com/simonoscr/nixfiles/commit/eb627e88065b291be36f94d50deb889ce2d2e246))

## [11.1.2](https://gitlab.com/simonoscr/nixfiles/compare/11.1.1...11.1.2) (2024-10-27)

### 🛠 Fixes

* **checks:** lint ([ce794b1](https://gitlab.com/simonoscr/nixfiles/commit/ce794b100ea227cf5bf5febfe37db78bbb823894))

## [11.1.1](https://gitlab.com/simonoscr/nixfiles/compare/11.1.0...11.1.1) (2024-10-27)

### 🛠 Fixes

* **checks:** make separate checks dir ([474cdfd](https://gitlab.com/simonoscr/nixfiles/commit/474cdfd8285718250d47fb5500691f2a4a8cf12b))
* **checks:** make separate checks dir ([e238cff](https://gitlab.com/simonoscr/nixfiles/commit/e238cff9569d06b506b85c2fbbd25f35ce5bf5c7))

## [11.1.0](https://gitlab.com/simonoscr/nixfiles/compare/11.0.2...11.1.0) (2024-10-27)

### 🚀 Features

* **templates:** added templates to flake and be able tro selfreference them ([e35ac29](https://gitlab.com/simonoscr/nixfiles/commit/e35ac29cabaf786553dbaa3233ca7dce98c8e008))

## [11.0.2](https://gitlab.com/simonoscr/nixfiles/compare/11.0.1...11.0.2) (2024-10-27)

### 📦 Other

* **flake:** update flake.lock ([e75caff](https://gitlab.com/simonoscr/nixfiles/commit/e75cafffa2ba2fb40e53b7163a390fa1b14d2696))

### 🛠 Fixes

* **llm:** enable ollama and open-webui again ([d41a62f](https://gitlab.com/simonoscr/nixfiles/commit/d41a62f785877233f75328c61b36b464dc768529))
* **zed-editor:** update and restructure settings ([2b14a8e](https://gitlab.com/simonoscr/nixfiles/commit/2b14a8e3555ca97dab8d82f49f3ea7623f10dbe8))

## [11.0.1](https://gitlab.com/simonoscr/nixfiles/compare/11.0.0...11.0.1) (2024-10-26)

### 📦 Other

* **flake:** update flake.lock ([757f1a5](https://gitlab.com/simonoscr/nixfiles/commit/757f1a5f3bfcb7834db8c2c9444c377d5fa8dbf3))
* **flake:** update flake.lock ([ef91dce](https://gitlab.com/simonoscr/nixfiles/commit/ef91dcee9a7c34b539eda19918de96dc3c6629f2))

### 🛠 Fixes

* add kde-connect ([eb4c28e](https://gitlab.com/simonoscr/nixfiles/commit/eb4c28ee7eb377b7451058b49abf387f47e63b2c))
* rename fonts, add pkgs ([cf7d865](https://gitlab.com/simonoscr/nixfiles/commit/cf7d8658a1c3963b2eb29f258baa67a1d776bbce))
* **gamescope:** remove default args ([ccbba85](https://gitlab.com/simonoscr/nixfiles/commit/ccbba85619f7ee59e6d29a5647f48db6c60c20d0))
* **gaming:** set more ENV for star-citizen ([545749f](https://gitlab.com/simonoscr/nixfiles/commit/545749f881db7b164983e6557a44a1a4bb0b83d1))

## [11.0.0](https://gitlab.com/simonoscr/nixfiles/compare/10.5.1...11.0.0) (2024-10-19)

### 🧨 Breaking changes!

* making everything a module in home-manager WIP ([56a0715](https://gitlab.com/simonoscr/nixfiles/commit/56a0715f81c4adc20f709fe0700da0ca2a1f5144))

## [10.5.1](https://gitlab.com/simonoscr/nixfiles/compare/10.5.0...10.5.1) (2024-10-19)

### 📦 Other

* **flake:** update flake.lock ([5b1991f](https://gitlab.com/simonoscr/nixfiles/commit/5b1991f4477c8377caf7c0f481b7751c572377c1))

### 🛠 Fixes

* **zed-editor:** use hm module ([773ddac](https://gitlab.com/simonoscr/nixfiles/commit/773ddac54053bc4f5bdd42524e250a3711d4b48f))

## [10.5.0](https://gitlab.com/simonoscr/nixfiles/compare/10.4.1...10.5.0) (2024-10-18)

### 📦 Other

* **flake:** add more nixpkgs inputs ([1f9575f](https://gitlab.com/simonoscr/nixfiles/commit/1f9575ffcf1e2384bed90a9e31aab1c4c1c393b1))
* **flake:** update flake ([b42f568](https://gitlab.com/simonoscr/nixfiles/commit/b42f568202e3a05538a1cc5925b1c644ec104dde))
* **flake:** update flake ([bc3b4cd](https://gitlab.com/simonoscr/nixfiles/commit/bc3b4cdd3c379dd94ecb32eb3ca76e4ec4583ea8))
* **flake:** update flake.lock ([aed5220](https://gitlab.com/simonoscr/nixfiles/commit/aed5220072fe19634f4485ab6fec92152f9715d5))

### 🚀 Features

* add templates ([ee2724e](https://gitlab.com/simonoscr/nixfiles/commit/ee2724e6cfdde173e6c1b3c3d8b9ded2d430b4a9))
* **overlays:** add nixpkgs stable, git and unstable inputs as overlays ([7571b30](https://gitlab.com/simonoscr/nixfiles/commit/7571b30f894ea64b0d8faf408cc998d331e4f388))

### 🛠 Fixes

* **nix:** add comments and update system-features ([62813a5](https://gitlab.com/simonoscr/nixfiles/commit/62813a5bae6c8a2ac7eaf02e55a9a7ff90e40a7b))
* **pkgs:** disable broken packages ([ea17ba5](https://gitlab.com/simonoscr/nixfiles/commit/ea17ba5e760d357ca8df3d60e17d657e90d6eec3))
* **zed:** update config ([db92f67](https://gitlab.com/simonoscr/nixfiles/commit/db92f67360f9fca6b4dd18705647a9d63f7087bf))

## [10.4.1](https://gitlab.com/simonoscr/nixfiles/compare/10.4.0...10.4.1) (2024-10-13)

### 🛠 Fixes

* **pkgs:** disable unused pkgs ([3941f53](https://gitlab.com/simonoscr/nixfiles/commit/3941f53022ca404aedc98c63c7c1021c23ca5ff6))

## [10.4.0](https://gitlab.com/simonoscr/nixfiles/compare/10.3.1...10.4.0) (2024-10-13)

### 📦 Other

* **flake:** update flake ([cafa126](https://gitlab.com/simonoscr/nixfiles/commit/cafa1262be4f2c0a03f4ac0c202c2ff20b0c1bbd))
* **flake:** update flake ([9bf9e1c](https://gitlab.com/simonoscr/nixfiles/commit/9bf9e1ce313cb77ae606c31f30376d8d8b5676f9))
* **flake:** update flake ([c10cbdc](https://gitlab.com/simonoscr/nixfiles/commit/c10cbdc6e1eea2bc08cedab94e3cb3b46af0e733))

### 🚀 Features

* **gaming:** add star-citizen-umu from nix-gaming ([617dd2e](https://gitlab.com/simonoscr/nixfiles/commit/617dd2e53eff57ced6b76b1f48f261df16a72240))

## [10.3.1](https://gitlab.com/simonoscr/nixfiles/compare/10.3.0...10.3.1) (2024-10-12)

### 🛠 Fixes

* **gaming:** disable star-citizen pkgs because its running fine through steam ([b0a5897](https://gitlab.com/simonoscr/nixfiles/commit/b0a5897fafd2f2f5d214af601d2a53f6d306eafe))

## [10.3.0](https://gitlab.com/simonoscr/nixfiles/compare/10.2.1...10.3.0) (2024-10-12)

### 📦 Other

* **flake:** update flake ([4ef8c5a](https://gitlab.com/simonoscr/nixfiles/commit/4ef8c5ad93afbc9c26efe48f33084bb5e8cba687))
* **flake:** update flake ([bb6ca2f](https://gitlab.com/simonoscr/nixfiles/commit/bb6ca2f8962055024c58f9e2085b1bfaab13c489))

### 🚀 Features

* **kernel:** dont use LTO kernel because of missing moduels and set scheduler to scx_rustland ([3b8ea89](https://gitlab.com/simonoscr/nixfiles/commit/3b8ea89b9bdc2a3519a50451097cc853cda03e41))

### 🛠 Fixes

* **gaming:** add star-citizen from nix-gaming ([70a9427](https://gitlab.com/simonoscr/nixfiles/commit/70a94277a135428327cf55d91ddc0195a5f99e9a))
* **hyprland:** remove element-desktop from autostart ([2e373a6](https://gitlab.com/simonoscr/nixfiles/commit/2e373a6b054101fd1066125fd4e611dcf9763656))
* **hyprland:** update flake input and autostart program ([adc9e15](https://gitlab.com/simonoscr/nixfiles/commit/adc9e15ac9e09005190781c729a7cbb7cd1a6e61))
* **pkgs:** add element-desktop ([6c10181](https://gitlab.com/simonoscr/nixfiles/commit/6c101819785e9c93e47b2a056d7603f8e09eaa53))

## [10.2.1](https://gitlab.com/simonoscr/nixfiles/compare/10.2.0...10.2.1) (2024-10-11)

### 🛠 Fixes

* yubiokey login, xdg, and update flake ([0aa5209](https://gitlab.com/simonoscr/nixfiles/commit/0aa5209d26bc1c640af20d00f83ed6fc272df4e2))

## [10.2.0](https://gitlab.com/simonoscr/nixfiles/compare/10.1.1...10.2.0) (2024-10-11)

### 📦 Other

* **flake:** update flake ([74ee294](https://gitlab.com/simonoscr/nixfiles/commit/74ee294544a85767dc4c115a118c3797a626bbe7))

### 🚀 Features

* setup gpg and ssh correctly for yubikey ([9cf387c](https://gitlab.com/simonoscr/nixfiles/commit/9cf387c6078280dd2f39b5ea0a3352d22185e251))

## [10.1.1](https://gitlab.com/simonoscr/nixfiles/compare/10.1.0...10.1.1) (2024-10-08)

### 📦 Other

* **flake:** update flake ([27cdf1f](https://gitlab.com/simonoscr/nixfiles/commit/27cdf1fe3b218701a6f0f85d8cfff91a32c90c49))
* **flake:** update flake ([c90ed19](https://gitlab.com/simonoscr/nixfiles/commit/c90ed19bafedc9591e530628e6f6c53407a4f0a1))

### 🛠 Fixes

* **ags:** busName for ags to hypr to prvent double starting with hyprland ([2e9a3ba](https://gitlab.com/simonoscr/nixfiles/commit/2e9a3bafc8b417a74a640e7e27b3613b4ac0f3e5))
* **flake:** move zen-browser-flake input from github to gitlab ([b98f459](https://gitlab.com/simonoscr/nixfiles/commit/b98f459c9d7995cfcce4474c9250dafbe58568c3))

## [10.1.0](https://gitlab.com/simonoscr/nixfiles/compare/10.0.1...10.1.0) (2024-10-08)

### 📦 Other

* **flake:** update flake ([35b84fd](https://gitlab.com/simonoscr/nixfiles/commit/35b84fd0f9db65b34df68bc55464afbfb0fb8964))

### 🚀 Features

* restructure secrets and prepare agenix ([5e2de8e](https://gitlab.com/simonoscr/nixfiles/commit/5e2de8ee681c53147d8bef5b959ecfddfc4b55ff))

### 🛠 Fixes

* **flake:** add agenix input ([d61063d](https://gitlab.com/simonoscr/nixfiles/commit/d61063dc0d10b318aac5310d74711cf3f1581325))
* **nix:** use secret instead of rendered template for nix-access-token to prevent github rate-limit ([3d9f9f8](https://gitlab.com/simonoscr/nixfiles/commit/3d9f9f806cf7cb5632904ab50251680fa1662d7d))
* **ssh:** enable keytoagent ([102aa56](https://gitlab.com/simonoscr/nixfiles/commit/102aa56df617e088b906ccdd47a61ff7ca4a8120))

## [10.0.1](https://gitlab.com/simonoscr/nixfiles/compare/10.0.0...10.0.1) (2024-10-04)

### 📔 Docs

* update README ([163c07a](https://gitlab.com/simonoscr/nixfiles/commit/163c07a9545cbce7646bcc3b5cf8aa433a88ac38))

### 📦 Other

* **flake:** update flake ([a75120a](https://gitlab.com/simonoscr/nixfiles/commit/a75120abf78a22d2b7f25db3e46e76d2fb235855))

### 🛠 Fixes

* delete desktop.iso ([b6de985](https://gitlab.com/simonoscr/nixfiles/commit/b6de985c3056833a024d2b41ea3dc7f9ea7b9fdb))
* **hyprland:** enable plugins and update settings and packages ([810807f](https://gitlab.com/simonoscr/nixfiles/commit/810807f0e94ea31162808d0028e5af85ff076105))
* **images:** added different image templates ([6d92f46](https://gitlab.com/simonoscr/nixfiles/commit/6d92f4645242e5801eac4ada69bed39ab60fb8d5))
* **pkgs:** add pkg ([c37747a](https://gitlab.com/simonoscr/nixfiles/commit/c37747a872b6421d805b1b473a45bf4c1ec595a0))
* **security:** setup yubikey and u2f_keys corectly finally ([45660a7](https://gitlab.com/simonoscr/nixfiles/commit/45660a79075ad0223e4768adb13d0818a68488f1))
* **xdg:** update mimeApps ([3253ca9](https://gitlab.com/simonoscr/nixfiles/commit/3253ca9209d809e7ceb339732c1452a846fcc240))

## [10.0.0](https://gitlab.com/simonoscr/nixfiles/compare/9.5.2...10.0.0) (2024-10-03)

### 📦 Other

* lint ([931578f](https://gitlab.com/simonoscr/nixfiles/commit/931578f537216799d72066c4f1c840068c0c372e))
* use variables for locations ([e60b78c](https://gitlab.com/simonoscr/nixfiles/commit/e60b78c5ed6f07865be18a2b28efbf52d7f40bbb))
* **flake:** update flake ([726f4bf](https://gitlab.com/simonoscr/nixfiles/commit/726f4bf9392501b51bf7445b0a4ad1fc72ebdade))

### 🧨 Breaking changes!

* **flake:** removed flake-parts and moved to nix-system ([d05a802](https://gitlab.com/simonoscr/nixfiles/commit/d05a8027202bf5934962bdbc7e0f92cca88befa6))

### 🚀 Features

* **vim:** removed nixvim and use just neovim ([4a3b8eb](https://gitlab.com/simonoscr/nixfiles/commit/4a3b8eb9520cf3d4c66c3faa9f60ff6a95a2e943))

### 🛠 Fixes

* **starship:** update prompt settings ([67c7716](https://gitlab.com/simonoscr/nixfiles/commit/67c7716cd5fffc43830b70e2ce682cb36359b958))
* **zed:** update editor settings ([a09fea0](https://gitlab.com/simonoscr/nixfiles/commit/a09fea006519c5f9090a3f845481d56ab87b8763))

## [9.5.2](https://gitlab.com/simonoscr/nixfiles/compare/9.5.1...9.5.2) (2024-10-03)

### 🛠 Fixes

* **zed:** fixed line wrapping and server url ([38c6502](https://gitlab.com/simonoscr/nixfiles/commit/38c650259cd1adbdfb41cca8e5d0624f0e23f9f9))

## [9.5.1](https://gitlab.com/simonoscr/nixfiles/compare/9.5.0...9.5.1) (2024-10-03)

### 📦 Other

* **flake:** update flake ([c68a4dd](https://gitlab.com/simonoscr/nixfiles/commit/c68a4dddd220c34231713278521afe71a6547db5))

### 🛠 Fixes

* **browser:** add brave as backup chromium browser ([d30bbc8](https://gitlab.com/simonoscr/nixfiles/commit/d30bbc8d8f41578eb132271abc7c705e0d45a687))
* **game:** try to get gamescope working with steam ([760b583](https://gitlab.com/simonoscr/nixfiles/commit/760b5830c7ddc5fa7f424c56d8b33652cfc79163))
* **hyprland:** enable plugins ([0740977](https://gitlab.com/simonoscr/nixfiles/commit/0740977f65fec6ef07bf1f856457b0d427f04234))

## [9.5.0](https://gitlab.com/simonoscr/nixfiles/compare/9.4.0...9.5.0) (2024-10-02)

### 📦 Other

* **flake:** update flake inputs ([8ca5394](https://gitlab.com/simonoscr/nixfiles/commit/8ca53946b823ce38298524d5713cac93575c4e96))

### 🚀 Features

* **vm:** vagrant for spinning up vms to test ansible playbooks ([77f49cb](https://gitlab.com/simonoscr/nixfiles/commit/77f49cb915fda858be48a1a9063e9b69774acb3e))

## [9.4.0](https://gitlab.com/simonoscr/nixfiles/compare/9.3.4...9.4.0) (2024-10-01)

### 📦 Other

* **flake:** update flake ([cc8c93c](https://gitlab.com/simonoscr/nixfiles/commit/cc8c93ccc83ab987d214dbeede1821daeaa97665))
* **flake:** update flake ([a219a3a](https://gitlab.com/simonoscr/nixfiles/commit/a219a3a08320c5a83a9926a660ab1d130627795e))
* **flake:** update flake ([1df7e61](https://gitlab.com/simonoscr/nixfiles/commit/1df7e6127376dc66954f45983d2b4d7f79f3f307))
* **flake:** update flake inputs ([0f34c5b](https://gitlab.com/simonoscr/nixfiles/commit/0f34c5b43c3a842c9732b1f94c90586880dd84fc))

### 🚀 Features

* add fish to shells ([fab4e75](https://gitlab.com/simonoscr/nixfiles/commit/fab4e753f1f8d810752c3aa014e1c1c1f39ddaf0))
* add zen browser ([27d61d6](https://gitlab.com/simonoscr/nixfiles/commit/27d61d6028d20b7fb7517e0d00912a84e6f4cb2f))
* **shell:** add fish, update settings, update default shell to zsh for all users and override for specific user to fish ([f6ee2b0](https://gitlab.com/simonoscr/nixfiles/commit/f6ee2b090e2254ac6196c8c348c27f4dabba1280))

### 🛠 Fixes

* update miscs configurations ([3ffeb71](https://gitlab.com/simonoscr/nixfiles/commit/3ffeb71e574591dc510a35c5ef4d96e41d04157b))
* **firefox:** settings and default ([49429ea](https://gitlab.com/simonoscr/nixfiles/commit/49429ea30aeae883652bc1324f737ebe788b134a))
* **home:** move sessionVariables to home.nix and make zen default browser ([de48af2](https://gitlab.com/simonoscr/nixfiles/commit/de48af214a55c31056222a0dbd9c52daa0dcb804))
* **server:** remove deprecated option noXlibs ([78dbd33](https://gitlab.com/simonoscr/nixfiles/commit/78dbd33a78bf63976fab3bd94ec4f1fdee6ce23f))
* **zed-editor:** dmove integrated terminal to bottom ([2b0e981](https://gitlab.com/simonoscr/nixfiles/commit/2b0e981d11acb30716d302b65eda321251041d4d))
* **zed-editor:** update settings ([1be4505](https://gitlab.com/simonoscr/nixfiles/commit/1be45051e7205353c6481d1788f989702aada3d7))
* **zed:** update config, lsp etc ([4009417](https://gitlab.com/simonoscr/nixfiles/commit/4009417683ed8d4731d04639b006bdff2219f734))

## [9.3.4](https://gitlab.com/simonoscr/nixfiles/compare/9.3.3...9.3.4) (2024-09-20)

### 🛠 Fixes

* **zed:** update config ([86cda71](https://gitlab.com/simonoscr/nixfiles/commit/86cda713ebfd931d24408fd46a6312f60c3c8889))

## [9.3.3](https://gitlab.com/simonoscr/nixfiles/compare/9.3.2...9.3.3) (2024-09-19)

### 📦 Other

* **flake:** update flake ([d93579f](https://gitlab.com/simonoscr/nixfiles/commit/d93579f583e480cb95572b94db8a01208025aaef))

### 🦊 CI/CD

* fix typo in template file name ([ddf43f8](https://gitlab.com/simonoscr/nixfiles/commit/ddf43f8777384f20c7d0a44223cf3ff9e0b59ee1))
* use pipeline template ([149b250](https://gitlab.com/simonoscr/nixfiles/commit/149b2501093a8f0b2210b36fe55d7d72666c5b90))
* use pipeline template ([fd1b0f2](https://gitlab.com/simonoscr/nixfiles/commit/fd1b0f2d3f7d00cd3d1c64ce1ad81bee4226d38a))
* **deps:** update node.js to v22.9.0 ([1af00e6](https://gitlab.com/simonoscr/nixfiles/commit/1af00e6f1774f9ce3b87bd839b0febbf4ca7ac17))

### 🛠 Fixes

* **git:** add more git aliases ([b573f5b](https://gitlab.com/simonoscr/nixfiles/commit/b573f5b41b99c0e3f0e4c50dfff7f689e810dd2c))

## [9.3.2](https://gitlab.com/simonoscr/nixfiles/compare/9.3.1...9.3.2) (2024-09-15)

### 📦 Other

* add renovate.json ([89c7380](https://gitlab.com/simonoscr/nixfiles/commit/89c738019f898a442b5ea6485bd1ebb38bb25bc5))
* **flake:** update flake ([2af6c16](https://gitlab.com/simonoscr/nixfiles/commit/2af6c1672dd0e1253c6b02ef9b46014d8169c3bd))
* **flake:** update flake ([5403c01](https://gitlab.com/simonoscr/nixfiles/commit/5403c010e4f04813a82918a0252d4687ee3313bc))

### 🛠 Fixes

* remove teamspeak5 ([d92b334](https://gitlab.com/simonoscr/nixfiles/commit/d92b3347569d8ca571a34dad25d16c51464a24fd))
* **hyprland:** add ts3 shortcut ([77cc08b](https://gitlab.com/simonoscr/nixfiles/commit/77cc08bf4861014371e3d17f5577bd8b3083ac14))

## [9.3.1](https://gitlab.com/simonoscr/nixfiles/compare/9.3.0...9.3.1) (2024-09-14)

### 📦 Other

* **flake:** update flake ([4a5cea6](https://gitlab.com/simonoscr/nixfiles/commit/4a5cea6fed308cd6427b8fb9bd6a5752fda5b011))
* **lint:** renovate.json ([2176475](https://gitlab.com/simonoscr/nixfiles/commit/217647532f22163cda0a3c956fd386f7efa5d054))

### 🛠 Fixes

* **nix:** use latest nix version ([adfd656](https://gitlab.com/simonoscr/nixfiles/commit/adfd6569c38c7c43daa935b4e03982017074ccad))

## [9.3.0](https://gitlab.com/simonoscr/nixfiles/compare/9.2.2...9.3.0) (2024-09-11)

### 📦 Other

* **flake:** update flake ([c922f3b](https://gitlab.com/simonoscr/nixfiles/commit/c922f3b284633d224a5e6d303e0f9ca953d42c75))

### 🚀 Features

* **kernel:** switch to linux_cachyos-lto kernel ([62a2832](https://gitlab.com/simonoscr/nixfiles/commit/62a2832f52e207f06f2acb76fcf4a5a1ccd8abb8))

### 🛠 Fixes

* **hyprpanel:** update ([dde5528](https://gitlab.com/simonoscr/nixfiles/commit/dde55285d70d27fdf7cb2398ac5a83c0884e9916))
* **nix:** add chaotic nyx idk substituters ([a07f17b](https://gitlab.com/simonoscr/nixfiles/commit/a07f17bf68b6932c22a6eb25dbd517ec6b2d4a0e))

## [9.2.2](https://gitlab.com/simonoscr/nixfiles/compare/9.2.1...9.2.2) (2024-09-09)

### 📔 Docs

* **README:** remove checkboxes ([1f7fda4](https://gitlab.com/simonoscr/nixfiles/commit/1f7fda4e9cbeac7c70e0746be655514b3de0ef27))

### 📦 Other

* **flake:** update flake ([39b7ae5](https://gitlab.com/simonoscr/nixfiles/commit/39b7ae51e3cdaf879aeed427429cc50ad7f4312b))
* **flake:** update flake ([85393bc](https://gitlab.com/simonoscr/nixfiles/commit/85393bc5fd83cdd61fefca995454d1481037f984))

### 🛠 Fixes

* **ags:** update and enable systemd-service and remove ags from exec-once in hyprland ([e3a8bc9](https://gitlab.com/simonoscr/nixfiles/commit/e3a8bc938c07cd006600f376185788bf6f2e9f0a))

## [9.2.1](https://gitlab.com/simonoscr/nixfiles/compare/9.2.0...9.2.1) (2024-09-07)

### 📦 Other

* **lint:** cleanup ([47b1405](https://gitlab.com/simonoscr/nixfiles/commit/47b1405e1fab5930a4b05a5cda665ef65820b8bb))

### 🛠 Fixes

* **u2f:** fix settings keys ([a3594cd](https://gitlab.com/simonoscr/nixfiles/commit/a3594cdfa37e1b9a3c572cfe33463d1b025dbb1f))

## [9.2.0](https://gitlab.com/simonoscr/nixfiles/compare/9.1.5...9.2.0) (2024-09-07)

### 📦 Other

* **flake:** update flake ([ee30c93](https://gitlab.com/simonoscr/nixfiles/commit/ee30c9389ff2753417c9dad7c31f26fd7c88b544))
* **lint:** cleanup ([a57e9bd](https://gitlab.com/simonoscr/nixfiles/commit/a57e9bdbc9abed7ec2216247f5717b16629f7d5c))

### 🚀 Features

* **systemd:** disable oomd ([0c688de](https://gitlab.com/simonoscr/nixfiles/commit/0c688de2501fef618faca11c6244bd690d45ed81))

### 🛠 Fixes

* **amdgpu:** remove zenpower and modules ([d99ffcb](https://gitlab.com/simonoscr/nixfiles/commit/d99ffcb22ef12decf2a97370fa966939832cf228))
* **avahi:** disable ([2a661f0](https://gitlab.com/simonoscr/nixfiles/commit/2a661f002fd149d8bfaeae66a16a148da617fd73))
* **earlyoom:** refine service to kill and not to kill ([cd78b89](https://gitlab.com/simonoscr/nixfiles/commit/cd78b8996b414c733e80dfe68589d785fec11980))
* **gnome:** add disk utility ([b0678d0](https://gitlab.com/simonoscr/nixfiles/commit/b0678d0baaea8f784498d15b4607a957a465b1cb))
* **greetd:** remove [secure] StandardInput ([91e0293](https://gitlab.com/simonoscr/nixfiles/commit/91e029300468e87d44ef9cd469fbbda6183fc729))
* **hyprmode:** increase hz back to 165 again ([c7e9b20](https://gitlab.com/simonoscr/nixfiles/commit/c7e9b20eb087d7a45b783c12ee637d8b9d5f5c81))
* **network:** remove deprecated systctl commands; enable strict dnsovertls; iwd as wifi backend ([8afcfcd](https://gitlab.com/simonoscr/nixfiles/commit/8afcfcd2beb14404c32da0d232a403c085dc6543))
* **pipewire:** disable jack ([d478763](https://gitlab.com/simonoscr/nixfiles/commit/d47876395b4385ea8b03b0461874fb0d6cef2c43))
* **u2f:** cleanup yubikey setup and remove unused services and programs ([f7c0ece](https://gitlab.com/simonoscr/nixfiles/commit/f7c0ece7f2de8cf2790d355864b1ad198faca231))

## [9.1.5](https://gitlab.com/simonoscr/nixfiles/compare/9.1.4...9.1.5) (2024-09-07)

### 📦 Other

* **deps:** update node.js to v22.8.0 ([49bfb06](https://gitlab.com/simonoscr/nixfiles/commit/49bfb06b039111c30edf054f4aa253529afcccee))
* **flake:** update flake ([af418d6](https://gitlab.com/simonoscr/nixfiles/commit/af418d62985b01afc6cdfd494d1e11e8dcdf724f))
* **flake:** update flake ([5ca0262](https://gitlab.com/simonoscr/nixfiles/commit/5ca0262418abe9bf2de2dcde654709bdad31f394))

### 🛠 Fixes

* **gamescope:** disable cap_sys ([a7b5bc8](https://gitlab.com/simonoscr/nixfiles/commit/a7b5bc85dc8b51e14933c59f0743df86ba89d9bf))
* **login:** remove autologin for tuigreet and disable hyprlock on startup ([93691ad](https://gitlab.com/simonoscr/nixfiles/commit/93691ad8e71be7cf4c438688007ab1a91a4c9cec))
* **ollama:** update module options ([b7af414](https://gitlab.com/simonoscr/nixfiles/commit/b7af414ef3d3282665f26c6f9db7d5e4a67f750b))
* **zed:** remove themes and update config ([3618b3d](https://gitlab.com/simonoscr/nixfiles/commit/3618b3dafc652e5e5f007e2df4aa3663fc4aced4))

## [9.1.4](https://gitlab.com/simonoscr/nixfiles/compare/9.1.3...9.1.4) (2024-08-26)

### 📦 Other

* **flake:** update flake ([f1288bd](https://gitlab.com/simonoscr/nixfiles/commit/f1288bdc5d90d7a54f4802008524bce6c6459308))
* **flake:** update flake ([a3fc38f](https://gitlab.com/simonoscr/nixfiles/commit/a3fc38f804510d8f7e1dc6a7dfb6ce6c5a477876))
* **flake:** update flake ([a61b07f](https://gitlab.com/simonoscr/nixfiles/commit/a61b07f93c8717a7582f664decdbb9a65eca5a22))

### 🛠 Fixes

* **firefox:** update shyfox to 3.8.1 and user.js ([a968919](https://gitlab.com/simonoscr/nixfiles/commit/a9689199a2c6de2d15bbd0ebf1bee37a0a1942d6))
* **zed:** update config ([7d88a31](https://gitlab.com/simonoscr/nixfiles/commit/7d88a312a1beb38d2bb52902aac1b428ce2bd6da))

## [9.1.3](https://gitlab.com/simonoscr/nixfiles/compare/9.1.2...9.1.3) (2024-08-19)

### 💈 Style

* **lint:** apply deadnix and statix and add it also to pre-commit-hook ([fdd570a](https://gitlab.com/simonoscr/nixfiles/commit/fdd570a78632235069adbf471b667ac317d5f735))
* **lint:** apply linting ([1d9609f](https://gitlab.com/simonoscr/nixfiles/commit/1d9609f8982a70cb967083f139390eb8d1762ee8))

### 📦 Other

* **deps:** update node docker tag to v22.6.0 ([d9c2dd1](https://gitlab.com/simonoscr/nixfiles/commit/d9c2dd1e53c950f5ebb5cd98da471bda8117b128))
* **flake:** update flake ([f115102](https://gitlab.com/simonoscr/nixfiles/commit/f11510243545368d5214257535f83fc83977d8a7))
* **flake:** update flake ([5ce458c](https://gitlab.com/simonoscr/nixfiles/commit/5ce458cc273adf9fc91a546077d52c92dfbab786))
* **flake:** update flake ([13a8a47](https://gitlab.com/simonoscr/nixfiles/commit/13a8a47f4dadc9202918544918f05b073140ba23))
* **flake:** update flake ([4f331f2](https://gitlab.com/simonoscr/nixfiles/commit/4f331f25c15ad5f1a410dea4754d3b7d391f09c0))

### 🛠 Fixes

* **firefox:** update shyfox ([cfb4154](https://gitlab.com/simonoscr/nixfiles/commit/cfb41543490dafe5fd6af1ce2f4bbb687c4deaff))
* **hyprland:** update hyprland config ([ac4dec7](https://gitlab.com/simonoscr/nixfiles/commit/ac4dec7b6848da476595ebd0a15609720dc944b9))
* **hyprland:** update plugins and startup ([526e5e6](https://gitlab.com/simonoscr/nixfiles/commit/526e5e6ce01b49c071e728b2a0877c724ca38e5d))
* **hyprland:** use nixos module instead of upstream module ([d84dd6a](https://gitlab.com/simonoscr/nixfiles/commit/d84dd6a9b4a3afa15dab0b738406b1af6c8219da))
* **kitty:** enable kitty ([797d516](https://gitlab.com/simonoscr/nixfiles/commit/797d516e34e16f133bf7e6245404f5f3b69f7bdd))
* **llm:** enable ollama and open-webui again ([cad3ec1](https://gitlab.com/simonoscr/nixfiles/commit/cad3ec127e77fce8a9b757cdb9914fbfcde41903))

## [9.1.2](https://gitlab.com/simonoscr/nixfiles/compare/9.1.1...9.1.2) (2024-08-07)

### 📦 Other

* **flake:** update flake ([7e64b38](https://gitlab.com/simonoscr/nixfiles/commit/7e64b38157b5250781c3afd075ae491d66fd3725))
* **flake:** update flake ([2122e77](https://gitlab.com/simonoscr/nixfiles/commit/2122e772a60971db0d80257e8c547fa0e0da0931))

### 🛠 Fixes

* **qt:** theming why did i even remove this ([cbc7db2](https://gitlab.com/simonoscr/nixfiles/commit/cbc7db2708b06740fbab84a66e199ea601858b48))

## [9.1.1](https://gitlab.com/simonoscr/nixfiles/compare/9.1.0...9.1.1) (2024-08-06)

### 📦 Other

* **flake:** update flake ([003ee02](https://gitlab.com/simonoscr/nixfiles/commit/003ee0285fa59290d2e583b0718092a089c5a01b))

### 🛠 Fixes

* **fs:** remove deprecated feature ([5f61351](https://gitlab.com/simonoscr/nixfiles/commit/5f613515333f7a780fb0009739f29a76efadaf49))
* **hyprland:** update hyprland config and add hyprpanel ([054553e](https://gitlab.com/simonoscr/nixfiles/commit/054553e14d3591f1c5b754d2f24676bb352aa5d1))
* **pkgs:** add teamspeak5 and bitwarden-desktop ([b51c84d](https://gitlab.com/simonoscr/nixfiles/commit/b51c84df153bb835bf80a9b90924690edae63667))

## [9.1.0](https://gitlab.com/simonoscr/nixfiles/compare/9.0.5...9.1.0) (2024-08-03)

### 📦 Other

* **flake:** added chaotic nyx input ([3c9d6ae](https://gitlab.com/simonoscr/nixfiles/commit/3c9d6aecaef1cdf336f689da7e353172c2a1237a))
* **flake:** update flake ([d0245f0](https://gitlab.com/simonoscr/nixfiles/commit/d0245f021bbf82dc6f7c263599cf818e939aa921))

### 🚀 Features

* **kernel:** add and use cachyos kernel and adjust kernelParams and bootParams ([4b5de33](https://gitlab.com/simonoscr/nixfiles/commit/4b5de33bdc96b69a08989005422f3c27f8b8bdfc))

### 🛠 Fixes

* **firefox:** add simplelogin extension ([a666008](https://gitlab.com/simonoscr/nixfiles/commit/a666008a33b4fbd7706291dceb1c91d93010d583))
* **hyprland:** update config ([c94ea8b](https://gitlab.com/simonoscr/nixfiles/commit/c94ea8b34a686dec39d19198742437a5709fca0d))
* **zed:** updae theme, settings and config ([108118a](https://gitlab.com/simonoscr/nixfiles/commit/108118ae281c361755ec55dc7aba92781949fb0a))

## [9.0.5](https://gitlab.com/simonoscr/nixfiles/compare/9.0.4...9.0.5) (2024-07-31)

### 🛠 Fixes

* **zed:** split up settings, theme and config ([fdaf185](https://gitlab.com/simonoscr/nixfiles/commit/fdaf185bd4904c12698f9fdbd57a22987a603577))
* **zed:** split up settings, theme and config ([87b7bab](https://gitlab.com/simonoscr/nixfiles/commit/87b7babc2ada85396b92ef2d856cb139c63ef3aa))

## [9.0.4](https://gitlab.com/simonoscr/nixfiles/compare/9.0.3...9.0.4) (2024-07-31)

### 📦 Other

* **flake:** update flake ([7d850ef](https://gitlab.com/simonoscr/nixfiles/commit/7d850ef3d3b5ce8dd1d2cece0783aa1635dc1e1c))

### 🛠 Fixes

* **kernel:** dont use custom kernel ([99b0d69](https://gitlab.com/simonoscr/nixfiles/commit/99b0d692e2e762938ea25d995307d838334f40f7))
* **nix:** try access-token again to prevent api rate limit DOESNT WORK ([2928588](https://gitlab.com/simonoscr/nixfiles/commit/2928588373c6f559cfc2cdc3f9d61150828f3455))
* **yazi:** enable without latest git use from module ([f5eb144](https://gitlab.com/simonoscr/nixfiles/commit/f5eb14498b231d2a2791818e5b8532ad6bd76863))
* **zed:** add config file ([abc4287](https://gitlab.com/simonoscr/nixfiles/commit/abc4287ed6ae3e734bfc4608654707501a65b2bb))
* **zed:** adjust config and make nix to json ([412c59b](https://gitlab.com/simonoscr/nixfiles/commit/412c59bb6a66a41e8eaf11f4323e990a9355f6df))

## [9.0.3](https://gitlab.com/simonoscr/nixfiles/compare/9.0.2...9.0.3) (2024-07-29)

### 📦 Other

* **flake:** update flake ([5932aae](https://gitlab.com/simonoscr/nixfiles/commit/5932aaecb35c33f6bd75567940f5e1f276a15dab))
* **flake:** update flock ([f6774db](https://gitlab.com/simonoscr/nixfiles/commit/f6774dbf195ef5bdea0c0b26bed4202c9a7f3434))
* **flake:** update flock ([ebb2da4](https://gitlab.com/simonoscr/nixfiles/commit/ebb2da4467155146d28635e387783e319b056f4f))
* **flake:** update flock ([26b7ff6](https://gitlab.com/simonoscr/nixfiles/commit/26b7ff69b5fd8f9119d8cbc6881158927c15023d))

### 🛠 Fixes

* **dpmsScript:** get Playing ([0c24d22](https://gitlab.com/simonoscr/nixfiles/commit/0c24d22916e7670cd2c8823ad2d91db9fa1cc8ba))
* **firefox:** add shyfox ([919d428](https://gitlab.com/simonoscr/nixfiles/commit/919d4286d225c61dece9cbc1c178d444260a2c7e))
* **hypridle:** remove suspend keep running ([b52ccc6](https://gitlab.com/simonoscr/nixfiles/commit/b52ccc6ca60f78226b82ab411034e15efc014e06))
* **hyprpanel:** wip still not used bcs hyprland dont start with it ([3c032c7](https://gitlab.com/simonoscr/nixfiles/commit/3c032c7dda3f843ae9824d2bc83bbc8392b38343))

## [9.0.2](https://gitlab.com/simonoscr/nixfiles/compare/9.0.1...9.0.2) (2024-07-28)

### 📦 Other

* **flake:** update flock ([f658b4f](https://gitlab.com/simonoscr/nixfiles/commit/f658b4f68a472ad2966eb83925b42694e88ca22c))

### 🛠 Fixes

* update custom kernel with security features ([4f5d629](https://gitlab.com/simonoscr/nixfiles/commit/4f5d62957dc9712c40c26307e4170d1e47d70422))
* **vscodium:** add wezterm as external terminal ([bcccf4c](https://gitlab.com/simonoscr/nixfiles/commit/bcccf4c3dc0f5198f56b82823e1ad063eb6592f1))
* **zsh:** add run function to run easiert nix-shell commands ([caf5235](https://gitlab.com/simonoscr/nixfiles/commit/caf5235f7d9b74893cb5a544d0c7b2f6ac13b9e3))

## [9.0.1](https://gitlab.com/simonoscr/nixfiles/compare/9.0.0...9.0.1) (2024-07-28)

### 📦 Other

* **flake:** update flock ([1cb1163](https://gitlab.com/simonoscr/nixfiles/commit/1cb1163d437d684ebd6d0773cf2a0599d0660c3e))

### 🛠 Fixes

* update xanmod custom kernel to 6.10.1 ([94a7b11](https://gitlab.com/simonoscr/nixfiles/commit/94a7b114620f75edb046f683e6ea386081028fb0))

## [9.0.0](https://gitlab.com/simonoscr/nixfiles/compare/8.9.0...9.0.0) (2024-07-28)

### 📔 Docs

* **firefox:** comment on two config params for hardware video acceleration with VA_API ([78144f3](https://gitlab.com/simonoscr/nixfiles/commit/78144f3c47ed31fea2fd94da53ddf73e48dbf318))

### 📦 Other

* **flake:** update flock ([d3e9130](https://gitlab.com/simonoscr/nixfiles/commit/d3e9130502d56cc1520f0ae62d89afbe27f70ba4))
* **flake:** update flock ([b3bad1a](https://gitlab.com/simonoscr/nixfiles/commit/b3bad1a7ec3b8a986382cf388265519003044035))

### 🧨 Breaking changes!

* added latest xanmod-git kernel with custom patches ([df313dd](https://gitlab.com/simonoscr/nixfiles/commit/df313ddac54dfe2037030886dcc7c36cad77efec))

### 🛠 Fixes

* **ags:** split up multiple configurations and use flake input as non-flake instead of fetchfromgithub ([ad1e130](https://gitlab.com/simonoscr/nixfiles/commit/ad1e130ba90a457e64f2a21ca7776fb1f8acc99e))
* **llm:** continoue config update for depp-code ([fc88a77](https://gitlab.com/simonoscr/nixfiles/commit/fc88a77ce967318dc2031378703cb72763959d02))
* **tmpf:** increase size to 75% of RAM to be able to build custom kernel ([94c902a](https://gitlab.com/simonoscr/nixfiles/commit/94c902a35fa1583f19c46666302e31e0b138a7bf))

## [8.9.0](https://gitlab.com/simonoscr/nixfiles/compare/8.8.3...8.9.0) (2024-07-27)

### 📦 Other

* **flake:** update flock ([eae9e27](https://gitlab.com/simonoscr/nixfiles/commit/eae9e275ad487fa3fce09b47b0679e26f76ebf59))

### 🚀 Features

* tailray a tailscale systray ([7d48ac1](https://gitlab.com/simonoscr/nixfiles/commit/7d48ac1872eff732d6703cb59a4ce20ad0019687))

## [8.8.3](https://gitlab.com/simonoscr/nixfiles/compare/8.8.2...8.8.3) (2024-07-27)

### 📦 Other

* **flake:** update flock ([4bcff81](https://gitlab.com/simonoscr/nixfiles/commit/4bcff814d6c22c4a425687ee9b055dd5172a7521))

### 🛠 Fixes

* **failscale:** update settings ([c11fbf8](https://gitlab.com/simonoscr/nixfiles/commit/c11fbf88f8d7e789b65ffc0bf5b755d49e0ebd28))

## [8.8.2](https://gitlab.com/simonoscr/nixfiles/compare/8.8.1...8.8.2) (2024-07-27)

### 📦 Other

* **flake:** update flock ([b1b617d](https://gitlab.com/simonoscr/nixfiles/commit/b1b617d0f4d577cf43b0085123a6f843038ff931))

### 🛠 Fixes

* **llm:** default config and location ([50d484a](https://gitlab.com/simonoscr/nixfiles/commit/50d484af198bfcd23e91c66fd580e3f75974c979))
* **wezterm:** make it uising the dgpu instead of the cpu and/or igpu ([788e195](https://gitlab.com/simonoscr/nixfiles/commit/788e195c8df95fdcf9a51a42ea205446f1f7f690))

## [8.8.1](https://gitlab.com/simonoscr/nixfiles/compare/8.8.0...8.8.1) (2024-07-27)

### 📔 Docs

* add desc to vscode settings ([6a1a246](https://gitlab.com/simonoscr/nixfiles/commit/6a1a246e4e623d14eb868000fe69cb4adc7b42c7))

### 📦 Other

* **flake:** update flock ([8f4f351](https://gitlab.com/simonoscr/nixfiles/commit/8f4f351d88e7148179c63608970ec875be080a10))

### 🛠 Fixes

* **llm:** create own folder and splitup backend and frontend ([4c640a1](https://gitlab.com/simonoscr/nixfiles/commit/4c640a154e16eae906ac563f094e996cd253cf3c))
* **tty:** disable ALL ttys ([029cdd8](https://gitlab.com/simonoscr/nixfiles/commit/029cdd8f7fa130fbbd631c2a1d2d1d276ccd8df1))

## [8.8.0](https://gitlab.com/simonoscr/nixfiles/compare/8.7.7...8.8.0) (2024-07-27)

### 📦 Other

* **flake:** update flock ([bc58970](https://gitlab.com/simonoscr/nixfiles/commit/bc5897013aa3df99efd2510ec947fa0e594d7be8))
* **flake:** update flock ([c21b77a](https://gitlab.com/simonoscr/nixfiles/commit/c21b77a3d3ca429b47482852ae8e4047e8f4563c))

### 🚀 Features

* **llm:** add ollama with open-webui for local llm ([22d3916](https://gitlab.com/simonoscr/nixfiles/commit/22d3916e883f6ad617eb8918414838c1fac20c44))
* **oom:** add earlyoom for better handling of oom scenarios ([314043b](https://gitlab.com/simonoscr/nixfiles/commit/314043b3ae11b0af032e0b6080b841b52a9ac81a))
* **systemd:** add systemd config file and disable ttys and set default timeouts ([e4799ea](https://gitlab.com/simonoscr/nixfiles/commit/e4799ea2917136383a13f8ec9943992d00d7b2bd))

### 🛠 Fixes

* **fonts:** update fonts and add some default ones for comp ([91a0f02](https://gitlab.com/simonoscr/nixfiles/commit/91a0f02da406cf849d95e8d1f3b59e7417b06877))
* **gpu:** enable amd rocm for local llm ([1fd6098](https://gitlab.com/simonoscr/nixfiles/commit/1fd6098728e9256faf191c5c98a2092431ae6808))
* **greetd:** adjust session paths ([a398903](https://gitlab.com/simonoscr/nixfiles/commit/a398903139aa9637b8e70667a7d7b66eb52120d1))
* **pam:** add pams ([ed4dfb0](https://gitlab.com/simonoscr/nixfiles/commit/ed4dfb0e44d18d721693462ffd0c134f886ec72c))
* **polit:** switch polkit from gnome to pantheon agent ([674b3ac](https://gitlab.com/simonoscr/nixfiles/commit/674b3acaf4088dcc76f3f0e177ccf1d13100463e))
* **polkit:** change windows rules for pantheon polkit agent instead of gnome ([3553400](https://gitlab.com/simonoscr/nixfiles/commit/3553400d2269c8a91f9b10b117a9282bffd1b1c9))
* **udiskie:** storage daemon for system ([c1dd726](https://gitlab.com/simonoscr/nixfiles/commit/c1dd72653c5875ee0ab7b54528bd3364292a1edc))
* **vscode:** remove invalid settings and add continue extension to use local llm in vscode ([7751653](https://gitlab.com/simonoscr/nixfiles/commit/77516533f62b704f0513d6b5a963d41a0639aa96))

## [8.7.7](https://gitlab.com/simonoscr/nixfiles/compare/8.7.6...8.7.7) (2024-07-26)

### :scissors: Refactor

* server configuration ([7cd3d90](https://gitlab.com/simonoscr/nixfiles/commit/7cd3d90f0fe06e3b5bab0a3cc93d25629a65344b))

### 📔 Docs

* update README ([058afd5](https://gitlab.com/simonoscr/nixfiles/commit/058afd57ec8b5eb35423b12d594a4a7352867132))
* update readme capital letters not needed ([10f0fdb](https://gitlab.com/simonoscr/nixfiles/commit/10f0fdba7d9d25d50c07888043821c30e26058b8))

### 📦 Other

* **flake:** update flock ([3185a20](https://gitlab.com/simonoscr/nixfiles/commit/3185a20e46e686fddc6ac8297b849cedb3921afb))

## [8.7.6](https://gitlab.com/simonoscr/nixfiles/compare/8.7.5...8.7.6) (2024-07-25)

### 📔 Docs

* update readme with checklist ([8d491f0](https://gitlab.com/simonoscr/nixfiles/commit/8d491f00646ba8aad83538a9afaf87ac8a2e05cc))

### 📦 Other

* **flake:** update flock ([5c8ac7b](https://gitlab.com/simonoscr/nixfiles/commit/5c8ac7b92d0162d08a1b240f93bb2520747d881b))

### 🛠 Fixes

* add agstheme as non flake input ([3ca0c70](https://gitlab.com/simonoscr/nixfiles/commit/3ca0c7089d402e1167d88ca4c987143d16dfdc97))
* initial setup for using own ags WIP ([0405e3e](https://gitlab.com/simonoscr/nixfiles/commit/0405e3ed8bf7053a5fe2e29c70b9813117b5d151))

## [8.7.5](https://gitlab.com/simonoscr/nixfiles/compare/8.7.4...8.7.5) (2024-07-23)

### 👀 Reverts

* drop substis from nixConfig ([b0066ab](https://gitlab.com/simonoscr/nixfiles/commit/b0066abefde110bf11c6733f079fe2c553b8c86b))

## [8.7.4](https://gitlab.com/simonoscr/nixfiles/compare/8.7.3...8.7.4) (2024-07-23)

### 📦 Other

* **flake:** update flock ([d201b54](https://gitlab.com/simonoscr/nixfiles/commit/d201b54427553cb3fe2b389603a8801fa042bc9a))

### 🛠 Fixes

* hardware and boot fixes ([5e62f51](https://gitlab.com/simonoscr/nixfiles/commit/5e62f511523a49fd6d9be7b5682d115265aed8dd))
* hyprgamemode ([22e9c26](https://gitlab.com/simonoscr/nixfiles/commit/22e9c26e408e85234f771036a91c83f2ae690e56))
* move substis to nixConfig ([19423ad](https://gitlab.com/simonoscr/nixfiles/commit/19423adb9139f897b8bfb4d969273579cad17d45))
* update secrets management ([9fc91f9](https://gitlab.com/simonoscr/nixfiles/commit/9fc91f96447c0d194f7439969ec6c014fbf25095))
* xdg dirs ([092da72](https://gitlab.com/simonoscr/nixfiles/commit/092da729733100649185e9760a54b31c03e34a14))
* **zsh:** move to .zsh dir ([5bf31a1](https://gitlab.com/simonoscr/nixfiles/commit/5bf31a102d5dad23581cc433926190bd75b8d509))

## [8.7.3](https://gitlab.com/simonoscr/nixfiles/compare/8.7.2...8.7.3) (2024-07-23)

### :scissors: Refactor

* cleanup double entries and move some keys ([ea1f6ff](https://gitlab.com/simonoscr/nixfiles/commit/ea1f6ff0556fc5d56ad4bd92aa09b199940b615e))

### 📦 Other

* **flake:** update flake ([52d699c](https://gitlab.com/simonoscr/nixfiles/commit/52d699cda640365584301700505206b938d8c797))

### 🛠 Fixes

* drop matugen ([3265823](https://gitlab.com/simonoscr/nixfiles/commit/32658232f20a83ce13b06a656304712f5d05fe0f))
* ja ([91dea02](https://gitlab.com/simonoscr/nixfiles/commit/91dea0219c24b062d0fc9d990782bf81eeb92074))
* **hyprland:** update config ([1e214af](https://gitlab.com/simonoscr/nixfiles/commit/1e214af45257626eee95d998fd9b1110af836959))
* **i3:** updae bind ([2c3656a](https://gitlab.com/simonoscr/nixfiles/commit/2c3656aa1e0b8d33852dc2df336b58f2acf320ef))

## [8.7.2](https://gitlab.com/simonoscr/nixfiles/compare/8.7.1...8.7.2) (2024-07-22)

### 🛠 Fixes

* **gtk:** store gtk themes in a more prominent place ([91c976b](https://gitlab.com/simonoscr/nixfiles/commit/91c976b3d1ab1ee36d90af0db9f62d1232fc0e6c))

## [8.7.1](https://gitlab.com/simonoscr/nixfiles/compare/8.7.0...8.7.1) (2024-07-21)

### 🛠 Fixes

* **hyprmode:** hz to allowed ([e961d34](https://gitlab.com/simonoscr/nixfiles/commit/e961d34ee41718e014bf1cfa26439a2a52dfe75e))

## [8.7.0](https://gitlab.com/simonoscr/nixfiles/compare/8.6.0...8.7.0) (2024-07-21)

### 📦 Other

* **flake:** update flake ([6547ef4](https://gitlab.com/simonoscr/nixfiles/commit/6547ef4cdbf4b92baee372efa606c802b0d2b06a))
* **flake:** update flake ([46f8637](https://gitlab.com/simonoscr/nixfiles/commit/46f86379eeacbe125b45592f339ce625b51cbea6))

### 🚀 Features

* **all:** refactor and utilize default.nix files ([808f3e6](https://gitlab.com/simonoscr/nixfiles/commit/808f3e6796b59357b1fa3916ce58d0a2a1bdde00))
* **hm:** move and refactor home-manager ([c49779f](https://gitlab.com/simonoscr/nixfiles/commit/c49779f39a6ab5f8b0d48f563d597b2a6e7a29b9))

### 🛠 Fixes

* add power-profiles-daemon ([8a47463](https://gitlab.com/simonoscr/nixfiles/commit/8a47463b198c1c11a39ee62baaf749944b9e9ee7))
* **firefox:** use tmpfs ([10bdeeb](https://gitlab.com/simonoscr/nixfiles/commit/10bdeeb8e114a599eeea041b00366820676d77ea))
* **flake:** drop tailray input ([4a4853e](https://gitlab.com/simonoscr/nixfiles/commit/4a4853e0b5f2a24afd45663269891d97189da294))
* **hyprland:** move hyprland universum files closer together ([fcb6898](https://gitlab.com/simonoscr/nixfiles/commit/fcb689855448e8f930e364f84aacf3b4f2a319e3))

## [8.6.0](https://gitlab.com/simonoscr/nixfiles/compare/8.5.1...8.6.0) (2024-07-21)

### 📦 Other

* **flake:** update flake ([8fdc9f8](https://gitlab.com/simonoscr/nixfiles/commit/8fdc9f8f3c6e4eb9d94feb46d804e3aa57ec0a5e))

### 🚀 Features

* **refactor:** add bunch of extra config files and modules ([813a8c5](https://gitlab.com/simonoscr/nixfiles/commit/813a8c5614d6ed31f009fee2301e2a2703308210))

### 🛠 Fixes

* **iso:** in flake.nix file ([7bc6ca5](https://gitlab.com/simonoscr/nixfiles/commit/7bc6ca5aaf8481dc34ba748984003c938333c683))

## [8.5.1](https://gitlab.com/simonoscr/nixfiles/compare/8.5.0...8.5.1) (2024-07-20)

### 📦 Other

* **flake:** update flake ([3412065](https://gitlab.com/simonoscr/nixfiles/commit/34120650c62469effdfd8cf8df1948e42b12ce91))
* **flake:** update flake ([8dc66d9](https://gitlab.com/simonoscr/nixfiles/commit/8dc66d9b823b92cfc46915a77720f1cd112a5754))

### 🛠 Fixes

* **firefox:** importing bookmarks ([360d062](https://gitlab.com/simonoscr/nixfiles/commit/360d0626b4f1a99b27b21ae5b8601a7a2bc660dc))
* **yubikey:** drop unused packages and enable u2f ([3e67959](https://gitlab.com/simonoscr/nixfiles/commit/3e67959027bf0fa57b76859831f7864ba2bbb806))

## [8.5.0](https://gitlab.com/simonoscr/nixfiles/compare/8.4.3...8.5.0) (2024-07-19)

### 📦 Other

* **flake:** update flake ([82d3907](https://gitlab.com/simonoscr/nixfiles/commit/82d3907ccadbb0c26187a785cccbe014d4eeb960))
* **flake:** update flake ([0ee3858](https://gitlab.com/simonoscr/nixfiles/commit/0ee38584e5a78a6514baa7f27035f1a859410f53))

### 🚀 Features

* nix-index ([7941299](https://gitlab.com/simonoscr/nixfiles/commit/7941299181c57e5a5e70cbd5776cb75772d8c00b))

### 🛠 Fixes

* make nixpkgs available in the nixpath ([c19f3b0](https://gitlab.com/simonoscr/nixfiles/commit/c19f3b08925f03d5a85209d896193342615ce8e5))
* move pre-commit-hooks into flake.nix instead of own file! ([47fb966](https://gitlab.com/simonoscr/nixfiles/commit/47fb966f15f6e1182a1a8ad220a0224e9f862c83))
* **firefox:** update bookmarks ([f78f9ef](https://gitlab.com/simonoscr/nixfiles/commit/f78f9efbdb4ae861d03cb699c82e2f851ddc3bb6))

## [8.4.3](https://gitlab.com/simonoscr/nixfiles/compare/8.4.2...8.4.3) (2024-07-19)

### 📦 Other

* **flake:** update flake ([a70b0da](https://gitlab.com/simonoscr/nixfiles/commit/a70b0da404586b41139a7c07dcd49b3b2ee1c099))
* **flake:** update flake ([75527f6](https://gitlab.com/simonoscr/nixfiles/commit/75527f65d8dd7c6618d9b02c7d9e1a4ae7d48f3f))
* **flake:** update flake ([98571c8](https://gitlab.com/simonoscr/nixfiles/commit/98571c882be5a6d3e28b73e566db85700696cbde))

### 🦊 CI/CD

* fixed tag 22.4.1 for node image because of bug ([c5b7561](https://gitlab.com/simonoscr/nixfiles/commit/c5b756161ac9b28f542a0916cc1b5bdc91bd813c))
* fixed tag for node image because of bug ([62c44f8](https://gitlab.com/simonoscr/nixfiles/commit/62c44f8eeac5c18cc10635c9c7ee67244e79e2d4))

### 🛠 Fixes

* remove i3 ([f314b7d](https://gitlab.com/simonoscr/nixfiles/commit/f314b7d409e1ae7816b5cd7ff77939ab4ec2e6c3))
* substituters ([1225b00](https://gitlab.com/simonoscr/nixfiles/commit/1225b00a94fb4981ead5e4edc4c6ca450bb6ce99))
* update different small services ([67a3ef7](https://gitlab.com/simonoscr/nixfiles/commit/67a3ef78905c11c776707287a1a709848051829d))
* WIP add iso ([4bdd647](https://gitlab.com/simonoscr/nixfiles/commit/4bdd647632f5c29a9b05b2f441721c1cbcc66554))
* **ags:** update new hm module ([e96d9ac](https://gitlab.com/simonoscr/nixfiles/commit/e96d9ac6be7063738c819509b50bca4fcf06df8c))
* **direnv:** suppress logging ([9944966](https://gitlab.com/simonoscr/nixfiles/commit/994496614bd99f90d7923daf834b8c2b239bc318))
* **zsh:** update config ([98e421f](https://gitlab.com/simonoscr/nixfiles/commit/98e421f298110a9cfe10f5635b029306f4cb93d2))

## [8.4.2](https://gitlab.com/simonoscr/nixfiles/compare/8.4.1...8.4.2) (2024-07-16)

### 📦 Other

* **flake:** update flake ([dddc240](https://gitlab.com/simonoscr/nixfiles/commit/dddc240b9468ea54b0f4cc54066af914b1258f95))

### 🛠 Fixes

* fix typo tailray ([a6c9869](https://gitlab.com/simonoscr/nixfiles/commit/a6c98697722533fbd3f08561f2a4224c7e79893f))

## [8.4.1](https://gitlab.com/simonoscr/nixfiles/compare/8.4.0...8.4.1) (2024-07-16)

### 🛠 Fixes

* gpu and performance ([2797815](https://gitlab.com/simonoscr/nixfiles/commit/279781555fcef4359aa2b0e78bee795ad83eb208))
* udev rules for wooting ([4bd6ee8](https://gitlab.com/simonoscr/nixfiles/commit/4bd6ee83eecc5380cc6b07b02fa593d4a859aea7))

## [8.4.0](https://gitlab.com/simonoscr/nixfiles/compare/8.3.1...8.4.0) (2024-07-16)

### 📦 Other

* WIP prepare impermanence ([cf2d586](https://gitlab.com/simonoscr/nixfiles/commit/cf2d5863fb5857ac7d64175666ece5acf7156837))
* **flake:** update flake ([7a328c6](https://gitlab.com/simonoscr/nixfiles/commit/7a328c6cb03576f8e9aaccb25e30fe0381b4a382))
* **flake:** update flake ([7a8e47c](https://gitlab.com/simonoscr/nixfiles/commit/7a8e47ca7cc8342bf0067caa754407b36e3e780c))
* **flake:** update flake ([be8a53a](https://gitlab.com/simonoscr/nixfiles/commit/be8a53a0937e9a40bdda895f4e6bf81ed26e09e8))
* **flake:** update flake ([89e07f1](https://gitlab.com/simonoscr/nixfiles/commit/89e07f178bee5897206160c3f7d8616013508370))

### 🚀 Features

* init tailtray ([46c269b](https://gitlab.com/simonoscr/nixfiles/commit/46c269b113db435f32460298996f3b50affff5a4))

### 🛠 Fixes

* dont suspend when audio ([2fdb5af](https://gitlab.com/simonoscr/nixfiles/commit/2fdb5af8980415dd6534d5bdfcb7da91191e3046))

## [8.3.1](https://gitlab.com/simonoscr/nixfiles/compare/8.3.0...8.3.1) (2024-07-13)

### 📦 Other

* **flake:** update flake ([d584c01](https://gitlab.com/simonoscr/nixfiles/commit/d584c01cf7a020d6916a08b8ef37d92fb773772e))
* **flake:** update flake ([6541b57](https://gitlab.com/simonoscr/nixfiles/commit/6541b579b7d2214e9304e6ae6ff1094a6a6f8605))

### 🛠 Fixes

* update security ([0ac63d2](https://gitlab.com/simonoscr/nixfiles/commit/0ac63d2503af1b330b1ec1dd508211835d620923))

## [8.3.0](https://gitlab.com/simonoscr/nixfiles/compare/8.2.6...8.3.0) (2024-07-13)

### 🚀 Features

* move work to own repository ([6f6b745](https://gitlab.com/simonoscr/nixfiles/commit/6f6b745dd3d7d6a1418ff1ffdb4f49795e4d5856))

## [8.2.6](https://gitlab.com/simonoscr/nixfiles/compare/8.2.5...8.2.6) (2024-07-13)

### 📦 Other

* **flake:** update flake.lock ([54aa3af](https://gitlab.com/simonoscr/nixfiles/commit/54aa3af202c9b3f288a4e3fa8dc2cc26d2b363dc))

### 🛠 Fixes

* pkg and secret ([b37a427](https://gitlab.com/simonoscr/nixfiles/commit/b37a4279e6d95cd252786313024443dc6d8998bd))
* remove docker and use podman instead ([7e911fa](https://gitlab.com/simonoscr/nixfiles/commit/7e911fad7288061d041156d064646b81d6818d88))
* remove vpn from i3status ([704498f](https://gitlab.com/simonoscr/nixfiles/commit/704498fc9ca16692dfb6deecf32d415be9b5a5c2))

## [8.2.5](https://gitlab.com/simonoscr/nixfiles/compare/8.2.4...8.2.5) (2024-07-13)

### 📦 Other

* **flake:** update flake.lock ([8331a72](https://gitlab.com/simonoscr/nixfiles/commit/8331a729c416553d71132fc64a0df0638ad447f3))

### 🛠 Fixes

* enable fingerprint and secureboot ([55571da](https://gitlab.com/simonoscr/nixfiles/commit/55571da417726202cbe84cce52055c02ea5ccb8d))
* security opti ([f1178bd](https://gitlab.com/simonoscr/nixfiles/commit/f1178bda90f9275668ee9059d10f89f043f71ab5))

## [8.2.4](https://gitlab.com/simonoscr/nixfiles/compare/8.2.3...8.2.4) (2024-07-13)

### 🛠 Fixes

* disable fingerprint ([ab8cecd](https://gitlab.com/simonoscr/nixfiles/commit/ab8cecdeb78e8a06ea67683ec9d6a69025c18626))

## [8.2.3](https://gitlab.com/simonoscr/nixfiles/compare/8.2.2...8.2.3) (2024-07-13)

### 🛠 Fixes

* add luks boot ([60f7d26](https://gitlab.com/simonoscr/nixfiles/commit/60f7d262de5f1763e75bc73bee02cc4dcf94faf8))

## [8.2.2](https://gitlab.com/simonoscr/nixfiles/compare/8.2.1...8.2.2) (2024-07-13)

### :scissors: Refactor

* **work:** cleanup and sort alpha ([48367d2](https://gitlab.com/simonoscr/nixfiles/commit/48367d2e64b80cd28ccd4639290c33009e32f102))

## [8.2.1](https://gitlab.com/simonoscr/nixfiles/compare/8.2.0...8.2.1) (2024-07-13)

### 🛠 Fixes

* **work:** openssh as own file ([33ff56f](https://gitlab.com/simonoscr/nixfiles/commit/33ff56fe03bbe8dd2d87dadf8822100c39aca921))

## [8.2.0](https://gitlab.com/simonoscr/nixfiles/compare/8.1.2...8.2.0) (2024-07-13)

### 🚀 Features

* **work:** add fingerprint for laptop ([49fd696](https://gitlab.com/simonoscr/nixfiles/commit/49fd69639e3db66494c973cb3c5388babd4a275c))

## [8.1.2](https://gitlab.com/simonoscr/nixfiles/compare/8.1.1...8.1.2) (2024-07-13)

### 🛠 Fixes

* **work:** update hardware-configuration ([ab7c5a8](https://gitlab.com/simonoscr/nixfiles/commit/ab7c5a87a43edc0cff922c2fe79b8dae88592151))

## [8.1.1](https://gitlab.com/simonoscr/nixfiles/compare/8.1.0...8.1.1) (2024-07-13)

### 📦 Other

* **flake:** update flake ([5d5f988](https://gitlab.com/simonoscr/nixfiles/commit/5d5f988d2499db634ceb04ee4ab2b8048aebfa84))

### 🛠 Fixes

* **work:** disable secureboot for initial setup ([fb76ff8](https://gitlab.com/simonoscr/nixfiles/commit/fb76ff8a02d02fd03f38a63a7adba83183a9dcfc))

## [8.1.0](https://gitlab.com/simonoscr/nixfiles/compare/8.0.3...8.1.0) (2024-07-13)

### 📦 Other

* **flake:** update flake.lock ([f85e47f](https://gitlab.com/simonoscr/nixfiles/commit/f85e47f6fc6b7a712a649a8ff7273dde99d4df80))

### 🚀 Features

* add lanzaboote ([c8ea393](https://gitlab.com/simonoscr/nixfiles/commit/c8ea3932b7d2137b474f2de7152b36d222c36893))
* update hardware and add software ([0d5367a](https://gitlab.com/simonoscr/nixfiles/commit/0d5367aaa2bf14a9d96e67b916f56330f6a60bfc))
* update hardware like gpu and bluetooth ([9977f02](https://gitlab.com/simonoscr/nixfiles/commit/9977f02a14ea1701a4dab74cea590eab9ff0a8c6))
* update i3 ([c7f9138](https://gitlab.com/simonoscr/nixfiles/commit/c7f91387710eca03f895ac14c9c94dc30ee3704c))

### 🛠 Fixes

* cleanup unsused files ([c86b03b](https://gitlab.com/simonoscr/nixfiles/commit/c86b03b00416a1cfc92702c1a5b82ff50a174983))

## [8.0.3](https://gitlab.com/simonoscr/nixfiles/compare/8.0.2...8.0.3) (2024-07-12)

### 🛠 Fixes

* **work:** add hyprland to work profile ([e7e113c](https://gitlab.com/simonoscr/nixfiles/commit/e7e113cb86ac363b9daadc3754e4bc0d63be2e3e))

## [8.0.2](https://gitlab.com/simonoscr/nixfiles/compare/8.0.1...8.0.2) (2024-07-12)

### 🛠 Fixes

* **work:** increase stateVersion to 24.05 for work nixos ([fa50af8](https://gitlab.com/simonoscr/nixfiles/commit/fa50af83b5169a5694d5f8b72ffe0c953ea5823b))

## [8.0.1](https://gitlab.com/simonoscr/nixfiles/compare/8.0.0...8.0.1) (2024-07-12)

### 📦 Other

* **flake:** update flake ([5b36b30](https://gitlab.com/simonoscr/nixfiles/commit/5b36b30949f4e813f9ef1d78f2ac7e31582d9b29))

### 🛠 Fixes

* **work:** hardware-configuration.nix ([84b3369](https://gitlab.com/simonoscr/nixfiles/commit/84b3369753a66855012117dd3ec9e3dbd33ed110))

## [8.0.0](https://gitlab.com/simonoscr/nixfiles/compare/7.12.1...8.0.0) (2024-07-12)

### 🧨 Breaking changes!

* switch to nixos for work laptop ([736c1b5](https://gitlab.com/simonoscr/nixfiles/commit/736c1b58ed85b676ffb3f677cab5fc6cffd514f2))

## [7.12.1](https://gitlab.com/simonoscr/nixfiles/compare/7.12.0...7.12.1) (2024-07-11)

### 🛠 Fixes

* **yazi:** enable image preview again with fixed nose for python ([a8c0cc9](https://gitlab.com/simonoscr/nixfiles/commit/a8c0cc97a196c60f9661bca85fbf4e47a294fef3))

## [7.12.0](https://gitlab.com/simonoscr/nixfiles/compare/7.11.3...7.12.0) (2024-07-11)

### 🚀 Features

* yubikey login,sudo,hyprlock WIP ([80e7e08](https://gitlab.com/simonoscr/nixfiles/commit/80e7e087348b5fd3075ff717d586ceef4af16538))

## [7.11.3](https://gitlab.com/simonoscr/nixfiles/compare/7.11.2...7.11.3) (2024-07-10)

### 📦 Other

* **flake:** update flake ([840699b](https://gitlab.com/simonoscr/nixfiles/commit/840699b630648dac4360b6fd24f1861e0729a65b))

### 🛠 Fixes

* allow OCSP in work profile ([e81c48e](https://gitlab.com/simonoscr/nixfiles/commit/e81c48e0636736d676b467c6836bde8020541025))

## [7.11.2](https://gitlab.com/simonoscr/nixfiles/compare/7.11.1...7.11.2) (2024-07-10)

### 📦 Other

* **flake:** ommit extra- prefix for nixConfig substituters and public key ([7d4cc0b](https://gitlab.com/simonoscr/nixfiles/commit/7d4cc0bd2303f46f7d1c241ac0cc69b160ec0fd1))

### 🛠 Fixes

* **pkgs:** disable bitwarden-desktop ([d8ff823](https://gitlab.com/simonoscr/nixfiles/commit/d8ff8236c2da5a82cc69de2736f67efef2f4f7c6))

## [7.11.1](https://gitlab.com/simonoscr/nixfiles/compare/7.11.0...7.11.1) (2024-07-09)

### 🛠 Fixes

* **wezterm:** fix padding typo and add wezterm to hyprland shortcuts ([b19ca71](https://gitlab.com/simonoscr/nixfiles/commit/b19ca715dc537f6eef6b1d402aee59f84e079107))

## [7.11.0](https://gitlab.com/simonoscr/nixfiles/compare/7.10.1...7.11.0) (2024-07-09)

### 📦 Other

* **flake:** update flake ([233713f](https://gitlab.com/simonoscr/nixfiles/commit/233713f819f2a7610da05f9ba7a55c037844f185))

### 🚀 Features

* **firefox:** update config for up-to-date user.js and settings ([cd73569](https://gitlab.com/simonoscr/nixfiles/commit/cd73569c9e8d9bdcadd9c70f97d5647c3f6ee2e8))

### 🛠 Fixes

* **boot:** enable systemd-boot initrd ([dfaaf3c](https://gitlab.com/simonoscr/nixfiles/commit/dfaaf3c71bb3dda23fd3130a7247fbf8c3264f99))
* **firefox-work:** update also firefox config and settings for work profile ([6b76eb2](https://gitlab.com/simonoscr/nixfiles/commit/6b76eb20e75d0714da630670a5aadad4bd44de05))
* **hyprland:** add hyprcursor env ([2b7c84f](https://gitlab.com/simonoscr/nixfiles/commit/2b7c84fbe3244600a94d7baa1713bba1a815e115))
* **nixpkgs:** remove programs that cant be build because of nose python error ([3a127fc](https://gitlab.com/simonoscr/nixfiles/commit/3a127fc367fd3043c8dd3da78de81209fa641633))
* **podman:** update podman home-manager module WIP ([2603e76](https://gitlab.com/simonoscr/nixfiles/commit/2603e7618a1b7255cb4ae56ded1d41688bff1537))

## [7.10.1](https://gitlab.com/simonoscr/nixfiles/compare/7.10.0...7.10.1) (2024-07-08)

### 📦 Other

* **flake:** update flake ([afcdbe0](https://gitlab.com/simonoscr/nixfiles/commit/afcdbe0b8df3ec875e56c82de3ed40b979a6822b))

### 🛠 Fixes

* update wezterm config ([17083e7](https://gitlab.com/simonoscr/nixfiles/commit/17083e7680ee9e322ceee11961340558c192ed7f))

## [7.10.0](https://gitlab.com/simonoscr/nixfiles/compare/7.9.2...7.10.0) (2024-07-08)

### 🚀 Features

* **podman:** WIP add podman module and config ([2c80209](https://gitlab.com/simonoscr/nixfiles/commit/2c802092c0846fe70da284aec3a9125721065e83))

## [7.9.2](https://gitlab.com/simonoscr/nixfiles/compare/7.9.1...7.9.2) (2024-07-08)

### 🛠 Fixes

* **wezterm:** adjust config ([ff1fa2e](https://gitlab.com/simonoscr/nixfiles/commit/ff1fa2e9b8dfaa2fcd17149949bc1e281a178f6c))

## [7.9.1](https://gitlab.com/simonoscr/nixfiles/compare/7.9.0...7.9.1) (2024-07-08)

### 🛠 Fixes

* add pkgs ([38ed834](https://gitlab.com/simonoscr/nixfiles/commit/38ed834b8b65a6d224c3ddda3a3409079a89d83e))
* **terminal:** add wezterm, adjust config for wezterm starship and i3 ([ae02955](https://gitlab.com/simonoscr/nixfiles/commit/ae0295502679da8ea06bf9174cad0af2ab280cad))

## [7.9.0](https://gitlab.com/simonoscr/nixfiles/compare/7.8.3...7.9.0) (2024-07-07)

### 🚀 Features

* add wezterm ([fa509b7](https://gitlab.com/simonoscr/nixfiles/commit/fa509b72ad627609bac296edc62e1be26c3e4f0b))

## [7.8.3](https://gitlab.com/simonoscr/nixfiles/compare/7.8.2...7.8.3) (2024-07-07)

### 🛠 Fixes

* **gaming:** tweaks and remove corectl ([7c48b9a](https://gitlab.com/simonoscr/nixfiles/commit/7c48b9ad72fd0aee08a1be7cfdbd92face60e0ea))

## [7.8.2](https://gitlab.com/simonoscr/nixfiles/compare/7.8.1...7.8.2) (2024-07-07)

### 🛠 Fixes

* **firefox:** WIP add default arkenfox user.js ([8c8636c](https://gitlab.com/simonoscr/nixfiles/commit/8c8636cecb08aefc5b9df47d8f2649cf6f83d52d))

## [7.8.1](https://gitlab.com/simonoscr/nixfiles/compare/7.8.0...7.8.1) (2024-07-07)

### 🛠 Fixes

* **starship:** update style and better reflect different prompt states ([840a6d5](https://gitlab.com/simonoscr/nixfiles/commit/840a6d577e09b247501e913cbee2991b7365df26))

## [7.8.0](https://gitlab.com/simonoscr/nixfiles/compare/7.7.4...7.8.0) (2024-07-07)

### 📦 Other

* **flake:** update flake ([4896a04](https://gitlab.com/simonoscr/nixfiles/commit/4896a04c2add02c211db9eec4d6dea5e6636c743))
* **flake:** update flake ([2d0a080](https://gitlab.com/simonoscr/nixfiles/commit/2d0a080bc3e8d5f9e1a29ad53931c91f7b1ca83b))

### 🚀 Features

* add shortcut script with yad for shortcut overview ([978134c](https://gitlab.com/simonoscr/nixfiles/commit/978134c3eb03c61315c0867cd6da69e73ba21c20))

## [7.7.4](https://gitlab.com/simonoscr/nixfiles/compare/7.7.3...7.7.4) (2024-07-06)

### 📦 Other

* **flake:** update flake ([9c778ba](https://gitlab.com/simonoscr/nixfiles/commit/9c778ba4049c90625bf673d181a5f03a192aec61))

### 🛠 Fixes

* **cleanup:** remove unused imports ([033fd67](https://gitlab.com/simonoscr/nixfiles/commit/033fd67ec37a7f4210ab6370e514b63b10c42998))
* **terminal:** adjust starship and kitty settings etc ([b6b1e23](https://gitlab.com/simonoscr/nixfiles/commit/b6b1e237180ca4acbc30d98f0d5dbb33d5f5f3af))

## [7.7.3](https://gitlab.com/simonoscr/nixfiles/compare/7.7.2...7.7.3) (2024-07-06)

### 📦 Other

* **flake:** update flake ([1f34c0b](https://gitlab.com/simonoscr/nixfiles/commit/1f34c0b18202903ead48ba3795068b2d36aa9046))

### 🛠 Fixes

* **git:** set pull.rebase to true ([4a6cb4b](https://gitlab.com/simonoscr/nixfiles/commit/4a6cb4bee4f3e30dd443aaa7bf1108d6ff9d8e09))

## [7.7.2](https://gitlab.com/simonoscr/nixfiles/compare/7.7.1...7.7.2) (2024-07-06)

### 🛠 Fixes

* **gitlab-runner:** update from registrationToken to authenticationToken ([5b3a1e7](https://gitlab.com/simonoscr/nixfiles/commit/5b3a1e7e918ab65c2e10b6d7dabae358a16a2966))

## [7.7.1](https://gitlab.com/simonoscr/nixfiles/compare/7.7.0...7.7.1) (2024-07-06)

### 📦 Other

* **flake:** update flake ([267a6d3](https://gitlab.com/simonoscr/nixfiles/commit/267a6d382ce2bdc0d1f73ba7ce08ce020d1bf694))

### 🛠 Fixes

* **flake:** update inputs ([d7f699c](https://gitlab.com/simonoscr/nixfiles/commit/d7f699cd0dfdc77f1311e2b97caf0cab0fcb4922))
* **gaming:** steam without extrapkgs and gamescope with capsysnice enabled ([03c7d1a](https://gitlab.com/simonoscr/nixfiles/commit/03c7d1af4755be416449db2b43c3255d7c14d6aa))

## [7.7.0](https://gitlab.com/simonoscr/nixfiles/compare/7.6.3...7.7.0) (2024-07-05)

### 🚀 Features

* add packages.nix ([2495cc0](https://gitlab.com/simonoscr/nixfiles/commit/2495cc028111d3d64f7cfe17cf5f3a0ad3cbc68f))

## [7.6.3](https://gitlab.com/simonoscr/nixfiles/compare/7.6.2...7.6.3) (2024-07-05)

### 📦 Other

* **flake:** update flake ([5e19225](https://gitlab.com/simonoscr/nixfiles/commit/5e192259de8e3f831017ec54cecbef52252c9e3d))
* **flake:** update flake ([bd2169b](https://gitlab.com/simonoscr/nixfiles/commit/bd2169b5a728e0aab71b30624ec813a707053ebd))
* **flake:** update flake ([bb04451](https://gitlab.com/simonoscr/nixfiles/commit/bb04451418fb165f19de7f3293307b421d65aacd))
* **flake:** update flake ([b42cbab](https://gitlab.com/simonoscr/nixfiles/commit/b42cbab2399403c11d6149580c93d5358dc2b479))
* **flake:** update nixConfig ([c098f97](https://gitlab.com/simonoscr/nixfiles/commit/c098f9744260cd30c3221974de5480bfb3d2bb08))

### 🛠 Fixes

* add helm shell alias ([1e5224c](https://gitlab.com/simonoscr/nixfiles/commit/1e5224c45c5ff9b37678c9b1acdf17dfa0e2dff3))

## [7.6.2](https://gitlab.com/simonoscr/nixfiles/compare/7.6.1...7.6.2) (2024-07-02)

### 🛠 Fixes

* move substituters config to flakes nixConfig ([95a0d5d](https://gitlab.com/simonoscr/nixfiles/commit/95a0d5d09a8985fd2af63cfdafaaf204ddd44289))

## [7.6.1](https://gitlab.com/simonoscr/nixfiles/compare/7.6.0...7.6.1) (2024-07-02)

### 🛠 Fixes

* **work:** move to nixConfig flake for substituters and disable old substituters with nix.settings ([54bf477](https://gitlab.com/simonoscr/nixfiles/commit/54bf47741326488bbd2f3f384fbe3ce1e73674de))

## [7.6.0](https://gitlab.com/simonoscr/nixfiles/compare/7.5.4...7.6.0) (2024-07-02)

### 📦 Other

* **flake:** update flake ([5a6ca3d](https://gitlab.com/simonoscr/nixfiles/commit/5a6ca3d0559549c71398b84aad5a059900811427))
* **flake:** update flake ([7914356](https://gitlab.com/simonoscr/nixfiles/commit/7914356fc898aed963486cb71c42fe9c1d5ff35e))
* **flake:** update flake ([0e7d7a4](https://gitlab.com/simonoscr/nixfiles/commit/0e7d7a4b87f5d3d2393de4cb994093d794546daf))

### 🚀 Features

* **i3 work:** add dunst and i3status-rust for i3status; update i3lock and packages with utils ([06f641f](https://gitlab.com/simonoscr/nixfiles/commit/06f641fbcdc03cddd07dad0c7efd1cf2a8434f48))

## [7.5.4](https://gitlab.com/simonoscr/nixfiles/compare/7.5.3...7.5.4) (2024-07-01)

### 🛠 Fixes

* default EDITOR to nvim and add evolution with all plugins to pkgs ([463f25a](https://gitlab.com/simonoscr/nixfiles/commit/463f25a76970739cb363a2760e2e9f3a593e9faf))
* Inter font to Inter Variable font ([f5eccbd](https://gitlab.com/simonoscr/nixfiles/commit/f5eccbd3005a67662a1847cce2021dd973a2e374))
* sops hm module ([07cb1a0](https://gitlab.com/simonoscr/nixfiles/commit/07cb1a0fbb627f5679d9af3789891e13e690e134))

## [7.5.3](https://gitlab.com/simonoscr/nixfiles/compare/7.5.2...7.5.3) (2024-07-01)

### 🛠 Fixes

* **work:** zsh as default shell with kitty and kitty as default terminal ([b741ac0](https://gitlab.com/simonoscr/nixfiles/commit/b741ac080aae9d3aa52dd630bfbb2527230b4c04))

## [7.5.2](https://gitlab.com/simonoscr/nixfiles/compare/7.5.1...7.5.2) (2024-07-01)

### 📦 Other

* **flake:** update flake ([4dbb9dc](https://gitlab.com/simonoscr/nixfiles/commit/4dbb9dcd32a9c3da53ef567ce49ea65eebefbf73))

### 🛠 Fixes

* add trusted subtituters and update ([37da3ab](https://gitlab.com/simonoscr/nixfiles/commit/37da3ab14e674c7b66bdf6977a3261a4364c3f1b))

## [7.5.1](https://gitlab.com/simonoscr/nixfiles/compare/7.5.0...7.5.1) (2024-06-28)

### 🛠 Fixes

* terminal and PATH issue with sudo ([e0f0e97](https://gitlab.com/simonoscr/nixfiles/commit/e0f0e9712d04e4ba5a015de410f710159c78e586))

## [7.5.0](https://gitlab.com/simonoscr/nixfiles/compare/7.4.4...7.5.0) (2024-06-28)

### 🚀 Features

* update and enable alacritty instead of kitty and also update shortcuts for work profile i3 and cleanup ([0895fb2](https://gitlab.com/simonoscr/nixfiles/commit/0895fb27b3c76d8e0454fe4e94e5315c5b65d18d))

## [7.4.4](https://gitlab.com/simonoscr/nixfiles/compare/7.4.3...7.4.4) (2024-06-28)

### 🛠 Fixes

* add secret management to work profile ([51ec7fb](https://gitlab.com/simonoscr/nixfiles/commit/51ec7fbbaf9540e50c319aefb726d814a4e773a1))
* nix and nixpkgs configs ([04c456d](https://gitlab.com/simonoscr/nixfiles/commit/04c456dc9eff234634a7d05345662b04f9b226ef))

## [7.4.3](https://gitlab.com/simonoscr/nixfiles/compare/7.4.2...7.4.3) (2024-06-28)

### 🛠 Fixes

* work profile ([50074ed](https://gitlab.com/simonoscr/nixfiles/commit/50074ed207497687e87fe7b7c6c97cec2de95280))

## [7.4.2](https://gitlab.com/simonoscr/nixfiles/compare/7.4.1...7.4.2) (2024-06-27)

### 📦 Other

* **flake:** update flake ([c135fe6](https://gitlab.com/simonoscr/nixfiles/commit/c135fe6f4d2cfe69737f75a31f1045f1828dd933))

### 🛠 Fixes

* nix config ([9c4a167](https://gitlab.com/simonoscr/nixfiles/commit/9c4a167dc92147972660696e250203f5a72e10e0))
* ssh for server ([54b6a96](https://gitlab.com/simonoscr/nixfiles/commit/54b6a962b488c73475e06f09c53823a7fe71a280))

## [7.4.1](https://gitlab.com/simonoscr/nixfiles/compare/7.4.0...7.4.1) (2024-06-27)

### 🛠 Fixes

* adjust git config ([e1abac9](https://gitlab.com/simonoscr/nixfiles/commit/e1abac9d59d808696577b85bfadf20f5c4938a25))

## [7.4.0](https://gitlab.com/simonoscr/nixfiles/compare/7.3.1...7.4.0) (2024-06-27)

### 📦 Other

* **flake:** update flake ([7393a9b](https://gitlab.com/simonoscr/nixfiles/commit/7393a9b4b6f4f5d7c0ed530c0d842b66a377856d))

### 🚀 Features

* added hypr-gamemode script that does some magic like disabling effects, set performance mode, remove ags bar etc. ([66e932e](https://gitlab.com/simonoscr/nixfiles/commit/66e932e87eaae9fe515f082a11d73b54cd3bb6d4))
* locale setup ([c177dd4](https://gitlab.com/simonoscr/nixfiles/commit/c177dd44a12a5654b799a56804ee59c427dec23e))

### 🛠 Fixes

* set opener in yazi ([6b85dc9](https://gitlab.com/simonoscr/nixfiles/commit/6b85dc972fe879a3e10c58b48e8e9d5843212e2f))
* **graphics:** extraPackages not needed for amdgpu ([f6ac467](https://gitlab.com/simonoscr/nixfiles/commit/f6ac467a35ff781d2346daf2df07a3babd5e2887))

## [7.3.1](https://gitlab.com/simonoscr/nixfiles/compare/7.3.0...7.3.1) (2024-06-26)

### 📦 Other

* **flake:** update flake ([71fa7a2](https://gitlab.com/simonoscr/nixfiles/commit/71fa7a21a8ccc66b711cdd5e8cce434bc4603401))
* **flake:** update flake ([f304610](https://gitlab.com/simonoscr/nixfiles/commit/f30461083395b5b06a710e1d5632cd3c9d0efcfd))

### 🦊 CI/CD

* no tag until gitlab-runner is running ([7b87d27](https://gitlab.com/simonoscr/nixfiles/commit/7b87d27dbfca9ec3d0595fd4efdf0694ba018518))

### 🛠 Fixes

* cleanup home-manager for desktop ([21e8385](https://gitlab.com/simonoscr/nixfiles/commit/21e8385e82818d56d3b3a08b6a83d17f83709cf2))
* enable hyprland vfr and atomic set ([c38c357](https://gitlab.com/simonoscr/nixfiles/commit/c38c357bf794bbece9804857d2770dc389678bd8))
* firefox profile for work ([8a21c46](https://gitlab.com/simonoscr/nixfiles/commit/8a21c4668006ce136c44d165b920c7bc508d0b02))
* gitlab-runner with podman WIP ([d76cf86](https://gitlab.com/simonoscr/nixfiles/commit/d76cf8642ef235a4099b475f5c169bec62e3bee8))
* hardware and security changes and cleanups ([c263414](https://gitlab.com/simonoscr/nixfiles/commit/c263414b084801f9a566fcbc2e865eeddb2a204a))
* hyprland things ([3a1581f](https://gitlab.com/simonoscr/nixfiles/commit/3a1581ffe7417a417157010875fbd180cdb2fa6a))

## [7.3.0](https://gitlab.com/simonoscr/nixfiles/compare/7.2.0...7.3.0) (2024-06-24)

### 📦 Other

* **flake:** update flake ([3cdbd31](https://gitlab.com/simonoscr/nixfiles/commit/3cdbd31f75d83568705a4e19e633ff38fd3314ea))
* **lint:** remove yamllin ([c31f502](https://gitlab.com/simonoscr/nixfiles/commit/c31f5022a124e46b8199dca2a9a321de44816f9a))

### 🚀 Features

* new amdgpu and graphic options and remove deprecated ([5404487](https://gitlab.com/simonoscr/nixfiles/commit/5404487fdb7c50e21e0ecdeb25210a578b571fa8))

### 🛠 Fixes

* add packages ([fe0923d](https://gitlab.com/simonoscr/nixfiles/commit/fe0923dbf8ddf3be8ce263d2228ee5d5a116c2f6))

## [7.2.0](https://gitlab.com/simonoscr/nixfiles/compare/7.1.8...7.2.0) (2024-06-20)

### :scissors: Refactor

* cleanup double entries and move some keys ([1ff0e00](https://gitlab.com/simonoscr/nixfiles/commit/1ff0e00b9f22ce4e32762b92310695d6eed23746))

### 🚀 Features

* added iso configuration to flake for server and desktop ([864812a](https://gitlab.com/simonoscr/nixfiles/commit/864812af45280ba6b88b320fbe6c5535e6790e3d))

## [7.1.8](https://gitlab.com/simonoscr/nixfiles/compare/7.1.7...7.1.8) (2024-06-20)

### 🛠 Fixes

* update hyprland settings and configuration ([5c04611](https://gitlab.com/simonoscr/nixfiles/commit/5c04611c72b85c315147419fde69728238ab393c))

## [7.1.7](https://gitlab.com/simonoscr/nixfiles/compare/7.1.6...7.1.7) (2024-06-20)

### 🛠 Fixes

* **work:** add cosign to packages ([ba95139](https://gitlab.com/simonoscr/nixfiles/commit/ba951399fda5460a81c7006e8699c35cad1be255))

## [7.1.6](https://gitlab.com/simonoscr/nixfiles/compare/7.1.5...7.1.6) (2024-06-20)

### 🛠 Fixes

* work profile i3 setup and configs ([060ae87](https://gitlab.com/simonoscr/nixfiles/commit/060ae87efad974bb61b1866b1c8ab4500752b39f))
* work profile i3 setup and configs for autorandr ([6808991](https://gitlab.com/simonoscr/nixfiles/commit/68089916b2b39cc788b4490d265ecfd2e276d62b))

## [7.1.5](https://gitlab.com/simonoscr/nixfiles/compare/7.1.4...7.1.5) (2024-06-19)

### 📦 Other

* **flake:** update flake ([a70fd49](https://gitlab.com/simonoscr/nixfiles/commit/a70fd49586ff2ec8465fd08ea15420149fdec0b7))
* **flake:** update flake ([1be28c8](https://gitlab.com/simonoscr/nixfiles/commit/1be28c8cffce4caf876e22b94f8a51f3ee91ad5f))

### 🛠 Fixes

* work profile i3 setup and configs ([e96c9bf](https://gitlab.com/simonoscr/nixfiles/commit/e96c9bfa8265b747cd3c1cde50625e2c7a0cde8e))

## [7.1.4](https://gitlab.com/simonoscr/nixfiles/compare/7.1.3...7.1.4) (2024-06-18)

### 🛠 Fixes

* work profile i3 setup and configs ([e0b9283](https://gitlab.com/simonoscr/nixfiles/commit/e0b9283fea8fcc0cf59d688a3a5727014691bdc7))

## [7.1.3](https://gitlab.com/simonoscr/nixfiles/compare/7.1.2...7.1.3) (2024-06-18)

### 🛠 Fixes

* adjust work profile with custom configs ([8396f6b](https://gitlab.com/simonoscr/nixfiles/commit/8396f6b852fbb843ccc87d6fa31b50d020fc56b0))

## [7.1.2](https://gitlab.com/simonoscr/nixfiles/compare/7.1.1...7.1.2) (2024-06-18)

### 🛠 Fixes

* adjust work profile with custom configs ([91a1245](https://gitlab.com/simonoscr/nixfiles/commit/91a1245e55efa6eaa690c51a4924c48c47032dcf))
* adjust work profile with custom configs ([c7478cf](https://gitlab.com/simonoscr/nixfiles/commit/c7478cffb80cc94935a12d1048759ddb80972dab))
* adjust work profile with custom configs ([aafb950](https://gitlab.com/simonoscr/nixfiles/commit/aafb950b91cfdb4bbfe07a7721d50df6e4c690dc))
* adjust work profile with custom configs ([34c49ca](https://gitlab.com/simonoscr/nixfiles/commit/34c49ca5ae064a02d90390a133cbfb21d8eb5eac))
* adjust work profile with custom configs ([07fbc64](https://gitlab.com/simonoscr/nixfiles/commit/07fbc645e6fe24e515f595aaab22794c659f6a92))
* adjust work profile with custom configs ([b8c7caf](https://gitlab.com/simonoscr/nixfiles/commit/b8c7caf47365cfa5477bbf399b8f0a82283ce217))
* adjust work profile with custom configs ([084c0df](https://gitlab.com/simonoscr/nixfiles/commit/084c0dfa18caaa1ece723998bdc29876cf819ab7))
* adjust work profile with custom configs ([b9fb8f7](https://gitlab.com/simonoscr/nixfiles/commit/b9fb8f77d292850331ad5d468368c6ce6b177eb2))
* adjust work profile with custom configs ([bc5c7b6](https://gitlab.com/simonoscr/nixfiles/commit/bc5c7b65e0e94a8ac4878df5e1648cd64d6bab5f))

## [7.1.1](https://gitlab.com/simonoscr/nixfiles/compare/7.1.0...7.1.1) (2024-06-18)

### 🛠 Fixes

* work profile ([9b02895](https://gitlab.com/simonoscr/nixfiles/commit/9b02895d2d052c5b16f3f0e969e6e7322e4bec85))

## [7.1.0](https://gitlab.com/simonoscr/nixfiles/compare/7.0.6...7.1.0) (2024-06-17)

### 💈 Style

* format repo with nixfmt-rfc-style ([60bf5ae](https://gitlab.com/simonoscr/nixfiles/commit/60bf5aed52226153f8374531b9796e7176fad50a))

### 🚀 Features

* cleanup and format ([697198e](https://gitlab.com/simonoscr/nixfiles/commit/697198e3a8709c3a331af2346dfeebf4562da493))

## [7.0.6](https://gitlab.com/simonoscr/nixfiles/compare/7.0.5...7.0.6) (2024-06-17)

### 💈 Style

* format with nixfmt-rfc-style ([9ec1298](https://gitlab.com/simonoscr/nixfiles/commit/9ec129858c7024f3352b394ee9e0edbe8200fce7))

### 🛠 Fixes

* **editor:** huge update to vscodium settings and extensions ([54f20bb](https://gitlab.com/simonoscr/nixfiles/commit/54f20bb8adeee8e1fb2abb68ec7c4661156215be))
* **fonts:** adjust font config and added font config to work profile ([ec64082](https://gitlab.com/simonoscr/nixfiles/commit/ec64082e501a4c235cc50c5e4d841dfb949851f3))

## [7.0.5](https://gitlab.com/simonoscr/nixfiles/compare/7.0.4...7.0.5) (2024-06-17)

### :scissors: Refactor

* format repository with nixfmt-rfc-style ([aa6d171](https://gitlab.com/simonoscr/nixfiles/commit/aa6d1712be8e5c3bad7d027a9f843fc116b52ac0))

### 📦 Other

* **flake:** update flake ([78eef32](https://gitlab.com/simonoscr/nixfiles/commit/78eef324b30bed1f1db743b9b99044da9922cb08))

### 🛠 Fixes

* add nixfmt-rfc-style formatter to pre-commit-hook ([c81a47e](https://gitlab.com/simonoscr/nixfiles/commit/c81a47e99c925d396317f4caf5eb034d661d8906))
* small fixes ([7275678](https://gitlab.com/simonoscr/nixfiles/commit/727567834123fceae110c1612c892f2b00d67a24))
* try nixfmt for pre-commit-hook ([9e7dd47](https://gitlab.com/simonoscr/nixfiles/commit/9e7dd47f8df5d9b9aba9c480aff6b27eb4e87d15))
* use nix-pre-commit-hooks ([e7bdcf9](https://gitlab.com/simonoscr/nixfiles/commit/e7bdcf9d8424a5e226c2459a5a2e67ffc6527616))

## [7.0.4](https://gitlab.com/simonoscr/nixfiles/compare/7.0.3...7.0.4) (2024-06-16)

### 🛠 Fixes

* cleanup unsued .nix files and update hyprlock ([465b1b4](https://gitlab.com/simonoscr/nixfiles/commit/465b1b4f21695fb2d58590e905d8a145694791c1))
* **ags:** add powerprofile and adjust dependencies ([f54a94b](https://gitlab.com/simonoscr/nixfiles/commit/f54a94b7ae50757c554e29e19c6391bb77afa699))
* **direnv:** adjust devshell ([49b5679](https://gitlab.com/simonoscr/nixfiles/commit/49b567914b5bd74192f4572045d8718f2fabbcd3))

## [7.0.3](https://gitlab.com/simonoscr/nixfiles/compare/7.0.2...7.0.3) (2024-06-16)

### 📦 Other

* **flake:** update flake ([aef59aa](https://gitlab.com/simonoscr/nixfiles/commit/aef59aa055689a7b1c64e69969daff533670a98f))

### 🛠 Fixes

* **greetd:** enable autologin to utilize hyprlock login-screen ([aceca86](https://gitlab.com/simonoscr/nixfiles/commit/aceca868a33760666297d31fef4bcb20781f05d3))
* **home-manager:** fix setup for work profile ([8008803](https://gitlab.com/simonoscr/nixfiles/commit/8008803edf98d1abc4632d2293f11bfd950d2d76))
* **network:** add fail2ban and restructure for server ([c68dc6b](https://gitlab.com/simonoscr/nixfiles/commit/c68dc6ba3e4d8444c9253dd786d0daba44da7abb))
* **nix:** nix settings ([52d5ae5](https://gitlab.com/simonoscr/nixfiles/commit/52d5ae51daf87fbe0d53e0e09cb1e85abf2b1b0a))
* **server:** add git as module and adjust zsh settings for server ([38b64a9](https://gitlab.com/simonoscr/nixfiles/commit/38b64a9465c86b8621d2d970ba6fde05c0fbeda6))

## [7.0.2](https://gitlab.com/simonoscr/nixfiles/compare/7.0.1...7.0.2) (2024-06-16)

### 💈 Style

* **cursor:** switch cursor style to bibata ([9a5a341](https://gitlab.com/simonoscr/nixfiles/commit/9a5a3418255f7121ab1638b101019a2f0b700589))

### 📦 Other

* **flake:** update flake ([b62cd4f](https://gitlab.com/simonoscr/nixfiles/commit/b62cd4fa59d08e70274f9ea39ecc9586b00faf1b))

### 🛠 Fixes

* add sops-nix config file for profile w ([825ee26](https://gitlab.com/simonoscr/nixfiles/commit/825ee26aece2706e8b4522df842f8b391a2258fb))
* **ags:** update ags ([0e9909f](https://gitlab.com/simonoscr/nixfiles/commit/0e9909f39999930b98ace880efd62b04a3c7af77))
* **firefox:** update FF theme ([21ccab5](https://gitlab.com/simonoscr/nixfiles/commit/21ccab5c5b48c39dcf7abf549d68c5b479970110))
* **hypr:** update hyprland config and hypridle and hyprlock ([06be0a0](https://gitlab.com/simonoscr/nixfiles/commit/06be0a00248d606a022870081900253fc55de492))
* **hyprland:** remove deprecated setting ([f9eabf3](https://gitlab.com/simonoscr/nixfiles/commit/f9eabf318e550ff71ca33cbb6056c51d77cd3354))

## [7.0.1](https://gitlab.com/simonoscr/nixfiles/compare/7.0.0...7.0.1) (2024-06-15)

### 🛠 Fixes

* rename secret profile and adjust path for server secrets ([c8d2c02](https://gitlab.com/simonoscr/nixfiles/commit/c8d2c0263ae2ebee8d0b8dc3ab087fe2b1e5861e))

## [7.0.0](https://gitlab.com/simonoscr/nixfiles/compare/6.0.1...7.0.0) (2024-06-15)

### ✨ Milestone

* split home-manager profiles ([1c2a51f](https://gitlab.com/simonoscr/nixfiles/commit/1c2a51f6831f526616327af486a08b12e4c66511))

### 🛠 Fixes

* split sops from ssh config file to seperate file for server ([536eac8](https://gitlab.com/simonoscr/nixfiles/commit/536eac837896cccf6cb2b243c085321775fdd9d4))

## [6.0.1](https://gitlab.com/simonoscr/nixfiles/compare/6.0.0...6.0.1) (2024-06-15)

### 🛠 Fixes

* remove home-manager from server ([de7f96f](https://gitlab.com/simonoscr/nixfiles/commit/de7f96f349f645f6d66545f689dbc3d63f4b1e24))

## [6.0.0](https://gitlab.com/simonoscr/nixfiles/compare/5.11.1...6.0.0) (2024-06-15)

### ✨ Milestone

* split configuration ([c029aaf](https://gitlab.com/simonoscr/nixfiles/commit/c029aaffc7c8c4d24d10aeec4fd61db9df27e022))
* split configuration server and desktop conf ([e22e76f](https://gitlab.com/simonoscr/nixfiles/commit/e22e76f65ae5f62f4a6ea0fbd730300e57e46d20))

### 🛠 Fixes

* ssh agent dont add keys ([4b7f0b5](https://gitlab.com/simonoscr/nixfiles/commit/4b7f0b5bb374b619b1eb80a35f9d19f3c2ac06a6))

## [5.11.1](https://gitlab.com/simonoscr/nixfiles/compare/5.11.0...5.11.1) (2024-06-15)

### 📦 Other

* **flake:** update flake ([cbc29a5](https://gitlab.com/simonoscr/nixfiles/commit/cbc29a5383a419b69ab57ded51fb6ffc1a37d1f3))

### 🧪 Tests

* added unready devtoys package ([abac38e](https://gitlab.com/simonoscr/nixfiles/commit/abac38e64db66289b9682f46336605abb3006638))

### 🛠 Fixes

* disable credential-helper-libsecret ([013fba2](https://gitlab.com/simonoscr/nixfiles/commit/013fba210748f6063c94ae810cb892446b5ffcc1))
* secret and ssh key handling ([cdf79ba](https://gitlab.com/simonoscr/nixfiles/commit/cdf79ba936df898ec0a5c392234fb685c92f6118))
* update flake and set correct FLAKE url for nh ([b1dbaca](https://gitlab.com/simonoscr/nixfiles/commit/b1dbaca2003f4863998fe564e04562a21f1b3ccf))
* **git:** added git-credential-helper with libsecret ([8793463](https://gitlab.com/simonoscr/nixfiles/commit/8793463565d4e9e8ad28cf339c582185ae60fc50))

## [5.11.0](https://gitlab.com/simonoscr/nixfiles/compare/5.10.2...5.11.0) (2024-06-14)

### 📦 Other

* **flake:** update flock ([3ed5b7b](https://gitlab.com/simonoscr/nixfiles/commit/3ed5b7b51dbcc45c69ce5ba430b6071c8e0dcbf9))
* **flake:** update flock ([ec60bbe](https://gitlab.com/simonoscr/nixfiles/commit/ec60bbe20c796b417d8f4391195cd139e6b2848f))

### 🚀 Features

* **modules:** greetd ([7fa72ae](https://gitlab.com/simonoscr/nixfiles/commit/7fa72ae2c34330a10397fa95786019dd30613765))

### 🛠 Fixes

* enable r2modman ([0294965](https://gitlab.com/simonoscr/nixfiles/commit/0294965e854860a633cae48497c4448e0c873f44))
* remove gtk4 files on postinstall ([c8406a5](https://gitlab.com/simonoscr/nixfiles/commit/c8406a56eb4bebd3122f48d8903bd6aeeb7b78c4))
* **hyprland:** update hyprlock, hypridle and hyprland settings and config ([df387f2](https://gitlab.com/simonoscr/nixfiles/commit/df387f2ffb5938ded99a3cca00a9fd0d5e1758f1))
* **network:** move hosts to extra entry ([f86d271](https://gitlab.com/simonoscr/nixfiles/commit/f86d27177a6a61732d115149a6466807dba190c8))

## [5.10.2](https://gitlab.com/simonoscr/nixfiles/compare/5.10.1...5.10.2) (2024-06-10)

### 📦 Other

* **flake:** update flock ([e83f2a2](https://gitlab.com/simonoscr/nixfiles/commit/e83f2a20fd2ddc27855591ce67aeaafe429a7ab6))

### 🛠 Fixes

* allowed- and trusted-users because simon is already in the wheel group ([1571670](https://gitlab.com/simonoscr/nixfiles/commit/1571670289f278555e9e9f88ecf5171f8a991c38))
* remove direnv entry i already have a direnv.nix file ([c79fe35](https://gitlab.com/simonoscr/nixfiles/commit/c79fe35c61871d45f53a6e69e0de5642b136cd09))
* remove unused nix config files ([055a25d](https://gitlab.com/simonoscr/nixfiles/commit/055a25d0ce09c1db9bfaa8fd62bbac6132798cc0))
* rename virtualisation.nix to podmand.nix and disable libvirtd ([2f61f8a](https://gitlab.com/simonoscr/nixfiles/commit/2f61f8aa56b07adfa7bf26114a70a08fe038400e))
* rename virtualisation.nix to podmand.nix and disable libvirtd ([7f55a79](https://gitlab.com/simonoscr/nixfiles/commit/7f55a792a7edde26c97b6ff9042b52e7469e877d))

## [5.10.1](https://gitlab.com/simonoscr/nixfiles/compare/5.10.0...5.10.1) (2024-06-10)

### 🛠 Fixes

* remove containderd ([d45307a](https://gitlab.com/simonoscr/nixfiles/commit/d45307a52e9b25e45bc459820a68c0599b0bd7f9))

## [5.10.0](https://gitlab.com/simonoscr/nixfiles/compare/5.9.0...5.10.0) (2024-06-10)

### 📦 Other

* **flake:** update flock ([10a425e](https://gitlab.com/simonoscr/nixfiles/commit/10a425e00dd53798832bb8d83ad89ba333d17c75))
* **flake:** update flock ([680b9ba](https://gitlab.com/simonoscr/nixfiles/commit/680b9badb8712caf3879cd28b8f97b2d333b3747))
* **flake:** update flock and pre-commit-hook flake-shell ([cabaebe](https://gitlab.com/simonoscr/nixfiles/commit/cabaebe3d668a2dc6a59260942c82c90dfc98688))

### 🚀 Features

* add openrecall module ([e5a2854](https://gitlab.com/simonoscr/nixfiles/commit/e5a2854ac47613a6be38e878b166ebf6ac641954))
* **network:** add quad9 dns and dnsovertls ([703772b](https://gitlab.com/simonoscr/nixfiles/commit/703772b9c6b4d93fbf62809a8f382f79c8dd9c63))

### 🛠 Fixes

* disable gamescope module ([46905f5](https://gitlab.com/simonoscr/nixfiles/commit/46905f50e19f72fe53bfbddecbce9130b29e6a83))
* disable gamescope, add but disable openrecall .nix ([8ed1bf7](https://gitlab.com/simonoscr/nixfiles/commit/8ed1bf7c789ffa3d25dbb614809be236650fc415))
* home-manager backupFileExtension ([59d49ac](https://gitlab.com/simonoscr/nixfiles/commit/59d49ac939b98c6317e8b4f3ec87570822ea24d5))
* hyprland move error banner to bottom, disable xwaylandbridge ([3aae032](https://gitlab.com/simonoscr/nixfiles/commit/3aae0321e142aacc0884547b11bbfe040b74285e))
* nixfmt rfc style from alejandra ([c6a2c52](https://gitlab.com/simonoscr/nixfiles/commit/c6a2c52253ba98d1f15b106d2432cb8fd77e469a))
* pre-commit-hook with nixfmt-rfc-style instead of alejandra ([552b652](https://gitlab.com/simonoscr/nixfiles/commit/552b6528ace4c5b613821423494456c96535e6b6))
* **firefox:** update hash for theme and update policies and extensions ([919e868](https://gitlab.com/simonoscr/nixfiles/commit/919e868794a3dbce411219d84c30bf408deb1400))
* **mangohud:** add vkbasalt to mangohud config and update unused packages ([318cb22](https://gitlab.com/simonoscr/nixfiles/commit/318cb2238c7badf84cc93ac336b6f5d1d36454c5))
* **steam:** removed vkbasalt from steam config and remove extest ([edd1d33](https://gitlab.com/simonoscr/nixfiles/commit/edd1d33c952378b7ae94b556bf7917368694fa26))

## [5.9.0](https://gitlab.com/simonoscr/nixfiles/compare/5.8.2...5.9.0) (2024-06-08)

### 📦 Other

* **flake:** update flock ([934679f](https://gitlab.com/simonoscr/nixfiles/commit/934679fb6b9de7be35a23f060029a431d97b658e))

### 🚀 Features

* add xdg mimeApps ([7d3a6db](https://gitlab.com/simonoscr/nixfiles/commit/7d3a6dbda89b80d7f1d24508fb5afa5abe990942))

### 🛠 Fixes

* adjust greetd login message ([2bdbd03](https://gitlab.com/simonoscr/nixfiles/commit/2bdbd03457d2b6c2653d84d3c446326c11c0e37e))
* disable containerd ([46b54a5](https://gitlab.com/simonoscr/nixfiles/commit/46b54a536c2ca2ba76aa4baaf65dc8a974b913c0))
* disable xdg-desktop-portal-wl ([c0d075f](https://gitlab.com/simonoscr/nixfiles/commit/c0d075f8fd199801d91b05e3155c1fa9bcb05b4b))
* remove bluetooth ([99d85e2](https://gitlab.com/simonoscr/nixfiles/commit/99d85e2cab40e0c2f032d6e62a55e666ed08a5a1))
* update dbus config ([b419030](https://gitlab.com/simonoscr/nixfiles/commit/b4190309f02888979679cd5f77e035787e6c67a5))
* update tailscale config ([cd6a181](https://gitlab.com/simonoscr/nixfiles/commit/cd6a181b3f70279d301a34c117448eb9657c8701))
* **profile:** add yq to work profile ([0e36f78](https://gitlab.com/simonoscr/nixfiles/commit/0e36f78502806cf583e43bb9e3a1ea3d17ad5144))

## [5.8.2](https://gitlab.com/simonoscr/nixfiles/compare/5.8.1...5.8.2) (2024-06-04)

### 📦 Other

* **flake:** update flock ([db2e5fe](https://gitlab.com/simonoscr/nixfiles/commit/db2e5fe6a6e9bdd9f7634c71f21d926046f11804))

### 🛠 Fixes

* **sops-nix:** add to sharedModule ([d679f31](https://gitlab.com/simonoscr/nixfiles/commit/d679f31f5bedbac115de920c90c7b12521867fb9))

## [5.8.1](https://gitlab.com/simonoscr/nixfiles/compare/5.8.0...5.8.1) (2024-06-03)

### 📦 Other

* **flake:** update flock ([c0559d6](https://gitlab.com/simonoscr/nixfiles/commit/c0559d6482eb828170443688b69868e333ba1ffe))

### 🛠 Fixes

* nh flake env ([a40d5ef](https://gitlab.com/simonoscr/nixfiles/commit/a40d5efe9168c0f3872b2e6e711df951c5ae6400))
* **firefox:** move from startpage to duckduckgo search ([d250734](https://gitlab.com/simonoscr/nixfiles/commit/d2507343efd066ebd06490128ce8768489b5dee3))

## [5.8.0](https://gitlab.com/simonoscr/nixfiles/compare/5.7.0...5.8.0) (2024-06-03)

### 🚀 Features

* work setup with i3 etc ([359319b](https://gitlab.com/simonoscr/nixfiles/commit/359319b352e7bfc01eb6c2c764aef921feab51d0))

## [5.7.0](https://gitlab.com/simonoscr/nixfiles/compare/5.6.0...5.7.0) (2024-06-02)

### 🚀 Features

* update packages for server ([4e0a300](https://gitlab.com/simonoscr/nixfiles/commit/4e0a30052cdb7985925e4fe6a756ae7a24e6b70a))

## [5.6.0](https://gitlab.com/simonoscr/nixfiles/compare/5.5.2...5.6.0) (2024-06-02)

### 📦 Other

* **flake:** update flock ([97a59f9](https://gitlab.com/simonoscr/nixfiles/commit/97a59f94b59b749c832cf3d24728ee105e97b226))

### 🦊 CI/CD

* update semantic-release job ([fe19ba9](https://gitlab.com/simonoscr/nixfiles/commit/fe19ba93cc5722015a0375d8d78502e011b8b5d8))

### 🚀 Features

* k3s+helm setup ([5dc8c79](https://gitlab.com/simonoscr/nixfiles/commit/5dc8c799d932a4321cb0ff93b48372499316848e))
* **server:** k3s and helm module to install k3s and helm and install and update helm-charts directly on k3s and network settings ([aea41ac](https://gitlab.com/simonoscr/nixfiles/commit/aea41ac72f35937080334295114be1a83fec7033))

### 🛠 Fixes

* add syncOptions CreateNamespace true ([f803775](https://gitlab.com/simonoscr/nixfiles/commit/f803775c8b64b4b33186263829a2561e7259af6a))

## [5.5.2](https://gitlab.com/simonoscr/nixfiles/compare/5.5.1...5.5.2) (2024-05-31)


### 📦 Other

* **flake:** update flock ([cba9072](https://gitlab.com/simonoscr/nixfiles/commit/cba90724cd0d73b0b7d5c585c7d4fd078cb44d6b))


### 🛠 Fixes

* **hyprland:** steam windowrules ([6910da4](https://gitlab.com/simonoscr/nixfiles/commit/6910da4e03f18b9dfd8deaca18796eab285297af))

## [5.5.1](https://gitlab.com/simonoscr/nixfiles/compare/5.5.0...5.5.1) (2024-05-31)


### 🛠 Fixes

* homeConfiguration for work profile ([88b2fa1](https://gitlab.com/simonoscr/nixfiles/commit/88b2fa171edf8bf7bd4b881b68bec31b2dac9ffe))

## [5.5.0](https://gitlab.com/simonoscr/nixfiles/compare/5.4.0...5.5.0) (2024-05-29)


### 📦 Other

* **flake:** update flock ([5a86324](https://gitlab.com/simonoscr/nixfiles/commit/5a863245c685cc7b90e9de3b7ff4fc002650ceac))


### 🚀 Features

* **desktop:** add ollama with oterm but disable it ([fc10b28](https://gitlab.com/simonoscr/nixfiles/commit/fc10b288b148eedcf7b6fa6d7dbb1307a8ab5980))


### 🛠 Fixes

* **hyprland:** update windows rules, hypridle, default settings ([2747f4b](https://gitlab.com/simonoscr/nixfiles/commit/2747f4bd26805a87a92b7c1ad16a9cfec3e8e8e6))
* **hyprland:** windowrules ([0676c4e](https://gitlab.com/simonoscr/nixfiles/commit/0676c4e3480574aca84906464bd87435fe0b15df))

## [5.4.0](https://gitlab.com/simonoscr/nixfiles/compare/5.3.0...5.4.0) (2024-05-27)


### 🚀 Features

* **ags:** move to fetchFromGitHub to automatically updating ags config instead of downloading manually ([b833f40](https://gitlab.com/simonoscr/nixfiles/commit/b833f4017e04e39b7c30d0f88f0b873fb985d93b))

## [5.3.0](https://gitlab.com/simonoscr/nixfiles/compare/5.2.14...5.3.0) (2024-05-27)


### 📦 Other

* **flake:** update flock ([3f1e240](https://gitlab.com/simonoscr/nixfiles/commit/3f1e2409841505c5026fc7f8c1cfc2b4d289d5e1))


### 🚀 Features

* **browser:** fetchFromGit for firefox theme to remove manual steps ([f544203](https://gitlab.com/simonoscr/nixfiles/commit/f544203a87d3ed9dd00fa72bb007b4a830e4e64c))
* **server:** k3s, argocd, helm, network settings and general configuration for server ([cda1f2f](https://gitlab.com/simonoscr/nixfiles/commit/cda1f2fc6454d83563d570b4d9dcb3910765fce0))


### 🛠 Fixes

* hyprland window rules and cleanup imports ([989c855](https://gitlab.com/simonoscr/nixfiles/commit/989c8559a883f2f98ca88ebbb126413f85e4d6da))

## [5.2.14](https://gitlab.com/simonoscr/nixfiles/compare/5.2.13...5.2.14) (2024-05-22)


### 📦 Other

* **flake:** update flock ([826966b](https://gitlab.com/simonoscr/nixfiles/commit/826966b9cb0549f9e8fe465e02ee034bd0611b5d))
* **flake:** update flock ([3a8e049](https://gitlab.com/simonoscr/nixfiles/commit/3a8e049a4df34b880e830d80bb9f3b2deab1b86b))
* **flake:** update flock ([bfc430e](https://gitlab.com/simonoscr/nixfiles/commit/bfc430ea9d6208d484d2a96b1fd2a901c234d77b))


### 🛠 Fixes

* disable old sound.enable setting for basic ALSA implentation (kind of deprecated) ([f0bcb90](https://gitlab.com/simonoscr/nixfiles/commit/f0bcb90298cbfe9252164e6b5a4307c5bac75303))
* **adio:** set dynamic HOME or XDG_CONFIG_HOME for config files ([c3f2e3f](https://gitlab.com/simonoscr/nixfiles/commit/c3f2e3ff420413f74b3dca8e8a4cbf8ca39ec283))
* **gaming:** add vkbasalt and config,, also adust mangohud config ([097b002](https://gitlab.com/simonoscr/nixfiles/commit/097b002afda3eded7371b4e54a519cebf1e850b8))
* **server:** adjust kubernetes deployment and add utils ([32ac77f](https://gitlab.com/simonoscr/nixfiles/commit/32ac77f5a2689b1c82b737f03478cdeeaab8a337))

## [5.2.13](https://gitlab.com/simonoscr/nixfiles/compare/5.2.12...5.2.13) (2024-05-20)


### 📦 Other

* **flake:** update flock ([08f565b](https://gitlab.com/simonoscr/nixfiles/commit/08f565b3f5a04c6ec487cf873600364440dec260))


### 🛠 Fixes

* disable bluetooth power on boot ([b6e92d8](https://gitlab.com/simonoscr/nixfiles/commit/b6e92d80fbbbd15e9b485318575d332e18115c57))
* firefox settings ([4e967a0](https://gitlab.com/simonoscr/nixfiles/commit/4e967a04d95c606577d56c07222276d1824124d9))
* hyprland things and add ags config file ([aa04654](https://gitlab.com/simonoscr/nixfiles/commit/aa04654e57c15ad6c4f186ec83ad69900e6fb6e7))
* update server related things with k8s ([270ff1c](https://gitlab.com/simonoscr/nixfiles/commit/270ff1c656ab5e93f8cdc7ac21ad2039e0318d69))
* **editors:** add extensions to vscodium ([b81653e](https://gitlab.com/simonoscr/nixfiles/commit/b81653e4a6fee4a88da471f10b71aa565bd35cae))
* **yazi:** ajdust settings for image preview and layout ([e7f22a8](https://gitlab.com/simonoscr/nixfiles/commit/e7f22a89875f394d645dcd617c51f9eb611303c8))

## [5.2.12](https://gitlab.com/simonoscr/nixfiles/compare/5.2.11...5.2.12) (2024-05-18)


### 📦 Other

* ref and update ([18926a6](https://gitlab.com/simonoscr/nixfiles/commit/18926a666c47307801e925bf5240585da33472b7))
* **flake:** update flock ([79f60ad](https://gitlab.com/simonoscr/nixfiles/commit/79f60ad5021fea510d2e719415e92e45154fd941))


### 🛠 Fixes

* add fs.file_max to sysctl for gaming ([3659b06](https://gitlab.com/simonoscr/nixfiles/commit/3659b06dd0a174016fbd85511951427679c1b3fd))
* hyprland settings, envs and rules ([211d7a8](https://gitlab.com/simonoscr/nixfiles/commit/211d7a87134c439ff2ef323350c90f5550921319))

## [5.2.11](https://gitlab.com/simonoscr/nixfiles/compare/5.2.10...5.2.11) (2024-05-17)


### 📔 Docs

* update README ([4856ce2](https://gitlab.com/simonoscr/nixfiles/commit/4856ce2a4e8a41aff75959d4bec7062fdb5a728f))


### 📦 Other

* **flake:** update flock ([9184371](https://gitlab.com/simonoscr/nixfiles/commit/918437190e1906027ea5c602951ac0047211bfc2))


### 🛠 Fixes

* add home.nix for work profile ([0811831](https://gitlab.com/simonoscr/nixfiles/commit/0811831fe2df41509b94940c43bd940a396e8e69))
* add homeConfiguration for work setup ([2582187](https://gitlab.com/simonoscr/nixfiles/commit/25821876cff9634ec32be090f367c284bc31da2c))
* hyprland plugins, settings and binds ([4319c50](https://gitlab.com/simonoscr/nixfiles/commit/4319c50cd54cf7c2fd8dc1248dada9e491efad36))
* mute shortcut for teamspeak ([9bb5eb1](https://gitlab.com/simonoscr/nixfiles/commit/9bb5eb1013f44cd6e1af8240d5f6b98ff7b655e1))
* update firefox theme ([351f3a9](https://gitlab.com/simonoscr/nixfiles/commit/351f3a9297e9dccbc1d7b484c104482f7b4261ef))

## [5.2.10](https://gitlab.com/simonoscr/nixfiles/compare/5.2.9...5.2.10) (2024-05-14)


### 📦 Other

* **flake:** update flock ([69b98bd](https://gitlab.com/simonoscr/nixfiles/commit/69b98bdc1c7d589c218ab1a8ee1c0601f3fdef1b))
* **flake:** update flock ([9822df4](https://gitlab.com/simonoscr/nixfiles/commit/9822df45b86b1b16e5e702a04fbf73f7455e19d6))


### 🛠 Fixes

* **hyprland:** update plugins and add hyprexpo and hyprexpo shortcut ([45688eb](https://gitlab.com/simonoscr/nixfiles/commit/45688ebbe604e674bd4b244d40759f038396df64))
* **vscodium:** make direnv restart automatic ([f74e6f8](https://gitlab.com/simonoscr/nixfiles/commit/f74e6f8b9a3f97fe63388b1b9ba96630d5937dea))

## [5.2.9](https://gitlab.com/simonoscr/nixfiles/compare/5.2.8...5.2.9) (2024-05-12)


### 📦 Other

* **flake:** update flock ([f002bee](https://gitlab.com/simonoscr/nixfiles/commit/f002beecd1a45d14ec147443dce3ac551f1be743))


### 🛠 Fixes

* add gamemode to extraGroups to user, zram settings and info,  Linux console font ([6116fc8](https://gitlab.com/simonoscr/nixfiles/commit/6116fc837b17edaf0309dcb33892f45e8bc5a975))
* hardware and boot changes ([84621ff](https://gitlab.com/simonoscr/nixfiles/commit/84621ffdf95086d75c5254bcbedc638a0b4c147c))
* more udiski settings ([f00fe2c](https://gitlab.com/simonoscr/nixfiles/commit/f00fe2c4d12ce0b81bcedd4eb0b919ed0191d3e7))
* remove thunar and adjust shortcuts to yazi ([1e13ef2](https://gitlab.com/simonoscr/nixfiles/commit/1e13ef296e5596c143b62cecaf1dd133f28f49d7))
* udev rules for drives and controller and card reader ([0565407](https://gitlab.com/simonoscr/nixfiles/commit/0565407895fac9d91111e10a35e1d71e59054191))
* **gaming:** set extest for steam, information about cap_sys for gamescope, enable inhibit_screensaver for gamemode ([cd6fb05](https://gitlab.com/simonoscr/nixfiles/commit/cd6fb0554b4649df8b159158dd769ef2bdbed693))

## [5.2.8](https://gitlab.com/simonoscr/nixfiles/compare/5.2.7...5.2.8) (2024-05-12)


### 📦 Other

* **flake:** update flock ([d6967e6](https://gitlab.com/simonoscr/nixfiles/commit/d6967e6dafd12e15d2ecaa6bc2ee0816bac9b950))


### 🛠 Fixes

* hypridle config now valid, remove unused mbsync.service, fix polkit.service config ([44c3eaf](https://gitlab.com/simonoscr/nixfiles/commit/44c3eaf9c515e0fe3917e0c3944f22c3138156e1))
* **audio:** pipewire rate to 48000 and fix configs for pipewire and wireplumber ([7e497a0](https://gitlab.com/simonoscr/nixfiles/commit/7e497a0bf115fa2b05099833f1bdfb4d27d7affe))
* **pre-commit-hook:** fix paths ([b2a50cf](https://gitlab.com/simonoscr/nixfiles/commit/b2a50cf6625a23701cea8517b1e388e390de0bfa))

## [5.2.7](https://gitlab.com/simonoscr/nixfiles/compare/5.2.6...5.2.7) (2024-05-12)


### 🛠 Fixes

* typo in pre-commit-hook.nix ([b8feb19](https://gitlab.com/simonoscr/nixfiles/commit/b8feb19ab65dfb0ff260fbac7e27c473850d1ee5))
* **pre-commit-hook:** fix missing input ([07b7b70](https://gitlab.com/simonoscr/nixfiles/commit/07b7b7026363558f6417f40e748204d378a7c015))

## [5.2.6](https://gitlab.com/simonoscr/nixfiles/compare/5.2.5...5.2.6) (2024-05-12)


### 🛠 Fixes

* **pre-commit-hook:** move pre-commit-hook config to a seperate file out of flake.nix ([7ff78ec](https://gitlab.com/simonoscr/nixfiles/commit/7ff78ecbfd3d0ba21a1cbdc50318ac1f9806fcfb))

## [5.2.5](https://gitlab.com/simonoscr/nixfiles/compare/5.2.4...5.2.5) (2024-05-11)


### 🛠 Fixes

* **audio:** more pipewire and wireplumber tweaks ([af96307](https://gitlab.com/simonoscr/nixfiles/commit/af96307175006256b6ede91516255750e0f23b04))

## [5.2.4](https://gitlab.com/simonoscr/nixfiles/compare/5.2.3...5.2.4) (2024-05-11)


### 🛠 Fixes

* update zsh and starship configurationand splitup files ([4568e8b](https://gitlab.com/simonoscr/nixfiles/commit/4568e8b1cdc5ec01d4b23029d8fce201b073b56c))

## [5.2.3](https://gitlab.com/simonoscr/nixfiles/compare/5.2.2...5.2.3) (2024-05-11)


### 🛠 Fixes

* **audio:** wireplumber audo channel-switch-config to remove easyeffects; and remove easyeffects ([420e052](https://gitlab.com/simonoscr/nixfiles/commit/420e05201eef418b4e2426b59e5d3d541e823a3e))

## [5.2.2](https://gitlab.com/simonoscr/nixfiles/compare/5.2.1...5.2.2) (2024-05-11)


### 📦 Other

* **flake:** update flock ([d044b9c](https://gitlab.com/simonoscr/nixfiles/commit/d044b9ce6187f0b8a3aac6181242e98c15295f41))


### 🛠 Fixes

* add aliases to zsh ([4b85b9c](https://gitlab.com/simonoscr/nixfiles/commit/4b85b9c3fc285ee33848f084cf106b1ffa67da24))

## [5.2.1](https://gitlab.com/simonoscr/nixfiles/compare/5.2.0...5.2.1) (2024-05-11)


### 📦 Other

* **flake:** update flock ([c27a7ce](https://gitlab.com/simonoscr/nixfiles/commit/c27a7ce75bf475a4129331c8574bd4f4a2b32b15))


### 🦊 CI/CD

* remove check job, because why running check in ci when having PRE-COMMIT(!)-hook? yeah ([8f9b743](https://gitlab.com/simonoscr/nixfiles/commit/8f9b743e225046170b8ab708f1373910fbcaf789))
* remove dead code from release stage ([489cfc5](https://gitlab.com/simonoscr/nixfiles/commit/489cfc595fd7737a65919c4d09b6d3f242f76f7f))


### 🛠 Fixes

* **editors/terminal:** add fonts to editors,emulators and terminals, adjust editorconfig ([c126a8f](https://gitlab.com/simonoscr/nixfiles/commit/c126a8fee8e13cfcb958224cf0b511820ce02c15))

## [5.2.0](https://gitlab.com/simonoscr/nixfiles/compare/5.1.3...5.2.0) (2024-05-11)


### 💈 Style

* lint2 ([83eeaba](https://gitlab.com/simonoscr/nixfiles/commit/83eeaba7897dd175c05b06ee0a74f990cb72e22e))


### 🚀 Features

* added new programs and services and refarctor some existing thing like fonts, gaming, programs, services for hm and nixos ([48135c4](https://gitlab.com/simonoscr/nixfiles/commit/48135c404e3c4c58ee386e402ce92d77304454e0))

## [5.1.3](https://gitlab.com/simonoscr/nixfiles/compare/5.1.2...5.1.3) (2024-05-11)


### 🛠 Fixes

* **ags:** add missing dependencies ([5849759](https://gitlab.com/simonoscr/nixfiles/commit/5849759280f3eebce8f452043f22d07c7c9d17ef))

## [5.1.2](https://gitlab.com/simonoscr/nixfiles/compare/5.1.1...5.1.2) (2024-05-11)


### :scissors: Refactor

* **cleanup:** hyprland files splitup and cleanup, inputs management sops-nix+hyprland+ags, update flock ([f159c03](https://gitlab.com/simonoscr/nixfiles/commit/f159c031e04266cceff68beda924329a4c51509d))

## [5.1.1](https://gitlab.com/simonoscr/nixfiles/compare/5.1.0...5.1.1) (2024-05-11)


### 📔 Docs

* remove docs folder, transfer to wiki ([ee719b2](https://gitlab.com/simonoscr/nixfiles/commit/ee719b21a324171e892f74ddb1f158d8156b1252))


### 📦 Other

* **flake:** update flake.lock ([6316d49](https://gitlab.com/simonoscr/nixfiles/commit/6316d4978e36abefbcca43c204c2d0290469aa44))
* **flake:** update flake.lock ([72e63f6](https://gitlab.com/simonoscr/nixfiles/commit/72e63f6b86d86bf2d39fec32b8caa2e91a58b1a0))


### 🛠 Fixes

* **gitignore:** added .direnv to gitignore git config ([acceb58](https://gitlab.com/simonoscr/nixfiles/commit/acceb58b3d83861a11dda0b7d2bc197d3aee2f90))

## [5.1.0](https://gitlab.com/simonoscr/nixfiles/compare/5.0.2...5.1.0) (2024-05-10)


### 📔 Docs

* update README for better formatting and reading ([a419969](https://gitlab.com/simonoscr/nixfiles/commit/a4199698e47feee874d65aa27ebe191a3ff11c5f))


### 🚀 Features

* add direnv to flake ([33ce09f](https://gitlab.com/simonoscr/nixfiles/commit/33ce09f76a3929bd19500408b12869bc9fd9b734))

## [5.0.2](https://gitlab.com/simonoscr/nixfiles/compare/5.0.1...5.0.2) (2024-05-10)


### 🛠 Fixes

* add pkgs.cachix to utilize own cachix cache ([a47031a](https://gitlab.com/simonoscr/nixfiles/commit/a47031a630f950a8672a0792655730b2867556e2))

## [5.0.1](https://gitlab.com/simonoscr/nixfiles/compare/5.0.0...5.0.1) (2024-05-10)


### 👀 Reverts

* **ci:** changes to the token variable for gitlab ([3fefce0](https://gitlab.com/simonoscr/nixfiles/commit/3fefce0fa639e976d6596192887156d356962a16))


### 💈 Style

* **format:** statix code; a pattern is empty, use `_` instead ([994bcc9](https://gitlab.com/simonoscr/nixfiles/commit/994bcc9a851b2288aab9d1f6fee11139293a1842))


### 🦊 CI/CD

* **fix:** repository url for nixsecrets ([210a9e2](https://gitlab.com/simonoscr/nixfiles/commit/210a9e2179677029e13c53fc83b2912d3d245867))
* **fix:** update pipeline-token variable ([4ef7fbb](https://gitlab.com/simonoscr/nixfiles/commit/4ef7fbb193b0c58174eb8bc2055fd47da86ca386))


### 🛠 Fixes

* **sops-nix:** to get secrets working with sops-nix in ci, but also when building locally; so with a dynamic CI part to determine the environment where sops-nix got executed ([13f359b](https://gitlab.com/simonoscr/nixfiles/commit/13f359bd2fca42617242fc7942b88aedd515a6aa))

## [5.0.0](https://gitlab.com/simonoscr/nixfiles/compare/4.0.1...5.0.0) (2024-05-10)


### :scissors: Refactor

* **pre-commit-hooks:** fix different linter and formatting issues; and removed things that pre-commit-hook wanted to be removed ([50a8a7b](https://gitlab.com/simonoscr/nixfiles/commit/50a8a7b8d9c1b5f16cc0ff89cbbf0a0efbf973ba))


### 👀 Reverts

*  remove added pre-commit-hook substituter for cachix cache ([ad37e1e](https://gitlab.com/simonoscr/nixfiles/commit/ad37e1ec3620a6b834583b8946e569ca0d4f6d37))


### 💈 Style

* cleanup code ([d473c86](https://gitlab.com/simonoscr/nixfiles/commit/d473c86d629aa96f1d411f4a74f8b81b1b033770))


### 📔 Docs

* add flake-parts information and add pre-commit badge ([ab4bd29](https://gitlab.com/simonoscr/nixfiles/commit/ab4bd29d8ec45acca65474079aa90fc5f765b698))


### 📦 Other

* **.gitignore:** add pre-commit-config to gitignore ([8160a27](https://gitlab.com/simonoscr/nixfiles/commit/8160a279b4d883f646dc8d1d42c00b447c6ecb9c))


### 🦊 CI/CD

* use nix flake check with pre-commit-hooks ([46955e8](https://gitlab.com/simonoscr/nixfiles/commit/46955e8bae26694614d544e07513a5b69931cc8d))


### 🧨 Breaking changes!

* switch to flake-parts for flake; not compatible with versions older versions ([f727109](https://gitlab.com/simonoscr/nixfiles/commit/f72710998e939597af931eec3510581ed0da8e4a))


### 🛠 Fixes

* disable sops-nix because cant run in ci env ([4d16b65](https://gitlab.com/simonoscr/nixfiles/commit/4d16b65c65fa1289fe10272b3de851ec66094570))
* dont use oauth2 for gitlab verification ([c706f5b](https://gitlab.com/simonoscr/nixfiles/commit/c706f5b5fdd74d7b071465663163a5c46abb5857))
* get nixsecrets repo with oauth2 and https instead of ssh ([6eeb201](https://gitlab.com/simonoscr/nixfiles/commit/6eeb2011f14d54abfba3a379163061e49406114d))
* **ci:** ssh fetchGit not working in gitlab pipeline; also a dependency-loop with this setup ([355c78b](https://gitlab.com/simonoscr/nixfiles/commit/355c78b7954e59136a4a678e4b5a3de200e8e8ed))
* **sops-nix:** missing needed rev on fetchGit ([c2eac8c](https://gitlab.com/simonoscr/nixfiles/commit/c2eac8c89a1f3887a488b62f46b5c5a565de9dba))

## [4.0.1](https://gitlab.com/simonoscr/nixfiles/compare/4.0.0...4.0.1) (2024-05-09)


### 💈 Style

* **nix:** format nix code ([5f69c7d](https://gitlab.com/simonoscr/nixfiles/commit/5f69c7d9004fd770c9046a95deb8dcbc0eade70f))


### 📦 Other

* **semantic-release:** cleanup unsued things ([3b2fd3d](https://gitlab.com/simonoscr/nixfiles/commit/3b2fd3db739651071c6c177f18e86c527d6e575c))


### 🛠 Fixes

* **sops:** add pulling private nixsecrets repo for sops setup; remove NUR; adjust flake.nix ([4b3c7f0](https://gitlab.com/simonoscr/nixfiles/commit/4b3c7f02bd89c401ef179dbf47fd8787a3690ac0))

## [4.0.0](https://gitlab.com/simonoscr/nixfiles/compare/3.0.0...4.0.0) (2024-05-09)


### 💈 Style

* **nix:** format nix code ([cbafa7b](https://gitlab.com/simonoscr/nixfiles/commit/cbafa7bf32e7ec28ae712c60f2843d25f378243c))


### 📔 Docs

* added multiple READMEs to folders ([69d24bc](https://gitlab.com/simonoscr/nixfiles/commit/69d24bc1bf0cacb6edb72c6b204d6199a9fec782))


### 📦 Other

* **flake:** cleanup flake.nix and update flake.lock ([fe06b28](https://gitlab.com/simonoscr/nixfiles/commit/fe06b28f0471e893b41b9907ebad77128db342a0))


### 🦊 CI/CD

* fix deadnix dead code fixes ([38a73ee](https://gitlab.com/simonoscr/nixfiles/commit/38a73eedff9ebcae1f24f29e3f9c8f30ca45810f))
* **fix:** remove before_script from release job to use default before_script ([adcff49](https://gitlab.com/simonoscr/nixfiles/commit/adcff49e0a90add26987d08f72c9ad24554ced80))
* **fix:** remove cachix from release job before_script ([d337d38](https://gitlab.com/simonoscr/nixfiles/commit/d337d382cd85a9a28526aafa492ad04b621376a7))
* **fix:** set extra before_script for release job ([2a1dd6c](https://gitlab.com/simonoscr/nixfiles/commit/2a1dd6cf51ac7aa2b85d3b4c46a63cca207db3bb))


### 🧨 Breaking changes!

* refactor home-manager configurations like in the release before to better reflect programs, services etc and find things faster ([7dacad4](https://gitlab.com/simonoscr/nixfiles/commit/7dacad461c254b376c24fbebaea1792a16fa55c3))


### 🛠 Fixes

* missing boolean in bash.nix ([b39632e](https://gitlab.com/simonoscr/nixfiles/commit/b39632e88a126b9900520b2450e8b225f4c88541))

## [3.0.0](https://gitlab.com/simonoscr/nixfiles/compare/2.7.2...3.0.0) (2024-05-09)


### 🧨 Breaking changes!

* **refactor:** restructure repo to better reflect what files are used in different systems ([736a1d0](https://gitlab.com/simonoscr/nixfiles/commit/736a1d09ec929817ff39a1bfd0fe1e563d7364f8))

## [2.7.2](https://gitlab.com/simonoscr/nixfiles/compare/2.7.1...2.7.2) (2024-05-09)


### ⚠ BREAKING CHANGES

* restructure repo to better reflect what files are used in different hosts; also deleted unused file and split many files up to see better where to find what

### :scissors: Refactor

* restructure repo to better reflect what files are used in different hosts; also deleted unused file and split many files up to see better where to find what ([8d5369a](https://gitlab.com/simonoscr/nixfiles/commit/8d5369a575005606fdda804ed30dfadcffd922a9))


### 📔 Docs

* update README ([8aa3054](https://gitlab.com/simonoscr/nixfiles/commit/8aa3054631dfdd5bc042b3bfc4cf84c1be4ac41d))


### 📦 Other

* **flake:** update flake.lock ([1519f96](https://gitlab.com/simonoscr/nixfiles/commit/1519f96ca2e0eb4ccb8b65050b38f76f294bf773))
* **flake:** update flake.lock ([3ab156e](https://gitlab.com/simonoscr/nixfiles/commit/3ab156e9133b690f77cd63a9c43a89f819ba20b3))


### 🦊 CI/CD

* switch default image ([41c757b](https://gitlab.com/simonoscr/nixfiles/commit/41c757b210185cf90faa805f1f8611f791259666))
* test build and push to cachix ([b0068bf](https://gitlab.com/simonoscr/nixfiles/commit/b0068bfc0cda1c1cb98fbddb0f389fcffbab05a3))
* **fix:** remove ${CI_PROJECT_DIR} from nix build, invalid url just use "." ([b755027](https://gitlab.com/simonoscr/nixfiles/commit/b755027dfe3a7c83b93cb25b4f41bd7abd45cfe3))
* **fix:** valid build stage ([25f6c5f](https://gitlab.com/simonoscr/nixfiles/commit/25f6c5f2bf414045f7ffcd53effb175cc737a2ca))
* **test:** fix typo in nix build ([5d37783](https://gitlab.com/simonoscr/nixfiles/commit/5d37783e91d236072eaf6b3cfffa0c622d432ea4))


### 🧪 Tests

* **ci:** add jq to get inputs value.path from nix flake json archive ([0ebe4d7](https://gitlab.com/simonoscr/nixfiles/commit/0ebe4d71cde077940711251b46cbd63baa1b3fd9))
* **ci:** add jq with nix-shell --run and execute nix flake archive in pipeline ([31de671](https://gitlab.com/simonoscr/nixfiles/commit/31de671f765a9a7c87922499dc98d37311a5d5bd))
* **ci:** build in pipeline and push to cachix ([27d1c86](https://gitlab.com/simonoscr/nixfiles/commit/27d1c8600309b829517805caf4e838da2cc064fa))
* **ci:** nix build flake ([3a21da3](https://gitlab.com/simonoscr/nixfiles/commit/3a21da34187f1f4e126c0c295535fd90bf561522))
* **ci:** test nix flake archive instead of nix build, because gitlab-runner has not enought disk space ([54b049d](https://gitlab.com/simonoscr/nixfiles/commit/54b049d254ff606dcd678d8e04be6a991a86c2d5))
* **ci:** try h in gitlab pipeline ([8304da1](https://gitlab.com/simonoscr/nixfiles/commit/8304da1bd67d2a7f44ad135ea8c7ea7e0597fea0))

## [2.7.1](https://gitlab.com/simonoscr/nixfiles/compare/2.7.0...2.7.1) (2024-05-06)


### 📦 Other

* **firefox:** add cachix to bookmarks ([5cb5915](https://gitlab.com/simonoscr/nixfiles/commit/5cb591578340e2c30e27ec791ea2cf5f063d9170))
* **flock:** update flake.lock ([0e83fb1](https://gitlab.com/simonoscr/nixfiles/commit/0e83fb12af2f87e35f1babc83c34061539734479))


### 🛠 Fixes

* **flake:** hyprland flake input; ags follow nixpkgs, remove old inputs ([ab3725c](https://gitlab.com/simonoscr/nixfiles/commit/ab3725cf160e9a232eecc7dfc32dc1f80a63a1e7))
* **hm:** outsource home-manager settings to own .nix file instead of flake ([0d359c4](https://gitlab.com/simonoscr/nixfiles/commit/0d359c4c57b89f48396a7c241e0d2dac1c474505))
* **hyprland:** add systemd variables and extraCommands ([a0ea2d4](https://gitlab.com/simonoscr/nixfiles/commit/a0ea2d45603ce3ef72cbc75a99265ff299824945))
* **hyprland:** install hyprland for system instead only with hm; should be better and should added some dependencies; update imports on configuration ([920ebe6](https://gitlab.com/simonoscr/nixfiles/commit/920ebe6610fbbff0048a501eb3c8a7c33922033d))

## [2.7.0](https://gitlab.com/simonoscr/nixfiles/compare/2.6.2...2.7.0) (2024-05-05)


### 📦 Other

* **flake.lock:** update flock ([3b8260a](https://gitlab.com/simonoscr/nixfiles/commit/3b8260af473df6f494e7d8cf9e41165118ee6662))


### 🦊 CI/CD

* remove 'only' rules for all jobs and stages ([7cd835f](https://gitlab.com/simonoscr/nixfiles/commit/7cd835f0d56c280ae32e2ed6d71c730155710528))


### 🚀 Features

* add and use own cachix cache ([0c68474](https://gitlab.com/simonoscr/nixfiles/commit/0c684740988e637ef7ad40549afda2dd47095194))

## [2.6.2](https://gitlab.com/simonoscr/nixfiles/compare/2.6.1...2.6.2) (2024-05-05)


### :scissors: Refactor

* avoid repeated keys in attribute sets ([70c8d7f](https://gitlab.com/simonoscr/nixfiles/commit/70c8d7f0c60b692a0a5fcf2b9d179168b490e1bd))


### 📦 Other

* update flock ([9008b0e](https://gitlab.com/simonoscr/nixfiles/commit/9008b0e61666cda780a1ef522ba641914f6df7f9))


### 🛠 Fixes

* **ags:** update ags version to 1.8.2 and disable matugen ([54147fd](https://gitlab.com/simonoscr/nixfiles/commit/54147fdb4b51ba0803989f08a46ee4a6875dd116))

## [2.6.1](https://gitlab.com/simonoscr/nixfiles/compare/2.6.0...2.6.1) (2024-05-04)


### 📦 Other

* fix releaserc ([151fe05](https://gitlab.com/simonoscr/nixfiles/commit/151fe059eca1e7a043f7d7754ddf615c7c44a1b5))
* make refactor commit also da patch release ([6befabf](https://gitlab.com/simonoscr/nixfiles/commit/6befabfc2f64a9ec367638032c705a87cf2cb3db))
* update flock ([540a913](https://gitlab.com/simonoscr/nixfiles/commit/540a913795ba2e041e3fe0bd7b1d72515b68ae41))


### 🦊 CI/CD

* added cache for npm ([59552fc](https://gitlab.com/simonoscr/nixfiles/commit/59552fc1b175ead5df6c9a78e58a17d53c25ab89))
* fixonventional-changelog-conventionalcommits @ v7.0.2 ([ab608ae](https://gitlab.com/simonoscr/nixfiles/commit/ab608aebee52f9ed1668660ca56fbb2460102099))
* remove npm cache from release job ([0004e4f](https://gitlab.com/simonoscr/nixfiles/commit/0004e4f0236ebdfcdbeade80d1fe28705a3b78f3))


### 🛠 Fixes

* disable hyprland debug log but keep option in hyprland config ([7ececd6](https://gitlab.com/simonoscr/nixfiles/commit/7ececd6458bc49bdf971bb9eea5390af4ea6b285))
* hyprland config option to disable or enable debug logs ([6016a08](https://gitlab.com/simonoscr/nixfiles/commit/6016a0895ae0b10745ff2f8592d9a2d936a9174b))

## [2.6.0](https://gitlab.com/simonoscr/nixfiles/compare/v2.5.2...2.6.0) (2024-05-03)


### 📦 Other

* **ci:** update gitlab-ci ([ff0eece](https://gitlab.com/simonoscr/nixfiles/commit/ff0eece96e98e7e47fb08f7cd2ceba2ed53e1181))
* **ci:** update job dependencies ([c0f363e](https://gitlab.com/simonoscr/nixfiles/commit/c0f363eb052d9775c3af4c80327e47033c5a63f9))
* **flake:** update flake.lock ([d26fc6d](https://gitlab.com/simonoscr/nixfiles/commit/d26fc6d830d96e2168abdd889944de315a1308fb))
* **semver:** update releaserc config commit message with scope ([684ead5](https://gitlab.com/simonoscr/nixfiles/commit/684ead5b86db48508223a3e0abbc7bef49fa1747))


### 🦊 CI/CD

* add assets to release ([f507ac3](https://gitlab.com/simonoscr/nixfiles/commit/f507ac329ef5e3e06c3b01a09792777ab23cd0ce))
* fix job typo ([858b8e8](https://gitlab.com/simonoscr/nixfiles/commit/858b8e8e6aa5f2e9dc2161b0a1384d74e4f4ec28))
* test ([319c120](https://gitlab.com/simonoscr/nixfiles/commit/319c12079d3ba3a54c8be213d6e90e54c65238ce))
* update stages ([3eb3912](https://gitlab.com/simonoscr/nixfiles/commit/3eb391224578d18ba1818b53a90ffa58509e6392))


### 🧪 Tests

* remove a nix build install-iso step for testing job ([41265fd](https://gitlab.com/simonoscr/nixfiles/commit/41265fd7f05dc825018c2b2630a4949b1c8e4b58))
* semantic release ([fe59780](https://gitlab.com/simonoscr/nixfiles/commit/fe59780338af3f7afda92cfa05fa7bf5562a8b0c))


### 🚀 Features

* update with iso flake; pipeline update refined semantic-release with gitlab release ([22f88a2](https://gitlab.com/simonoscr/nixfiles/commit/22f88a20e8a5eaed7b9a7df9c6638b82f5134a26))

# Changelog

All notable changes to this project will be documented in this file. See
[Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [2.5.2](https://gitlab.com/simonoscr/nixfiles/compare/v2.5.1...v2.5.2) (2024-05-01)


### Bug Fixes

* update git for sign commits ([49cc2d7](https://gitlab.com/simonoscr/nixfiles/commit/49cc2d7809625e5bbac2272f8c80d8a8ede2ca50))

## [2.5.1](https://gitlab.com/simonoscr/nixfiles/compare/v2.5.0...v2.5.1) (2024-05-01)


### Bug Fixes

* use virtualisation.nix instead of own podman.nix file ([8a96fd3](https://gitlab.com/simonoscr/nixfiles/commit/8a96fd3f7739e32f583c505b953bee5c62fb7fe1))

## [2.5.0](https://gitlab.com/simonoscr/nixfiles/compare/v2.4.8...v2.5.0) (2024-05-01)


### Features

* **ci:** update pipeline to contains cleanup, lint and tests, add podman ([aff8f58](https://gitlab.com/simonoscr/nixfiles/commit/aff8f582cc42caa5d708e0434c638c94588a0846))

## [2.4.8](https://gitlab.com/simonoscr/nixfiles/compare/v2.4.7...v2.4.8) (2024-05-01)


### Bug Fixes

* update flock and lint ([755a334](https://gitlab.com/simonoscr/nixfiles/commit/755a334aa4f5b98f9ee6ba48f2080056cb1c2453))

## [2.4.7](https://gitlab.com/simonoscr/nixfiles/compare/v2.4.6...v2.4.7) (2024-05-01)


### Bug Fixes

* remove dead code with deadnix ([d640b78](https://gitlab.com/simonoscr/nixfiles/commit/d640b78b4c43c52b758d409fb2fd4ae1263ad2d2))

## [2.4.6](https://gitlab.com/simonoscr/nixfiles/compare/v2.4.5...v2.4.6) (2024-05-01)


### Bug Fixes

* add hy3 hyprland plugin but not activate it and update ([223369d](https://gitlab.com/simonoscr/nixfiles/commit/223369dfbd283571122d78fe7368ddc72bd1925e))

## [2.4.5](https://gitlab.com/simonoscr/nixfiles/compare/v2.4.4...v2.4.5) (2024-04-30)


### Bug Fixes

* add zed hyprland shortcut ([c3ad0d2](https://gitlab.com/simonoscr/nixfiles/commit/c3ad0d229207e711ff7903d590db456c3912a025))

## [2.4.4](https://gitlab.com/simonoscr/nixfiles/compare/v2.4.3...v2.4.4) (2024-04-30)


### Bug Fixes

* add zed editor ([14c3927](https://gitlab.com/simonoscr/nixfiles/commit/14c3927508932fc47676abdba7a8520af9755042))

## [2.4.3](https://gitlab.com/simonoscr/nixfiles/compare/v2.4.2...v2.4.3) (2024-04-30)


### Bug Fixes

* add trusted- and allowed-users; remove chaotix and disable flake input; general update flock ([0e45260](https://gitlab.com/simonoscr/nixfiles/commit/0e452606d2a76ffed7cbb7330ddfac33ae1b600b))

## [2.4.2](https://gitlab.com/simonoscr/nixfiles/compare/v2.4.1...v2.4.2) (2024-04-30)


### Bug Fixes

* cleanup dead code with deadnix ([8856c79](https://gitlab.com/simonoscr/nixfiles/commit/8856c79a85d3e9b359463582e452bc4c23cb88b6))

## [2.4.1](https://gitlab.com/simonoscr/nixfiles/compare/v2.4.0...v2.4.1) (2024-04-29)


### Bug Fixes

* update firefox theme ([a4d12bc](https://gitlab.com/simonoscr/nixfiles/commit/a4d12bc6116480f82bb5bd759fb50d921ad3c6d9))
* update tabbar height ([41e0bbd](https://gitlab.com/simonoscr/nixfiles/commit/41e0bbd3be7f5bc2e36eb0ec700be78a2cf4d9fc))

## [2.4.0](https://gitlab.com/simonoscr/nixfiles/compare/v2.3.4...v2.4.0) (2024-04-29)


### Features

* add nh, update README; update docs ([57402f5](https://gitlab.com/simonoscr/nixfiles/commit/57402f5337ee3e18e0e57498965c873bd02fee02))

## [2.3.4](https://gitlab.com/simonoscr/nixfiles/compare/v2.3.3...v2.3.4) (2024-04-29)


### Bug Fixes

* update flock; hyprland sens and move libinput from services.xserver to services ([65c1ba7](https://gitlab.com/simonoscr/nixfiles/commit/65c1ba7e0703d264d3d95c9c30a48b6d6407e9a8))

## [2.3.3](https://gitlab.com/simonoscr/nixfiles/compare/v2.3.2...v2.3.3) (2024-04-28)


### Bug Fixes

* update hyprland config sens ([ead5ac7](https://gitlab.com/simonoscr/nixfiles/commit/ead5ac7ff415a60112e99f2dbd24673aa35778e4))

## [2.3.2](https://gitlab.com/simonoscr/nixfiles/compare/v2.3.1...v2.3.2) (2024-04-28)


### Bug Fixes

* update lock and firefox settings to open bookmarks in new tab ([e603407](https://gitlab.com/simonoscr/nixfiles/commit/e6034075c2a51f94b3a2015a2fc1b159fc27ff70))

## [2.3.1](https://gitlab.com/simonoscr/nixfiles/compare/v2.3.0...v2.3.1) (2024-04-27)


### Bug Fixes

* add globalshortcuts to hyprland for ts3; add media key shortcuts; update mangohud config ([8204343](https://gitlab.com/simonoscr/nixfiles/commit/820434393c45f1dad64ad3853061ba1c167d6e07))

## [2.3.0](https://gitlab.com/simonoscr/nixfiles/compare/v2.2.0...v2.3.0) (2024-04-24)


### Features

* big update nixos, hyprland ([5e35450](https://gitlab.com/simonoscr/nixfiles/commit/5e3545087c123a8c3650e15742e47af43c99c9cb))

## [2.2.0](https://gitlab.com/simonoscr/nixfiles/compare/v2.1.28...v2.2.0) (2024-04-07)


### Features

* docs; cleanup and pump minor after ref ([8e2e547](https://gitlab.com/simonoscr/nixfiles/commit/8e2e547306aa74aef2136bbe20987575afed47c5))

## [2.1.28](https://gitlab.com/simonoscr/nixfiles/compare/v2.1.27...v2.1.28) (2024-04-06)


### Bug Fixes

* cleanup; remove unused and doubled configs, options, servvices etc ([b02fc0f](https://gitlab.com/simonoscr/nixfiles/commit/b02fc0faf1a6a04ab6656b4a9bfc53b42f325268))

## [2.1.27](https://gitlab.com/simonoscr/nixfiles/compare/v2.1.26...v2.1.27) (2024-04-06)


### Bug Fixes

* add protonmail-desktop beta app ([37e275c](https://gitlab.com/simonoscr/nixfiles/commit/37e275c79077a488ddfce4c8f79ab8f63ff44dfb))

## [2.1.26](https://gitlab.com/simonoscr/nixfiles/compare/v2.1.25...v2.1.26) (2024-04-06)


### Bug Fixes

* add matugen and update flake input ([608fb7c](https://gitlab.com/simonoscr/nixfiles/commit/608fb7ccb55c6a459aaeff1457b3569f6ff93335))

## [2.1.25](https://gitlab.com/simonoscr/nixfiles/compare/v2.1.24...v2.1.25) (2024-04-01)


### Bug Fixes

* reduce audio latency ([a65b0ea](https://gitlab.com/simonoscr/nixfiles/commit/a65b0ea910ad0b5c490efc60a0d229ecba7fde3a))

## [2.1.24](https://gitlab.com/simonoscr/nixfiles/compare/v2.1.23...v2.1.24) (2024-03-31)


### Bug Fixes

* firefox icons ([db79fca](https://gitlab.com/simonoscr/nixfiles/commit/db79fcad98d7ffe0ac0199ee5e07970268942f40))

## [2.1.23](https://gitlab.com/simonoscr/nixfiles/compare/v2.1.22...v2.1.23) (2024-03-31)


### Bug Fixes

* firefox custom icons ([a882322](https://gitlab.com/simonoscr/nixfiles/commit/a882322530322ba9a0e8a245efc4f06c939e3591))

## [2.1.22](https://gitlab.com/simonoscr/nixfiles/compare/v2.1.21...v2.1.22) (2024-03-31)


### Bug Fixes

* firefox autohide tabs ([dbdf2d0](https://gitlab.com/simonoscr/nixfiles/commit/dbdf2d0d2d93f63ae739145c9d60fcb7a7446b9e))

## [2.1.21](https://gitlab.com/simonoscr/nixfiles/compare/v2.1.20...v2.1.21) (2024-03-31)


### Bug Fixes

* update firefox theme ([ba054da](https://gitlab.com/simonoscr/nixfiles/commit/ba054da95a1366130cbe1fc94622637de4d6c2d9))

## [2.1.20](https://gitlab.com/simonoscr/nixfiles/compare/v2.1.19...v2.1.20) (2024-03-31)


### Bug Fixes

* add firefox custom css theme ([99fe78e](https://gitlab.com/simonoscr/nixfiles/commit/99fe78e9bb44115ef43e861b70867e6e4590ece0))

## [2.1.19](https://gitlab.com/simonoscr/nixfiles/compare/v2.1.18...v2.1.19) (2024-03-30)


### Bug Fixes

* remove custom bezier from hyprland use default ([25120c7](https://gitlab.com/simonoscr/nixfiles/commit/25120c7a500a3d5400f78bac38f09a0cc35ec2d1))

## [2.1.18](https://gitlab.com/simonoscr/nixfiles/compare/v2.1.17...v2.1.18) (2024-03-30)


### Bug Fixes

* some hyprland config settings ([9e218e6](https://gitlab.com/simonoscr/nixfiles/commit/9e218e6f181e8e273e30e03c9d7aa755616fca1f))

## [2.1.17](https://gitlab.com/simonoscr/nixfiles/compare/v2.1.16...v2.1.17) (2024-03-30)


### Bug Fixes

* hyprland mouse focus behaviour ([784747b](https://gitlab.com/simonoscr/nixfiles/commit/784747b5c43af8acf39b3cccc417090787975e11))

## [2.1.16](https://gitlab.com/simonoscr/nixfiles/compare/v2.1.15...v2.1.16) (2024-03-30)


### Bug Fixes

* disable bugged hyprland plugin - no auto updating headers when updating hyprland; neovim optionchange from options to opts; copyKernels; remove kvm-amd virtualisation from kernelModules -> not used; enableAllFirmware; update flock ([9fe5986](https://gitlab.com/simonoscr/nixfiles/commit/9fe5986278dbf9a9a886ca4617bda584725754e9))

## [2.1.15](https://gitlab.com/simonoscr/nixfiles/compare/v2.1.14...v2.1.15) (2024-03-29)


### Bug Fixes

* add user to corectrl group for not entering password on startup ([3dc31f3](https://gitlab.com/simonoscr/nixfiles/commit/3dc31f3abad12b779994c66444b52c8a298c04e4))

## [2.1.14](https://gitlab.com/simonoscr/nixfiles/compare/v2.1.13...v2.1.14) (2024-03-29)


### Bug Fixes

* enable hyprland plugins; mangohud config ([379f8bb](https://gitlab.com/simonoscr/nixfiles/commit/379f8bbf7ef6d76ed1b3e956d4863ca2636a71e0))

## [2.1.13](https://gitlab.com/simonoscr/nixfiles/compare/v2.1.12...v2.1.13) (2024-03-29)


### Bug Fixes

* update mouse and focus behavior for hyprland ([b6b3883](https://gitlab.com/simonoscr/nixfiles/commit/b6b3883f7724059598bc156ebbf0cef27aaabdf7))

## [2.1.12](https://gitlab.com/simonoscr/nixfiles/compare/v2.1.11...v2.1.12) (2024-03-29)


### Bug Fixes

* steam with proton-ge-bin extra package; update fonts; update flock ([761efcf](https://gitlab.com/simonoscr/nixfiles/commit/761efcf741fd4fa100536c10dfd6b64f64a4c9eb))

## [2.1.11](https://gitlab.com/simonoscr/nixfiles/compare/v2.1.10...v2.1.11) (2024-03-28)


### Bug Fixes

* update sysctl configuration ([284a076](https://gitlab.com/simonoscr/nixfiles/commit/284a07638df430273d53691fa08aed6f4e1f48fe))

## [2.1.10](https://gitlab.com/simonoscr/nixfiles/compare/v2.1.9...v2.1.10) (2024-03-28)


### Bug Fixes

* add mangohud.nix and configuration; update flock; remove unused pkgs ([685d76f](https://gitlab.com/simonoscr/nixfiles/commit/685d76fb0e5ac40e2e33835588c8081a399547f0))

## [2.1.9](https://gitlab.com/simonoscr/nixfiles/compare/v2.1.8...v2.1.9) (2024-03-24)


### Bug Fixes

* update flock; remove unused inputs; disable watchdog; set RADV variables for gaming performance ([910caaf](https://gitlab.com/simonoscr/nixfiles/commit/910caafd79e6d7bea193de619bfb780bbf741e5c))

## [2.1.8](https://gitlab.com/simonoscr/nixfiles/compare/v2.1.7...v2.1.8) (2024-03-23)


### Bug Fixes

* ags applauncher to launcher; wine waylandFull; cachix for nix-gaming; star-citizen from nix-gaming ([a8a5b91](https://gitlab.com/simonoscr/nixfiles/commit/a8a5b916a49cf627801fbfa64828444c1bfbfb94))

## [2.1.7](https://gitlab.com/simonoscr/nixfiles/compare/v2.1.6...v2.1.7) (2024-03-22)


### Bug Fixes

* update ags ([9075e29](https://gitlab.com/simonoscr/nixfiles/commit/9075e294867281155cc2dc470e1cc365e4e8dcf8))

## [2.1.6](https://gitlab.com/simonoscr/nixfiles/compare/v2.1.5...v2.1.6) (2024-03-22)


### Bug Fixes

* update sysctl congestion_control to BRR ([1e9d2ac](https://gitlab.com/simonoscr/nixfiles/commit/1e9d2ac427f9bb71836fdfa0433a82707d5fc587))

## [2.1.5](https://gitlab.com/simonoscr/nixfiles/compare/v2.1.4...v2.1.5) (2024-03-22)


### Bug Fixes

* ags preventing tearing on fullscreen application so idsable when switching to "gaming"-resolution; update flock ([9268ba7](https://gitlab.com/simonoscr/nixfiles/commit/9268ba700596a3122ade8c89ba37ef2765017286))

## [2.1.4](https://gitlab.com/simonoscr/nixfiles/compare/v2.1.3...v2.1.4) (2024-03-21)


### Bug Fixes

* make tearing under hyprland work (ags bar was the problem) ([9a440a7](https://gitlab.com/simonoscr/nixfiles/commit/9a440a74a870fbddb3e29c18405184be04d6aff4))


### Reverts

* switch to xanmod 6.7.9 instead of latest 6.8.1; remove useless extraLibraries from steam; enable lowLatency pipewire; enable tearing in hyprland ([ca89e62](https://gitlab.com/simonoscr/nixfiles/commit/ca89e62eac893eb7e0f4ceb857aff2b430e92af4))

## [2.1.3](https://gitlab.com/simonoscr/nixfiles/compare/v2.1.2...v2.1.3) (2024-03-19)


### Bug Fixes

* update at switch linux kernel to latest 6.8.1 ([fa538cd](https://gitlab.com/simonoscr/nixfiles/commit/fa538cd7450e4a1737cfca07a7b1b0f159abf34f))

## [2.1.2](https://gitlab.com/simonoscr/nixfiles/compare/v2.1.1...v2.1.2) (2024-03-17)


### Bug Fixes

* update flock and small fixes to adapt to nixpkgs changes ([cb8cd4e](https://gitlab.com/simonoscr/nixfiles/commit/cb8cd4e623ceb74742350ce5c781c1b3002fee42))

## [2.1.1](https://gitlab.com/simonoscr/nixfiles/compare/v2.1.0...v2.1.1) (2024-03-15)


### Bug Fixes

* steam extraCompatPackages see https://github.com/fufexan/nix-gaming/issues/163 ([f721f2a](https://gitlab.com/simonoscr/nixfiles/commit/f721f2afa5d990ab43746d32ace97c1287eeee93))

## [2.1.0](https://gitlab.com/simonoscr/nixfiles/compare/v2.0.2...v2.1.0) (2024-03-05)


### Features

* update ags; restrcture for server voyager ([6654e8a](https://gitlab.com/simonoscr/nixfiles/commit/6654e8a58fd320b3ecf0456581e1a41676fa4e51))

## [2.0.2](https://gitlab.com/simonoscr/nixfiles/compare/v2.0.1...v2.0.2) (2024-03-02)


### Bug Fixes

* correct size for cursor ([c27d530](https://gitlab.com/simonoscr/nixfiles/commit/c27d530dcaa97767acfcf1e1252bf5009246dde0))

## [2.0.1](https://gitlab.com/simonoscr/nixfiles/compare/v2.0.0...v2.0.1) (2024-03-02)


### Bug Fixes

* **finally:** capitaine cursor; wrong size was set ([b8f6a1d](https://gitlab.com/simonoscr/nixfiles/commit/b8f6a1d2f8fce22658562c46dee7c333e9a051de))

## [2.0.0](https://gitlab.com/simonoscr/nixfiles/compare/v1.6.9...v2.0.0) (2024-03-02)


### ⚠ BREAKING CHANGES

* restructure repository for clear structure between home-manager(user scope) and nixos(system scope)

### Code Refactoring

* reorganize nixfiles to make more clear what is home-manager and what nix/nixos ([9d1e349](https://gitlab.com/simonoscr/nixfiles/commit/9d1e34986a1712bb0ea38a62a21a50f41264a7c9))

## [1.6.9](https://gitlab.com/simonoscr/nixfiles/compare/v1.6.8...v1.6.9) (2024-03-02)


### Bug Fixes

* hyprlock settings and shortcut, remove swaylock pam ([240f92a](https://gitlab.com/simonoscr/nixfiles/commit/240f92a6a9c97858bc98d9eb4cdaa8c8bfa56aef))

## [1.6.8](https://gitlab.com/simonoscr/nixfiles/compare/v1.6.7...v1.6.8) (2024-03-02)


### Bug Fixes

* switched from swaylock+swayidle to hyprlock+hypridle ([555097a](https://gitlab.com/simonoscr/nixfiles/commit/555097a28113476d0ebccaf1ab621e22385e16cc))

## [1.6.7](https://gitlab.com/simonoscr/nixfiles/compare/v1.6.6...v1.6.7) (2024-03-02)


### Bug Fixes

* example of adding knownHosts (but not for user) ([e5dbc4a](https://gitlab.com/simonoscr/nixfiles/commit/e5dbc4ad221466d5b06389eb1275193949d2cc34))

## [1.6.6](https://gitlab.com/simonoscr/nixfiles/compare/v1.6.5...v1.6.6) (2024-03-02)


### Bug Fixes

* change path to secrets ([131276e](https://gitlab.com/simonoscr/nixfiles/commit/131276e6fe5109d062f0cbcfc88e53df7adb5117))

## [1.6.5](https://gitlab.com/simonoscr/nixfiles/compare/v1.6.4...v1.6.5) (2024-03-02)


### Bug Fixes

* TERM user sessionsenv ([f1fe203](https://gitlab.com/simonoscr/nixfiles/commit/f1fe203209623e0e42f1d1dba6e0937e3c140bb6))

## [1.6.4](https://gitlab.com/simonoscr/nixfiles/compare/v1.6.3...v1.6.4) (2024-03-02)


### Bug Fixes

* update voyager and flock ([3f95ddb](https://gitlab.com/simonoscr/nixfiles/commit/3f95ddb755d558da56a812b300a0c79256a67d0e))

## [1.6.3](https://gitlab.com/simonoscr/nixfiles/compare/v1.6.2...v1.6.3) (2024-02-29)


### Bug Fixes

* add bookmark ([518df6d](https://gitlab.com/simonoscr/nixfiles/commit/518df6d6e56f9bb1cb03825d9c111f26e29c02b0))

## [1.6.2](https://gitlab.com/simonoscr/nixfiles/compare/v1.6.1...v1.6.2) (2024-02-29)


### Bug Fixes

* pipewire config anf update flock ([23d05bf](https://gitlab.com/simonoscr/nixfiles/commit/23d05bf679bd42577d41d2ccd2d23f0d22384b35))

## [1.6.1](https://gitlab.com/simonoscr/nixfiles/compare/v1.6.0...v1.6.1) (2024-02-28)


### Bug Fixes

* update and fix pipewire configuration. environment.etc not possible anymore. ([513f635](https://gitlab.com/simonoscr/nixfiles/commit/513f635d6cafdefc43627bbadb8d89875d9300b1))

## [1.6.0](https://gitlab.com/simonoscr/nixfiles/compare/v1.5.7...v1.6.0) (2024-02-25)


### Features

* update flock; big nixpkgs merge staging-next to nixos-unstable ([0bf7f1d](https://gitlab.com/simonoscr/nixfiles/commit/0bf7f1d88ab380db728afbcbcb0f07a3567a9220))

## [1.5.7](https://gitlab.com/simonoscr/nixfiles/compare/v1.5.6...v1.5.7) (2024-02-24)


### Bug Fixes

* update and switch kernel ([3e1bc71](https://gitlab.com/simonoscr/nixfiles/commit/3e1bc710d23cff724cff4a70c612396766571a61))
* update kernel again to xanmod 6.7.5; add kernel.sysctl parameter for low latency; and add inputs ([a84d019](https://gitlab.com/simonoscr/nixfiles/commit/a84d019d6f3a638c2541f6de5ee28a76a84b6772))

## [1.5.6](https://gitlab.com/simonoscr/nixfiles/compare/v1.5.5...v1.5.6) (2024-02-23)


### Bug Fixes

* open steam and webcord always on workspace 2 ([c05765c](https://gitlab.com/simonoscr/nixfiles/commit/c05765cff20209ff4513112d8aeb039745b152c1))

## [1.5.5](https://gitlab.com/simonoscr/nixfiles/compare/v1.5.4...v1.5.5) (2024-02-23)


### Bug Fixes

* disable waybar and update flock ([2eb6054](https://gitlab.com/simonoscr/nixfiles/commit/2eb605422feee92ea818f37c920501ef60939413))
* return back to original ags ([c0e8b53](https://gitlab.com/simonoscr/nixfiles/commit/c0e8b533d12672034f9b4c9938fe72eb97ff530a))

## [1.5.4](https://gitlab.com/simonoscr/nixfiles/compare/v1.5.3...v1.5.4) (2024-02-23)


### Bug Fixes

* switch to latest (6.7.5) xanmod kernel ([cf1b685](https://gitlab.com/simonoscr/nixfiles/commit/cf1b685ec7f110a7a2a3eec30acd9b35727470c5))

## [1.5.3](https://gitlab.com/simonoscr/nixfiles/compare/v1.5.2...v1.5.3) (2024-02-23)


### Bug Fixes

* add piper ([58df67d](https://gitlab.com/simonoscr/nixfiles/commit/58df67dd3b023200a8319acb0074d99fe73eca09))

## [1.5.2](https://gitlab.com/simonoscr/nixfiles/compare/v1.5.1...v1.5.2) (2024-02-22)


### Bug Fixes

* remove sops-nix from repo ([cf71e27](https://gitlab.com/simonoscr/nixfiles/commit/cf71e2746e563a11a31380e98767103378d53a75))

## [1.5.1](https://gitlab.com/simonoscr/nixfiles/compare/v1.5.0...v1.5.1) (2024-02-22)


### Bug Fixes

* disable firefox containers ([01d6ec1](https://gitlab.com/simonoscr/nixfiles/commit/01d6ec1e0f2b0c7db155abe8811df308ec20705d))

## [1.5.0](https://gitlab.com/simonoscr/nixfiles/compare/v1.4.11...v1.5.0) (2024-02-21)


### Features

* update flock; disable nixpkgs-wayland flake input; docs; i3 configuration; polybar ([72b20ec](https://gitlab.com/simonoscr/nixfiles/commit/72b20ec0369e9945af47b02a917840371bd09c60))

## [1.4.11](https://gitlab.com/simonoscr/nixfiles/compare/v1.4.10...v1.4.11) (2024-02-19)


### Bug Fixes

* idk ([6e1774b](https://gitlab.com/simonoscr/nixfiles/commit/6e1774b3a088efbba549025495cca4a6e593263a))

## [1.4.10](https://gitlab.com/simonoscr/nixfiles/compare/v1.4.9...v1.4.10) (2024-02-19)


### Bug Fixes

* switch to xanmod_latest kernel; add loginLimits; stylix settings; hyprland config ([9405e94](https://gitlab.com/simonoscr/nixfiles/commit/9405e94078f662d9b03edb69026626f86c304bdf))

## [1.4.9](https://gitlab.com/simonoscr/nixfiles/compare/v1.4.8...v1.4.9) (2024-02-17)


### Bug Fixes

* ref graphics to hardware.nix; add udiskie for auto mount sb sticks; set sysctl settings like increase zram swap; max_m ap_count and page-cluster ([251bf9e](https://gitlab.com/simonoscr/nixfiles/commit/251bf9e25e5821a4a9c8cc7a67d35c728898cdc5))

## [1.4.8](https://gitlab.com/simonoscr/nixfiles/compare/v1.4.7...v1.4.8) (2024-02-17)


### Bug Fixes

* add dunst.nix, adjust hyprland plugin settings; move home-packages to hyprland default.nix, update flock ([ea5542b](https://gitlab.com/simonoscr/nixfiles/commit/ea5542bd7dbd9bab16b4e8a6dc0b35e0fd2b28d6))

## [1.4.7](https://gitlab.com/simonoscr/nixfiles/compare/v1.4.6...v1.4.7) (2024-02-16)


### Bug Fixes

* keep website settings in firefox ([24cc88c](https://gitlab.com/simonoscr/nixfiles/commit/24cc88c08b836ec52bcda78e9207994ba3c63c26))

## [1.4.6](https://gitlab.com/simonoscr/nixfiles/compare/v1.4.5...v1.4.6) (2024-02-16)


### Bug Fixes

* gamemode CAPs; hyprland config window rules ([0398306](https://gitlab.com/simonoscr/nixfiles/commit/0398306bec3d002df254c16d5474cb65dab54952))

## [1.4.5](https://gitlab.com/simonoscr/nixfiles/compare/v1.4.4...v1.4.5) (2024-02-16)


### Bug Fixes

* switch to stylix for applications ([79632d0](https://gitlab.com/simonoscr/nixfiles/commit/79632d0a061d523848a5c2c4e8c34cf61f7fcd96))

## [1.4.4](https://gitlab.com/simonoscr/nixfiles/compare/v1.4.3...v1.4.4) (2024-02-16)


### Bug Fixes

* add AMDVLK pkg but default to RADV; add opencl pkgs ([b306f9b](https://gitlab.com/simonoscr/nixfiles/commit/b306f9b688c9fcc9169b0d701c85b3d590cfa5ac))

## [1.4.3](https://gitlab.com/simonoscr/nixfiles/compare/v1.4.2...v1.4.3) (2024-02-16)


### Bug Fixes

* add user to gamemode group; activate hyprland plugin to fix cs2 4:3 streched with SDL_VIDEO_DRIVER=x11 (wayland not working with this fix); fix hyprland env; move hyprland dependend packages to hyprland; ([181033e](https://gitlab.com/simonoscr/nixfiles/commit/181033ef3991f00418d0d4e5cd4484f1c1a08c1e))

## [1.4.2](https://gitlab.com/simonoscr/nixfiles/compare/v1.4.1...v1.4.2) (2024-02-16)


### Bug Fixes

* update firefox bookmarks and settings ([d04a484](https://gitlab.com/simonoscr/nixfiles/commit/d04a4849eac3a538b761e13942b52ec59ea8c38f))

## [1.4.1](https://gitlab.com/simonoscr/nixfiles/compare/v1.4.0...v1.4.1) (2024-02-15)


### Bug Fixes

* switch firefox pkgs to be able to import bookmarks file; hyprland fixes ([75e3206](https://gitlab.com/simonoscr/nixfiles/commit/75e32060323a520947b92bb515aa6e81764ca983))

## [1.4.0](https://gitlab.com/simonoscr/nixfiles/compare/v1.3.3...v1.4.0) (2024-02-14)


### Features

* add yazi, bat, direnv; added features to security, audio, and shell ([afd848c](https://gitlab.com/simonoscr/nixfiles/commit/afd848c57a9b0ce3fa156d1309d4b3bf54159968))

## [1.3.3](https://gitlab.com/simonoscr/nixfiles/compare/v1.3.2...v1.3.3) (2024-02-13)


### Bug Fixes

* wl env that breaks vscodium ([57ba1d2](https://gitlab.com/simonoscr/nixfiles/commit/57ba1d213e72ac75feaf29847df70c2a7e349348))

## [1.3.2](https://gitlab.com/simonoscr/nixfiles/compare/v1.3.1...v1.3.2) (2024-02-12)


### Bug Fixes

* small fixes in audio like removed unused pkgs; ([421c503](https://gitlab.com/simonoscr/nixfiles/commit/421c503ecf8b93b56ef73cc1df9d912971ad4c56))

## [1.3.1](https://gitlab.com/simonoscr/nixfiles/compare/v1.3.0...v1.3.1) (2024-02-11)


### Bug Fixes

* small fixes to gaming related things; remove mimeApps; remove unused inputs ([27cfb73](https://gitlab.com/simonoscr/nixfiles/commit/27cfb735fa236f95c29aec6a71db17caa1c13525))

## [1.3.0](https://gitlab.com/simonoscr/nixfiles/compare/v1.2.1...v1.3.0) (2024-02-11)


### Features

* switch kernel to linux_latest (6.7); use mesa-git (24.0); remove unused programs; cleanup code; add corectrl as program, activate OC for AMD GPU ([e7975cf](https://gitlab.com/simonoscr/nixfiles/commit/e7975cfba16a9fb2b40ee7528d2d198e71c87b00))

## [1.2.1](https://gitlab.com/simonoscr/nixfiles/compare/v1.2.0...v1.2.1) (2024-02-10)


### Bug Fixes

* restructure sops-nix ([7c3a146](https://gitlab.com/simonoscr/nixfiles/commit/7c3a14699a11c98fe6a5ceac726964b3affc599a))

## [1.2.0](https://gitlab.com/simonoscr/nixfiles/compare/v1.1.0...v1.2.0) (2024-02-09)


### Features

* **cosmos:** switch to home-manager module from standalone for cosmos host and fix some configuration problems ([0024eb1](https://gitlab.com/simonoscr/nixfiles/commit/0024eb1602fa3f763106a4d38e50e957ef8e5506))

## [1.1.0](https://gitlab.com/simonoscr/nixfiles/compare/v1.0.2...v1.1.0) (2024-02-09)


### Features

* add home-manager as a module to cosmos and remove standalone-hm config ([2a61f8f](https://gitlab.com/simonoscr/nixfiles/commit/2a61f8f221d05e8de217f69bc60f8b95f7579691))

## [1.0.2](https://gitlab.com/simonoscr/nixfiles/compare/v1.0.1...v1.0.2) (2024-02-09)


### Bug Fixes

* simples things in hyprland; manage game related dependencies and doc ([6f978b6](https://gitlab.com/simonoscr/nixfiles/commit/6f978b65764d57726c6e264af330d479d33e726b))

## [1.0.1](https://gitlab.com/simonoscr/nixfiles/compare/v1.0.0...v1.0.1) (2024-02-09)


### Bug Fixes

* hardware configurations and gaming related updates and tests ([bfbfe11](https://gitlab.com/simonoscr/nixfiles/commit/bfbfe115ffde43913c53a096c9af9e5c8ebcd6c9))
* nixvim structure update ([89b1350](https://gitlab.com/simonoscr/nixfiles/commit/89b135019e94688ca1bbf1c66f25efaa9d27a794))

## 1.0.0 (2024-02-08)


### Bug Fixes

* moce gamescope to home-manager package ([be954c3](https://gitlab.com/simonoscr/nixfiles/commit/be954c3bc656b26615a4751fb09e6fcaf22ad9d8))
* README center nixos ([8277bb6](https://gitlab.com/simonoscr/nixfiles/commit/8277bb64371e75c1f3a812ffbcf66d6bd26c9c69))
* remove docker pull from image reference in semver stage ([4619a2a](https://gitlab.com/simonoscr/nixfiles/commit/4619a2add8675315ed064fcb7e5fe4ee06f365c2))


############################################
########### MOVED TO GITLAB ################
############################################

## [1.8.12](https://github.com/simonoscr/dotfiles/compare/1.8.11...1.8.12) (2024-02-07)


### Bug Fixes

* theming and update flock ([d19eb3a](https://github.com/simonoscr/dotfiles/commit/d19eb3a3276d040bcd7aa61569d932aaaade39c2))



## [1.8.11](https://github.com/simonoscr/dotfiles/compare/1.8.10...1.8.11) (2024-02-06)


### Bug Fixes

* disable mesa-git ([a03a9a3](https://github.com/simonoscr/dotfiles/commit/a03a9a31236d962ea90cdcccc48fea77b74d3359))



## [1.8.10](https://github.com/simonoscr/dotfiles/compare/1.8.9...1.8.10) (2024-02-06)


### Bug Fixes

* update flock; add chaotic nyx and activate mesa-git ([27e5b3c](https://github.com/simonoscr/dotfiles/commit/27e5b3c50f3dd4664600683db80b732e7264e3d8))



## [1.8.9](https://github.com/simonoscr/dotfiles/compare/1.8.8...1.8.9) (2024-02-04)


### Bug Fixes

* disable SDL_VIDEO_DRIVER=wayland ([81d3440](https://github.com/simonoscr/dotfiles/commit/81d3440f15a31c3a253f0ed85696ef67ffa4ef2b))



## [1.8.8](https://github.com/simonoscr/dotfiles/compare/1.8.7...1.8.8) (2024-02-04)


### Bug Fixes

* update dashy and hyprland config (enable SDL_VIDE_DRIVER ([181af60](https://github.com/simonoscr/dotfiles/commit/181af600b37de4d0438325d6b416d2c6ce109f27))
* volume mount for puhole ([ad375f1](https://github.com/simonoscr/dotfiles/commit/ad375f17a8a97ea9ed453c8d3c65a0135f285e96))



## [1.8.7](https://github.com/simonoscr/dotfiles/compare/1.8.6...1.8.7) (2024-02-04)


### Bug Fixes

* add pihole ([8dea741](https://github.com/simonoscr/dotfiles/commit/8dea741710086e1aa2adbddb1c871562bfd52b5c))



## [1.8.6](https://github.com/simonoscr/dotfiles/compare/1.8.5...1.8.6) (2024-02-04)


### Bug Fixes

* cockpit and dashy ([d28bec1](https://github.com/simonoscr/dotfiles/commit/d28bec117364a76477c9099107b097f4a6dd7a88))
* update dashy, add cockpit and small fixes and updates ([e373d00](https://github.com/simonoscr/dotfiles/commit/e373d00bfeb6b6122742d9f6ec0a3934f6c770ad))



## [1.8.5](https://github.com/simonoscr/dotfiles/compare/1.8.4...1.8.5) (2024-02-04)


### Bug Fixes

* update waybar ([3ff88e7](https://github.com/simonoscr/dotfiles/commit/3ff88e71fbeea0b2c848feaa5d36badab27409af))



## [1.8.4](https://github.com/simonoscr/dotfiles/compare/1.8.3...1.8.4) (2024-02-04)


### Bug Fixes

* customize waybar, scripts and modules and nerd fonts ([894e26a](https://github.com/simonoscr/dotfiles/commit/894e26a765b2c0a79f4f860daa2edbdfc5958aa7))



## [1.8.3](https://github.com/simonoscr/dotfiles/compare/1.8.2...1.8.3) (2024-02-03)


### Bug Fixes

* add i3 shortcuts ([cb0a23c](https://github.com/simonoscr/dotfiles/commit/cb0a23c60ab3692a394a93d14e88b4b35d778a23))
* fix shortcuts and modifier ([5c6c369](https://github.com/simonoscr/dotfiles/commit/5c6c36942ff3c07ad538b4cde7bf650fa7387b72))



## [1.8.2](https://github.com/simonoscr/dotfiles/compare/1.8.1...1.8.2) (2024-02-03)


### Bug Fixes

* add i3, polybar, hyprland fixes, waybar, fixes ([3d68d38](https://github.com/simonoscr/dotfiles/commit/3d68d38744135c42c4dba37bd5ac46ac19d36030))



## [1.8.1](https://github.com/simonoscr/dotfiles/compare/1.8.0...1.8.1) (2024-02-03)


### Bug Fixes

* hyprland floating window rule ([d667238](https://github.com/simonoscr/dotfiles/commit/d66723804c1a3b36f596fe4025ca86153feb3ea4))



# [1.8.0](https://github.com/simonoscr/dotfiles/compare/1.7.10...1.8.0) (2024-02-03)


### Features

* use waybar instead of ags (broken), and add: wofi, rofi, wlogout ([8cfde20](https://github.com/simonoscr/dotfiles/commit/8cfde2068df924e55266f87af4c40187729c140a))



## [1.7.10](https://github.com/simonoscr/dotfiles/compare/1.7.9...1.7.10) (2024-02-03)


### Bug Fixes

* loki, dashy, packages, etc ([7a2f5df](https://github.com/simonoscr/dotfiles/commit/7a2f5df5d83932caaed93cc1472332c248bc329b))



## [1.7.9](https://github.com/simonoscr/dotfiles/compare/1.7.8...1.7.9) (2024-02-02)


### Bug Fixes

* add extension ([8e52dd6](https://github.com/simonoscr/dotfiles/commit/8e52dd6864598ec45837717372f0a3556b6bc41e))



## [1.7.8](https://github.com/simonoscr/dotfiles/compare/1.7.7...1.7.8) (2024-02-02)


### Bug Fixes

* remove nslookup and add dig ([693de8a](https://github.com/simonoscr/dotfiles/commit/693de8ab326e74e39d31083183eb6d3c36e9c3a4))



## [1.7.7](https://github.com/simonoscr/dotfiles/compare/1.7.6...1.7.7) (2024-02-02)


### Bug Fixes

* slim home-manager for server ([1bdbfe2](https://github.com/simonoscr/dotfiles/commit/1bdbfe2e29a790514f84e1c97b63b3244c662ac4))



## [1.7.6](https://github.com/simonoscr/dotfiles/compare/1.7.5...1.7.6) (2024-02-02)


### Bug Fixes

* vscodium extensions, firefox bookmarks ([4bc97dd](https://github.com/simonoscr/dotfiles/commit/4bc97dd13bc94adc6c613873acfea3843eb510f8))



## [1.7.5](https://github.com/simonoscr/dotfiles/compare/1.7.4...1.7.5) (2024-02-02)


### Bug Fixes

* add autoUpgrade to voyager ([a304b7c](https://github.com/simonoscr/dotfiles/commit/a304b7c512c74f902f6e76f88bc12d8578296122))



## [1.7.4](https://github.com/simonoscr/dotfiles/compare/1.7.3...1.7.4) (2024-02-02)


### Bug Fixes

* update firefox bookmarks and locale ([94373d7](https://github.com/simonoscr/dotfiles/commit/94373d73c64f87d99f1ec58a1df68c7b2280658e))



## [1.7.3](https://github.com/simonoscr/dotfiles/compare/1.7.2...1.7.3) (2024-02-02)


### Bug Fixes

* unbound logs and metrics and grafana dashboard ([90cc147](https://github.com/simonoscr/dotfiles/commit/90cc147aa62199a3e87861f0e08102de4d7efcb2))



## [1.7.2](https://github.com/simonoscr/dotfiles/compare/1.7.1...1.7.2) (2024-02-02)


### Bug Fixes

* add kanidm ([3c2d282](https://github.com/simonoscr/dotfiles/commit/3c2d282a12518ab925e78fd6c562dd348e303f43))
* add pam auth to applications ([0da792b](https://github.com/simonoscr/dotfiles/commit/0da792bf32b634ebf2f821ad17f94c8cbd93af4a))



## [1.7.1](https://github.com/simonoscr/dotfiles/compare/1.7.0...1.7.1) (2024-02-02)


### Bug Fixes

* add logs to unbound ([59044eb](https://github.com/simonoscr/dotfiles/commit/59044eb8008227e0bf15f5a872489eb9807c2fac))
* fix unbound exporter ([e786e95](https://github.com/simonoscr/dotfiles/commit/e786e95caa44ccd10d76ac703c063f92fd751839))



# [1.7.0](https://github.com/simonoscr/dotfiles/compare/1.6.12...1.7.0) (2024-02-02)


### Features

* add unbound as local DNS, use local DNS for monitoring stack, add grafana dashboards ([2587d4c](https://github.com/simonoscr/dotfiles/commit/2587d4c710e2f47ef9723f48cb28c11893f71bbf))



## [1.6.12](https://github.com/simonoscr/dotfiles/compare/1.6.11...1.6.12) (2024-01-29)


### Bug Fixes

* update network etc add openldap and kanidm ([8fa1895](https://github.com/simonoscr/dotfiles/commit/8fa1895d69fca570a5206aae1154efaf672d7636))



## [1.6.11](https://github.com/simonoscr/dotfiles/compare/1.6.10...1.6.11) (2024-01-29)


### Bug Fixes

* add unbound, networking and update ([283ffd1](https://github.com/simonoscr/dotfiles/commit/283ffd1f637fa247187a1ee8e039cc7f2d9ddbba))
* update local dns ([a79e267](https://github.com/simonoscr/dotfiles/commit/a79e267fa2d13475d57a423cda665f6f90432b29))



## [1.6.10](https://github.com/simonoscr/dotfiles/compare/1.6.9...1.6.10) (2024-01-28)


### Bug Fixes

* monitoring stack ([af54d73](https://github.com/simonoscr/dotfiles/commit/af54d73ac98114c92fb5504b2a1192398c77dead))



## [1.6.9](https://github.com/simonoscr/dotfiles/compare/1.6.8...1.6.9) (2024-01-28)


### Bug Fixes

* monitoring stack update with prometheus, loki, node-exporter, promtail, granfana and nginx ([5658f35](https://github.com/simonoscr/dotfiles/commit/5658f35a4951550bd4d4bc64794c66c4480a5f2f))
* update monitoring and logging stack ([4c2b580](https://github.com/simonoscr/dotfiles/commit/4c2b580c9f8a92203ac746cb6d7e6a732625a208))



## [1.6.8](https://github.com/simonoscr/dotfiles/compare/1.6.7...1.6.8) (2024-01-28)


### Bug Fixes

* configure grafana with datasource and node dashboard ([e2ff290](https://github.com/simonoscr/dotfiles/commit/e2ff2908407e87979d202966679b5a88e9397736))



## [1.6.7](https://github.com/simonoscr/dotfiles/compare/1.6.6...1.6.7) (2024-01-28)


### Bug Fixes

* update hardware-configuration.nix ([cd83841](https://github.com/simonoscr/dotfiles/commit/cd8384117c4fc854c9d9f6c6175685645069411e))



## [1.6.6](https://github.com/simonoscr/dotfiles/compare/1.6.5...1.6.6) (2024-01-28)


### Bug Fixes

* update vaultwarden ip ([ae8cf3c](https://github.com/simonoscr/dotfiles/commit/ae8cf3c2f993d48bf554d568e96a1d73b1a2504b))



## [1.6.5](https://github.com/simonoscr/dotfiles/compare/1.6.4...1.6.5) (2024-01-28)


### Bug Fixes

* add userhome directory ([1634274](https://github.com/simonoscr/dotfiles/commit/163427421b5362a24f8702cce642f66b40501e92))



## [1.6.4](https://github.com/simonoscr/dotfiles/compare/1.6.3...1.6.4) (2024-01-28)


### Bug Fixes

* update home.manager user for server ([30e23fe](https://github.com/simonoscr/dotfiles/commit/30e23fe939ce06ab5e7fe65193ea768ea0f56af7))



## [1.6.3](https://github.com/simonoscr/dotfiles/compare/1.6.2...1.6.3) (2024-01-28)


### Bug Fixes

* update ([1cbe18c](https://github.com/simonoscr/dotfiles/commit/1cbe18c26f18d4d563ec5927901a69412af480bc))
* update age key ([d842996](https://github.com/simonoscr/dotfiles/commit/d84299666dba5d432c9cbadf3b0011aa09dddb34))
* update sshd ([1be4ae9](https://github.com/simonoscr/dotfiles/commit/1be4ae9095319bfde0835515cc5d6edc70ca308a))



## [1.6.2](https://github.com/simonoscr/dotfiles/compare/1.6.1...1.6.2) (2024-01-28)


### Bug Fixes

* update docs and first installation process ([65a44ee](https://github.com/simonoscr/dotfiles/commit/65a44eeb6682e7ac3d051163dba0cf29867c2c4f))



## [1.6.1](https://github.com/simonoscr/dotfiles/compare/1.6.0...1.6.1) (2024-01-24)


### Bug Fixes

* update zsh aliases, add podman ([ea4db23](https://github.com/simonoscr/dotfiles/commit/ea4db23f6efdf9cd981a9aac2aaa25c9caec9b81))



# [1.6.0](https://github.com/simonoscr/dotfiles/compare/1.5.17...1.6.0) (2024-01-24)


### Features

* add monitoring stack ([40d8bf0](https://github.com/simonoscr/dotfiles/commit/40d8bf028c092efd15e142c49b4bb2b343ebd554))
* add prometheus, grafana, node exporter, vaultwarden and reverse-proxy to voyager ([e373954](https://github.com/simonoscr/dotfiles/commit/e3739544eea2747b300531d5860f5e5a3ba53e66))



## [1.5.17](https://github.com/simonoscr/dotfiles/compare/1.5.16...1.5.17) (2024-01-22)


### Bug Fixes

* hashedpassword for user ([a26fa82](https://github.com/simonoscr/dotfiles/commit/a26fa825f787c077ffc020c005fafc96a9f3ce5b))



## [1.5.16](https://github.com/simonoscr/dotfiles/compare/1.5.15...1.5.16) (2024-01-21)


### Bug Fixes

* typo ([4d9b473](https://github.com/simonoscr/dotfiles/commit/4d9b473cce7347fbeda366d8f2f30eaa785eecc1))
* update security, network and user files ([5fa4eb1](https://github.com/simonoscr/dotfiles/commit/5fa4eb153dbdca0fc42518a109041703471c6b9e))



## [1.5.15](https://github.com/simonoscr/dotfiles/compare/1.5.14...1.5.15) (2024-01-21)


### Bug Fixes

* revert openssh settings ([77e4a55](https://github.com/simonoscr/dotfiles/commit/77e4a55e7781ccf136136a105d6d469b62328df1))



## [1.5.14](https://github.com/simonoscr/dotfiles/compare/1.5.13...1.5.14) (2024-01-21)


### Bug Fixes

* update user passwords ([e318444](https://github.com/simonoscr/dotfiles/commit/e318444e043277793f00e635c9e584ddc07e1620))



## [1.5.13](https://github.com/simonoscr/dotfiles/compare/1.5.12...1.5.13) (2024-01-21)


### Bug Fixes

* hashedPasswordFile for user password ([1cb14ad](https://github.com/simonoscr/dotfiles/commit/1cb14ad7b15f78962f07594a856fc563260b7bbb))



## [1.5.12](https://github.com/simonoscr/dotfiles/compare/1.5.11...1.5.12) (2024-01-21)


### Bug Fixes

* typo ([f81eea4](https://github.com/simonoscr/dotfiles/commit/f81eea45b7ca15ab2d10e256ecf2f02fe4b43315))
* update passwordfile ([9956a40](https://github.com/simonoscr/dotfiles/commit/9956a407b153dd033fc4fb9d44321a57d21002bf))



## [1.5.11](https://github.com/simonoscr/dotfiles/compare/1.5.10...1.5.11) (2024-01-21)


### Bug Fixes

* neededFor User sops ([c2b664d](https://github.com/simonoscr/dotfiles/commit/c2b664d4086c4e82625bb2cce245501de5ac64bd))
* test user password with sops ([75d569b](https://github.com/simonoscr/dotfiles/commit/75d569b1c7ff68d5f468567a9026c9ef10db57be))



## [1.5.10](https://github.com/simonoscr/dotfiles/compare/1.5.9...1.5.10) (2024-01-21)


### Bug Fixes

* update secrets files with new age key ([edfef64](https://github.com/simonoscr/dotfiles/commit/edfef64e32154b2105de227883ccdd9934b414af))



## [1.5.9](https://github.com/simonoscr/dotfiles/compare/1.5.8...1.5.9) (2024-01-21)


### Bug Fixes

* update home.nix for oscar ([2b5d683](https://github.com/simonoscr/dotfiles/commit/2b5d683b898816bb8737cef17926385b1051ea77))



## [1.5.8](https://github.com/simonoscr/dotfiles/compare/1.5.7...1.5.8) (2024-01-21)


### Bug Fixes

* update home-manager user ([97ed1f3](https://github.com/simonoscr/dotfiles/commit/97ed1f3a79d0951e2836cc971d93db5f36ad94cf))



## [1.5.7](https://github.com/simonoscr/dotfiles/compare/1.5.6...1.5.7) (2024-01-21)


### Bug Fixes

* update sops-nix and secrets files ([ac70a4f](https://github.com/simonoscr/dotfiles/commit/ac70a4ff34f2715a96d42cec134b7cc96a22739b))



## [1.5.6](https://github.com/simonoscr/dotfiles/compare/1.5.5...1.5.6) (2024-01-21)


### Bug Fixes

* update hardware-configuration.nix for server ([fee56d1](https://github.com/simonoscr/dotfiles/commit/fee56d1fb0ff333c67739ab04d5f38001fb041bc))



## [1.5.5](https://github.com/simonoscr/dotfiles/compare/1.5.4...1.5.5) (2024-01-21)


### Bug Fixes

* users and root settings ([ec371e0](https://github.com/simonoscr/dotfiles/commit/ec371e0cb7e430bc1ec01cd92b1641d4c5bd2339))



## [1.5.4](https://github.com/simonoscr/dotfiles/compare/1.5.3...1.5.4) (2024-01-20)


### Bug Fixes

* remove cli.nix from home-manager ([624aefa](https://github.com/simonoscr/dotfiles/commit/624aefa83088d13d67f2b425125f6b515253484a))



## [1.5.3](https://github.com/simonoscr/dotfiles/compare/1.5.2...1.5.3) (2024-01-20)


### Bug Fixes

* update flake for new nixos user ([c33d2ef](https://github.com/simonoscr/dotfiles/commit/c33d2ef13c7d2e5c383290ac36a59bdf0dab357b))



## [1.5.2](https://github.com/simonoscr/dotfiles/compare/1.5.1...1.5.2) (2024-01-20)


### Bug Fixes

* ja ([da8da9b](https://github.com/simonoscr/dotfiles/commit/da8da9b666aad9a902696a345ec586d383b86f2e))



## [1.5.1](https://github.com/simonoscr/dotfiles/compare/1.5.0...1.5.1) (2024-01-20)


### Bug Fixes

* switch to new user ([ace27eb](https://github.com/simonoscr/dotfiles/commit/ace27eb411a20846ce0581acaf75a91119ea476e))



# [1.5.0](https://github.com/simonoscr/dotfiles/compare/1.4.15...1.5.0) (2024-01-20)


### Bug Fixes

* add missing users.nix import ([29006bb](https://github.com/simonoscr/dotfiles/commit/29006bb5ea3d3a2870e01885586e21a9768768ec))
* define group ([2f0fc8e](https://github.com/simonoscr/dotfiles/commit/2f0fc8e7e3879bb0dbc377637bc768f8a30d90a7))
* fix naming of host ([cb0dd17](https://github.com/simonoscr/dotfiles/commit/cb0dd1786446209ed971bcacbcc21ff93193440c))
* fix pathing ([a3928ff](https://github.com/simonoscr/dotfiles/commit/a3928ff435790ebb1b34cf3604139aae046028e7))
* validateSopsFile set to false ([538cdeb](https://github.com/simonoscr/dotfiles/commit/538cdebbb738414307ef37c8635300f0cee4628f))


### Features

* new user.nix to seperate user for services and security ([04a0f0d](https://github.com/simonoscr/dotfiles/commit/04a0f0d130e63ed4d79f50c027575f1a9b166fde))



## [1.4.15](https://github.com/simonoscr/dotfiles/compare/1.4.14...1.4.15) (2024-01-20)


### Bug Fixes

* remo allowSFTP ([cb05706](https://github.com/simonoscr/dotfiles/commit/cb05706f69cc93bb99cf7af9f597d45bf0ef9067))



## [1.4.14](https://github.com/simonoscr/dotfiles/compare/1.4.13...1.4.14) (2024-01-20)


### Bug Fixes

* allowed-user update settings ([24a11fa](https://github.com/simonoscr/dotfiles/commit/24a11faea91973d1c9cb2d68fd1bb00b34d85dcb))



## [1.4.13](https://github.com/simonoscr/dotfiles/compare/1.4.12...1.4.13) (2024-01-20)


### Bug Fixes

* some ssh hardening ([afebcdb](https://github.com/simonoscr/dotfiles/commit/afebcdbd4e91103509eaea0a7163547d3a9333c3))



## [1.4.12](https://github.com/simonoscr/dotfiles/compare/1.4.11...1.4.12) (2024-01-20)


### Bug Fixes

* update mode for secrets ([9e563ea](https://github.com/simonoscr/dotfiles/commit/9e563ea74bf847d68562708c5d9c27e19474a9a8))



## [1.4.11](https://github.com/simonoscr/dotfiles/compare/1.4.10...1.4.11) (2024-01-20)


### Bug Fixes

* add fail2ban ([059956f](https://github.com/simonoscr/dotfiles/commit/059956fd8a7c54b03501af8a237c98f7158caafa))



## [1.4.10](https://github.com/simonoscr/dotfiles/compare/1.4.9...1.4.10) (2024-01-20)


### Bug Fixes

* added configuration for sops-nix ([27619d2](https://github.com/simonoscr/dotfiles/commit/27619d2c6d4a15e0236f886d7153acc42f29b1fd))



## [1.4.9](https://github.com/simonoscr/dotfiles/compare/1.4.8...1.4.9) (2024-01-20)


### Bug Fixes

* zsh as default shell for user host ([9bb89b3](https://github.com/simonoscr/dotfiles/commit/9bb89b35a022736233f165cf31852c6b42b91266))



## [1.4.8](https://github.com/simonoscr/dotfiles/compare/1.4.7...1.4.8) (2024-01-20)


### Bug Fixes

* path for pub key symlink ([5e8fc53](https://github.com/simonoscr/dotfiles/commit/5e8fc539bcf260034cc9b33c50e8b1d394919acc))



## [1.4.7](https://github.com/simonoscr/dotfiles/compare/1.4.6...1.4.7) (2024-01-20)


### Bug Fixes

* authorized_key path ([98b2054](https://github.com/simonoscr/dotfiles/commit/98b2054f367bfa1e24f8d165586c0cf1dbca7689))



## [1.4.6](https://github.com/simonoscr/dotfiles/compare/1.4.5...1.4.6) (2024-01-20)


### Bug Fixes

* add mode for permissions ssh pub key ([1191d68](https://github.com/simonoscr/dotfiles/commit/1191d68f7a9ed6d0111f2f4792108cbf15c35cfa))



## [1.4.5](https://github.com/simonoscr/dotfiles/compare/1.4.4...1.4.5) (2024-01-20)


### Bug Fixes

* permissions ([b06b56b](https://github.com/simonoscr/dotfiles/commit/b06b56b802b117937bb81e1f0d7c8fdfaf67d253))



## [1.4.4](https://github.com/simonoscr/dotfiles/compare/1.4.3...1.4.4) (2024-01-20)


### Bug Fixes

* typo ([94b54f6](https://github.com/simonoscr/dotfiles/commit/94b54f6f398b7d188171d9baa889a71b43a9f4f8))



## [1.4.3](https://github.com/simonoscr/dotfiles/compare/1.4.2...1.4.3) (2024-01-20)


### Bug Fixes

* add wifi ([304ce21](https://github.com/simonoscr/dotfiles/commit/304ce214c3a7fd1a7d21834d47fc8ee84cea23ac))
* again miassing ; ([856ca61](https://github.com/simonoscr/dotfiles/commit/856ca6136b542679ca8aacf6479c0791d28a52c9))
* boolean without quotes for sure ([8a43943](https://github.com/simonoscr/dotfiles/commit/8a43943c76a25a97abb467e1f7fb49c1414d4ca1))



## [1.4.2](https://github.com/simonoscr/dotfiles/compare/1.4.1...1.4.2) (2024-01-20)


### Bug Fixes

* add keyfile path for user ([160e0b1](https://github.com/simonoscr/dotfiles/commit/160e0b140e9ecdf599281dfbc387284f6b8e0eb2))
* add owner of secrets and symlink for ssh key ([f6c16fd](https://github.com/simonoscr/dotfiles/commit/f6c16fdd0dd7db5abc6cb37ec04c14b27a4b5bd3))
* missing ; ([a844916](https://github.com/simonoscr/dotfiles/commit/a8449167418877081fe836546e3a08cbcfe61636))



## [1.4.1](https://github.com/simonoscr/dotfiles/compare/1.4.0...1.4.1) (2024-01-20)


### Bug Fixes

* add ssh key file location to openssh ([fc96b7a](https://github.com/simonoscr/dotfiles/commit/fc96b7a00a44e03934eb523c72349651aa8b4e93))
* missing ; ([d9c15b3](https://github.com/simonoscr/dotfiles/commit/d9c15b37e7f7a293814031eee7b9fec5c8222421))



# [1.4.0](https://github.com/simonoscr/dotfiles/compare/1.3.3...1.4.0) (2024-01-20)


### Bug Fixes

* import sops-nix module correct ([9dddb82](https://github.com/simonoscr/dotfiles/commit/9dddb8206d74b7a0afb636a6b809d528dff377ad))
* missing s for inputs in ssh.nix ([b59b84a](https://github.com/simonoscr/dotfiles/commit/b59b84abe472fe1d7b7108a502c1d22e2d8b3f78))
* switch linux kernel to latest instead of latest_hardened ([cf29ee5](https://github.com/simonoscr/dotfiles/commit/cf29ee5190ea5c43207243390b12a32a018d1d13))


### Features

* add ssh encryption for ssh connection - test ([417752a](https://github.com/simonoscr/dotfiles/commit/417752a8ed4312cd6d40a9f983965e26928a7e3c))



## [1.3.3](https://github.com/simonoscr/dotfiles/compare/1.3.2...1.3.3) (2024-01-19)


### Bug Fixes

* add k9s ([cde666e](https://github.com/simonoscr/dotfiles/commit/cde666edd522f0d85d05945a39bf57f3e44ffb2e))



## [1.3.2](https://github.com/simonoscr/dotfiles/compare/1.3.1...1.3.2) (2024-01-19)


### Bug Fixes

* add helm to voyager ([e4415fc](https://github.com/simonoscr/dotfiles/commit/e4415fc34b7cba6fe280d0a21a7411435e0be95e))



## [1.3.1](https://github.com/simonoscr/dotfiles/compare/1.3.0...1.3.1) (2024-01-19)


### Bug Fixes

* import missing k3s.nix ([34d4cd1](https://github.com/simonoscr/dotfiles/commit/34d4cd18b1228be93118833291d3b077afc73485))



# [1.3.0](https://github.com/simonoscr/dotfiles/compare/1.2.8...1.3.0) (2024-01-19)


### Features

* add k3s ([8ebfc2d](https://github.com/simonoscr/dotfiles/commit/8ebfc2d4de04ef3524375c411c751e66e43a40df))



## [1.2.8](https://github.com/simonoscr/dotfiles/compare/1.2.7...1.2.8) (2024-01-19)


### Bug Fixes

* add libvrtd ([20ebd1b](https://github.com/simonoscr/dotfiles/commit/20ebd1b04b9ae1617ddaac87c32b0f55b55e9f0e))



## [1.2.7](https://github.com/simonoscr/dotfiles/compare/1.2.6...1.2.7) (2024-01-19)


### Bug Fixes

* add new hardware configruation with LVM ([78d88fa](https://github.com/simonoscr/dotfiles/commit/78d88fa7a86dea86791a8b1233ea45ce5266ebd3))



## [1.2.6](https://github.com/simonoscr/dotfiles/compare/1.2.5...1.2.6) (2024-01-18)


### Bug Fixes

* vsc extensions ([4f83f24](https://github.com/simonoscr/dotfiles/commit/4f83f2436117297449a57cc469e8d81d4c07e807))



## [1.2.5](https://github.com/simonoscr/dotfiles/compare/1.2.4...1.2.5) (2024-01-18)


### Bug Fixes

* add vscodium extension ([bf0e881](https://github.com/simonoscr/dotfiles/commit/bf0e88182cc77ac61301265c198a07128cdf3b32))
* ya ([f637ca4](https://github.com/simonoscr/dotfiles/commit/f637ca4aad0c0f6c1e672aaae666db154be727bd))



## [1.2.4](https://github.com/simonoscr/dotfiles/compare/1.2.3...1.2.4) (2024-01-18)


### Bug Fixes

* modules path for host ([96530b2](https://github.com/simonoscr/dotfiles/commit/96530b23c9120acfe68871fc4f17256a18b12caa))



## [1.2.3](https://github.com/simonoscr/dotfiles/compare/1.2.2...1.2.3) (2024-01-18)


### Bug Fixes

* home name ([fff59ed](https://github.com/simonoscr/dotfiles/commit/fff59ed72d9b6ef6fecce66d73d814bd9fa22f16))



## [1.2.2](https://github.com/simonoscr/dotfiles/compare/1.2.1...1.2.2) (2024-01-18)


### Bug Fixes

* change user name ([5f46064](https://github.com/simonoscr/dotfiles/commit/5f46064159ab576d3ea17662f7f034d819c9f30c))



## [1.2.1](https://github.com/simonoscr/dotfiles/compare/1.2.0...1.2.1) (2024-01-18)


### Bug Fixes

* activate voyager host ([799e566](https://github.com/simonoscr/dotfiles/commit/799e566477ab0669981a82c7578389f0dca7f0ee))
* add hardware-configuration.nix for voyager ([3cbac0f](https://github.com/simonoscr/dotfiles/commit/3cbac0f1890d1501ecfefa6f40498fb71fb9884a))
* remove wrong/unused imports for voyager ([717973f](https://github.com/simonoscr/dotfiles/commit/717973f905b86b0cae9ed59674efa7e4537b8bd1))



# [1.2.0](https://github.com/simonoscr/dotfiles/compare/1.1.2...1.2.0) (2024-01-18)


### Features

* add home-server voyager configuration ([7133d63](https://github.com/simonoscr/dotfiles/commit/7133d63efcac6d3260d592cc5d1fc6432a011ac5))



## [1.1.2](https://github.com/simonoscr/dotfiles/compare/1.1.1...1.1.2) (2024-01-14)


### Bug Fixes

* wlroots dependency error for gamescope; wlroots version too high for gamescope ([70742a6](https://github.com/simonoscr/dotfiles/commit/70742a6546e6204bb27f4e606025adbd6cc4166b))



## [1.1.1](https://github.com/simonoscr/dotfiles/compare/1.1.0...1.1.1) (2024-01-07)


### Bug Fixes

* make swayidle able to shut off display in hyprland and change look of swaylock ([14efbd3](https://github.com/simonoscr/dotfiles/commit/14efbd31b0ec2cc7ea44c557af817309b94135c3))



# [1.1.0](https://github.com/simonoscr/dotfiles/compare/1.0.5...1.1.0) (2024-01-07)


### Features

* add i3 and hyprland session to tuigreet ([f80743c](https://github.com/simonoscr/dotfiles/commit/f80743cdbfb974f71495daa16729d476b20c69c6))



## [1.0.5](https://github.com/simonoscr/dotfiles/compare/1.0.4...1.0.5) (2024-01-07)


### Bug Fixes

* tuigreetd ([a0d0f53](https://github.com/simonoscr/dotfiles/commit/a0d0f53ec84b51be95d60b8db40616a2fc784c3c))



## [1.0.4](https://github.com/simonoscr/dotfiles/compare/1.0.3...1.0.4) (2024-01-07)


### Bug Fixes

* updated i3 configuration ([dc90015](https://github.com/simonoscr/dotfiles/commit/dc900156eabe4f45797cfbb1cd7ec445b674ba58))



## [1.0.3](https://github.com/simonoscr/dotfiles/compare/1.0.2...1.0.3) (2024-01-05)


### Bug Fixes

* update ags fixes ([ca26347](https://github.com/simonoscr/dotfiles/commit/ca263475add4f71b6eaf025e192ec1786c6c7d82))



## [1.0.2](https://github.com/simonoscr/dotfiles/compare/1.0.1...1.0.2) (2024-01-05)


### Bug Fixes

* update ags files ([df9292f](https://github.com/simonoscr/dotfiles/commit/df9292f0dd08bbcadd81f06916ba8a2e896fcf41))
* update ags fils ([f19edfb](https://github.com/simonoscr/dotfiles/commit/f19edfb4d116fd31d553cbce243a816bedce355f))



## [1.0.1](https://github.com/simonoscr/dotfiles/compare/1.0.0...1.0.1) (2024-01-04)


### Bug Fixes

* update ags ([bd2ce8f](https://github.com/simonoscr/dotfiles/commit/bd2ce8fdcd98fd301fac6b6fa0bde18a3cdba9aa))
* update ags ([6f92c46](https://github.com/simonoscr/dotfiles/commit/6f92c46cb1d9d308cec4429ec949aedc11cb79ab))



# [1.0.0](https://github.com/simonoscr/dotfiles/compare/0.6.7...1.0.0) (2024-01-03)


### Features

* change hardware ([cf2bb6f](https://github.com/simonoscr/dotfiles/commit/cf2bb6f34e826f9e4850ba1781d158697ece97ea))


### BREAKING CHANGES

* hardware configuration not for old intel set-up. It is for the AMD GPU now



## [0.6.7](https://github.com/simonoscr/dotfiles/compare/0.6.6...0.6.7) (2023-12-30)


### Bug Fixes

* add wine and bottles ([7083d44](https://github.com/simonoscr/dotfiles/commit/7083d4448c789c657d2c36923ae05d16f5e75578))



## [0.6.6](https://github.com/simonoscr/dotfiles/compare/0.6.5...0.6.6) (2023-12-29)


### Bug Fixes

* add protonup-qt ([70bcbf8](https://github.com/simonoscr/dotfiles/commit/70bcbf8acae4d7ab9d121f5ca9068028a023a558))



## [0.6.5](https://github.com/simonoscr/dotfiles/compare/0.6.4...0.6.5) (2023-12-29)


### Bug Fixes

* hyprland steam windowrules ([7498d6c](https://github.com/simonoscr/dotfiles/commit/7498d6c4629229ad651c0f7727fdc7071a1775f2))



## [0.6.4](https://github.com/simonoscr/dotfiles/compare/0.6.3...0.6.4) (2023-12-22)


### Bug Fixes

* change vscodium shortcut; add i3 ([debeaea](https://github.com/simonoscr/dotfiles/commit/debeaeaaffe28463c2cbd5f2e3a3bcd13e372424))



## [0.6.3](https://github.com/simonoscr/dotfiles/compare/0.6.2...0.6.3) (2023-12-22)


### Bug Fixes

* disable network-wait; fine-tuning tuigreet ([5b60437](https://github.com/simonoscr/dotfiles/commit/5b60437720502b2edddb4ff855a356300ac00112))



## [0.6.2](https://github.com/simonoscr/dotfiles/compare/0.6.1...0.6.2) (2023-12-22)


### Bug Fixes

* add thunar shortcut ([c702283](https://github.com/simonoscr/dotfiles/commit/c70228333a593609775697c23ee69d4b7d55d4fc))



## [0.6.1](https://github.com/simonoscr/dotfiles/compare/0.6.0...0.6.1) (2023-12-22)


### Bug Fixes

* add thunar; enable greetd ([3560f3d](https://github.com/simonoscr/dotfiles/commit/3560f3de315ff56542a6779a505f57a42a1c2c18))



# [0.6.0](https://github.com/simonoscr/dotfiles/compare/0.5.5...0.6.0) (2023-12-22)


### Features

* removed swap fileSystem and add test fileSystem; hyprland to home-manager, wayland settings, added waybar update flock ([f113064](https://github.com/simonoscr/dotfiles/commit/f113064835fb713ed7cac9bd2314d639c4904389))



## [0.5.5](https://github.com/simonoscr/dotfiles/compare/0.5.4...0.5.5) (2023-12-22)


### Bug Fixes

* set workspaces for hyprland+ags to 0 for dynamic workspaces ([4665051](https://github.com/simonoscr/dotfiles/commit/46650518f37857934bb10c01ea3431aabdbfa2a4))



## [0.5.4](https://github.com/simonoscr/dotfiles/compare/0.5.3...0.5.4) (2023-12-22)


### Bug Fixes

* codium hyprland shotcut; add sessionVariable NIXOS_OZONE_WL=1 ([d59b129](https://github.com/simonoscr/dotfiles/commit/d59b1297af2e56dd3b6887f5dbea72d31133149b))



## [0.5.3](https://github.com/simonoscr/dotfiles/compare/0.5.2...0.5.3) (2023-12-22)


### Bug Fixes

* update hyprland and ags config ([fc17b77](https://github.com/simonoscr/dotfiles/commit/fc17b77c02ac6929f92cfa8bcdf02a278f4df7d3))



## [0.5.2](https://github.com/simonoscr/dotfiles/compare/0.5.1...0.5.2) (2023-12-21)


### Bug Fixes

* gamescope, manoghud and gaming related fixes ([2c01b55](https://github.com/simonoscr/dotfiles/commit/2c01b557c43e04d668d75e00b4edb69e75cd5634))



## [0.5.1](https://github.com/simonoscr/dotfiles/compare/0.5.0...0.5.1) (2023-12-21)


### Bug Fixes

* activate steam with dependencies ([94ec00d](https://github.com/simonoscr/dotfiles/commit/94ec00d4e0a02fb984751f97f71536f3012e8e3b))



# [0.5.0](https://github.com/simonoscr/dotfiles/compare/0.4.2...0.5.0) (2023-12-20)


### Features

* add inputs; nixos-hardware, nixpkgs-wayland and smoll fixes ([40972d4](https://github.com/simonoscr/dotfiles/commit/40972d4b06223625604585150ff05bdd8852fd31))



## [0.4.2](https://github.com/simonoscr/dotfiles/compare/0.4.1...0.4.2) (2023-12-20)


### Bug Fixes

* hyprland, hm-hyprland, steam with extraPkgs and gamescope configuration ([b37a47b](https://github.com/simonoscr/dotfiles/commit/b37a47b277d551c46f02708773b58da8fe235410))



## [0.4.1](https://github.com/simonoscr/dotfiles/compare/0.4.0...0.4.1) (2023-12-17)


### Bug Fixes

* swayidle timeouts ([d287829](https://github.com/simonoscr/dotfiles/commit/d287829a6bf78810bb11e1c129c368dff7304720))



# [0.4.0](https://github.com/simonoscr/dotfiles/compare/0.3.2...0.4.0) (2023-12-17)


### Features

* add NUR nix user repository and his overlay; firefox settings ([b9d7ca0](https://github.com/simonoscr/dotfiles/commit/b9d7ca0601a9d274de16394ab4ea230a0c1580de))



## [0.3.2](https://github.com/simonoscr/dotfiles/compare/0.3.1...0.3.2) (2023-12-17)


### Bug Fixes

* enable fstrim, remove kernelParams, add neovim as nixvim module ([d3aaf94](https://github.com/simonoscr/dotfiles/commit/d3aaf94c8c70711dbe90a33097c146cc6134f7d4))



## [0.3.1](https://github.com/simonoscr/dotfiles/compare/0.3.0...0.3.1) (2023-12-16)


### Bug Fixes

* hardware-configuration; using now the new default (modesetting) in xserver.videoDriver (other drivers unmaintained) ([f9962c9](https://github.com/simonoscr/dotfiles/commit/f9962c97cf939ca268a1c0be135759d7577c52b3))



# [0.3.0](https://github.com/simonoscr/dotfiles/compare/0.2.3...0.3.0) (2023-12-15)


### Features

* add config and dotfiles ([cbe7edc](https://github.com/simonoscr/dotfiles/commit/cbe7edcf88a3d9ee873d0826ecbae201c6a32746))



## [0.2.3](https://github.com/simonoscr/dotfiles/compare/0.2.2...0.2.3) (2023-12-15)


### Bug Fixes

* remove double vscodium pkg ([540d444](https://github.com/simonoscr/dotfiles/commit/540d4447b5bee97d45014fc98a35e9a09e31a4f3))



## [0.2.2](https://github.com/simonoscr/dotfiles/compare/0.2.1...0.2.2) (2023-12-14)


### Bug Fixes

* fonts in hyprland config ([78e2ca9](https://github.com/simonoscr/dotfiles/commit/78e2ca95f70c440e7515be6dc7bc8fe7a809bdc2))



## [0.2.1](https://github.com/simonoscr/dotfiles/compare/0.2.0...0.2.1) (2023-12-14)


### Bug Fixes

* cursor theme and font settings ([c612906](https://github.com/simonoscr/dotfiles/commit/c6129061f6bb4c97b24cf9ae2c0842c9748e93ac))



# [0.2.0](https://github.com/simonoscr/dotfiles/compare/0.1.0...0.2.0) (2023-12-14)


### Features

* move steam to systemPackages and apply gamescope fix; fix theme related things ([c35322a](https://github.com/simonoscr/dotfiles/commit/c35322abda92f3e933b1e35352f57101ab5ec927))



# [0.1.0](https://github.com/simonoscr/dotfiles/compare/e82ced4c65128b4f94382bc27b991ae001613f8e...0.1.0) (2023-12-13)


### Features

* test ([718f12c](https://github.com/simonoscr/dotfiles/commit/718f12c18253c49b60f3e4963f48708772c5f57b))
* update nixos with more modules ([e82ced4](https://github.com/simonoscr/dotfiles/commit/e82ced4c65128b4f94382bc27b991ae001613f8e))
