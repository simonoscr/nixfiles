_: {
  imports = [
    ./audio.nix
    ./dconf.nix
    ./kdeconnect.nix
    ./polkit.nix
    ./tailray.nix
    ./udiskie.nix
  ];
}
