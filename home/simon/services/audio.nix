{
  config,
  lib,
  ...
}:
with lib;

let
  cfg = config.module.services.audio;

  xdg.configHome =
    let
      x = builtins.getEnv "XDG_CONFIG_HOME";
    in
    if x != "" then x else "${builtins.getEnv "HOME"}/.config";
in
{
  options.module.services.audio = {
    enable = mkEnableOption "Enable audio";
  };

  config = mkIf cfg.enable {
    ## this can switch left and right channel. needed this for my old headset
    #home.file."${xdg.configHome}/wireplumber/wireplumber.conf.d/51-change-channels.conf".text = ''
    #  monitor.alsa.rules = [
    #    {
    #      matches = [
    #        {
    #          node.name = "~alsa_output.usb-Focusrite_Scarlett_Solo_USB_Y7NY0CB223AE7B-00.*"
    #        }
    #      ]
    #      actions = {
    #        update-props = {
    #          audio.position  = "FR,FL"
    #        }
    #      }
    #    }
    #  ]
    #'';

    # wireplumber low-latency config
    home.file."${xdg.configHome}/wireplumber/wireplumber.conf.d/99-low-latency.conf".text = ''
      monitor.alsa.rules = [
        {
          matches = [
            {
              node.name = "~alsa_output.*"
            }
          ]
          actions = {
            update-props = {
              audio.format = "S32LE";
              audio.rate = 96000;
              api.alsa.period-size = 2;
            }
          }
        }
      ]
    '';
  };
}
