_: {
  imports = [
    ./gpg.nix
    ./mpv.nix
    ./ssh.nix
    ./xdg.nix
    ./zathura.nix
  ];
}
