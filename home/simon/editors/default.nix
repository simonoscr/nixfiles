_: {
  imports = [
    ./editorconfig.nix
    ./neovim.nix
    ./vscodium.nix
    ./zed
  ];
}
