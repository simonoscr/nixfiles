_: {
  programs.git = {
    ignores = [
      ".DS_Store"
      "*.swp"
      "Thumbs.db"
      ".idea"
      ".vscode"
      ".vscodium"
      ".direnv"
      "pre-commit-config.yaml"
      "results"
    ];
  };
}
