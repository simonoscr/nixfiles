_: {
  imports = [
    ./emulators/alacritty.nix
    ./emulators/kitty.nix
    ./emulators/wezterm.nix
    ./emulators/ghostty.nix
    ./programs/git
    ./programs/jujutsu
    ./programs/yazi
    ./programs/bat.nix
    ./programs/btop.nix
    ./programs/dircolors.nix
    ./programs/direnv.nix
    ./programs/fzf.nix
    ./programs/k9s.nix
    ./shell
  ];
}
