_: {
  imports = [
    ./zsh
    ./bash.nix
    ./aliases.nix
    ./fish.nix
    ./integration.nix
    ./starship.nix
  ];
}
