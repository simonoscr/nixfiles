_: {
  imports = [
    ./amdcpugpu.nix
    ./bluetooth.nix
    ./fwupd.nix
    ./hardware-configuration.nix
    ./wooting.nix
  ];
}
