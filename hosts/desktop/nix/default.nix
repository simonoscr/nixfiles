_: {
  imports = [
    ./docs.nix
    ./nix.nix
    ./nixpkgs.nix
    ./substituters.nix
    ./system.nix
  ];
}
