_: {
  imports = [
    ./ananicy-cpp.nix
    ./gamemode.nix
    ./gamescope.nix
    ./steam.nix
    ./system.nix
  ];
}
