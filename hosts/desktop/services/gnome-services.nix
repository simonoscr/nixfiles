_: {
  services = {

    gnome.gnome-keyring.enable = true;

    gvfs.enable = true;
  };
  programs.gnome-disks.enable = true;
}
