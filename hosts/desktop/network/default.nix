_: {
  imports = [
    ./firewall.nix
    ./network.nix
    ./openssh.nix
    ./optimize.nix
    ./resolved.nix
    ./tailscale.nix
  ];
}
