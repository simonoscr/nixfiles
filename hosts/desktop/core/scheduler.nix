_: {
  services.scx = {
    enable = true;
    scheduler = "scx_lavd";
    extraArgs = [
      #"--performance"
      "--autopower"
    ];
  };
}
