_: {
  imports = [
    ./boot.nix
    ./console.nix
    ./dbus.nix
    #./impermanence.nix
    ./locale.nix
    ./scheduler.nix
    ./users.nix
  ];
}
