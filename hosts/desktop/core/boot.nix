{ pkgs, ... }:
{
  boot = {
    kernelPackages = pkgs.linuxPackages_cachyos; # pkgs.linuxPackages_cachyos, pkgs.linuxPackages_xanmod_latest, pkgs.linuxPackages_zen, pkgs.linuxPackages_lqx, linuxPackages_latest

    kernelModules = [
      "kvm-amd"
    ];

    supportedFilesystems = [
      "ext4"
      "vfat"
      "tmpfs"
    ];

    swraid.enable = false;

    # Enable binfmt emulation of aarch64-linux, this is required for cross compilation.
    #binfmt.emulatedSystems = [ "aarch64-linux" ];

    loader = {
      timeout = 1;
      generationsDir.copyKernels = true;
      efi.canTouchEfiVariables = true;

      systemd-boot = {
        enable = true;
        configurationLimit = 15;
        consoleMode = "max";
        editor = false;
        netbootxyz.enable = true;
      };
    };

    tmp = {
      # /tmp on tmpfs, lets it live on your ram
      # it defaults to FALSE, which means you will use disk space instead of ram
      # enable tmpfs tmp on anything except servers and builders
      useTmpfs = true;

      # If not using tmpfs, which is naturally purged on reboot, we must clean
      # /tmp ourselves. /tmp should be volatile storage!
      cleanOnBoot = true;

      # The size of the tmpfs, in percentage form
      # this defaults to 50% of your ram, which is a good default
      # but should be tweaked based on your systems capabilities
      tmpfsSize = "90%";
    };

    initrd = {
      availableKernelModules = [
        "nvme"
        "xhci_pci"
        "ahci"
        "usbhid"
      ];
      # Verbosity of the initrd
      # disabling verbosity removes only the mandatory messages generated by the NixOS
      verbose = false;
      systemd = {
        # enable systemd in initrd
        enable = true;
        strip = true;
      };
    };
  };
}
